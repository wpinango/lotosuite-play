package com.lotosuiteplay.azar.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;

public class OwnTextView extends ConstraintLayout {
    private TextView tvTitle;
    private TextView tvBody;

    public OwnTextView(Context context) {
        super(context);
        init(context);
    }

    public OwnTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.OwnTextView, 0, 0);
        try {
            setTitleText(ta.getString(R.styleable.OwnTextView_titleText));
            setBodyText(ta.getString(R.styleable.OwnTextView_bodyText));
        } catch (Exception e){

        }
    }

    public OwnTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.OwnTextView, 0, 0);
        try {
            setTitleText(ta.getString(R.styleable.OwnTextView_titleText));
            setBodyText(ta.getString(R.styleable.OwnTextView_bodyText));
        } catch (Exception e){

        }
    }

    public void init(Context context) {
        ConstraintLayout.inflate(context, R.layout.own_textview,this);
        tvTitle = findViewById(R.id.tv_title);
        tvBody = findViewById(R.id.tv_body);
    }

    public void setTitleText(String title) {
        tvTitle.setText(title);
    }

    public void setBodyText(String body) {
        tvBody.setText(body);
    }

    public void setTextSize(float titleSize, float bodySize) {
        tvTitle.setTextSize(titleSize);
        tvBody.setTextSize(bodySize);
    }
}
