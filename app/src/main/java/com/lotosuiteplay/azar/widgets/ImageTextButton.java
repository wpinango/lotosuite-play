package com.lotosuiteplay.azar.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;

public class ImageTextButton extends ConstraintLayout {

    private TextView tvButtonTitle;
    private ImageView btnText;

    public ImageTextButton(Context context) {
        super(context);
        init(context);
    }

    public ImageTextButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ImageTextButton, 0, 0);
        try {
            setColorText(ta.getColor(R.styleable.ImageTextButton_textColor, getResources().getColor(R.color.white)));
            setImageResource(ta.getResourceId(R.styleable.ImageTextButton_imgResource, R.drawable.ic_lock_black_36dp));
            setButtonText(ta.getString(R.styleable.ImageTextButton_text));
        } catch (Exception e) {

        }
    }

    public ImageTextButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ImageTextButton, 0, 0);
        try {
            setColorText(ta.getColor(R.styleable.ImageTextButton_textColor, getResources().getColor(R.color.white)));
            setImageResource(ta.getResourceId(R.styleable.ImageTextButton_imgResource, R.drawable.ic_lock_black_36dp));
            setButtonText(ta.getString(R.styleable.ImageTextButton_text));
        } catch (Exception e) {

        }
    }


    public void init(Context context) {
        ConstraintLayout.inflate(context, R.layout.button_text, this);
        tvButtonTitle = findViewById(R.id.tv_button_title);
        btnText = findViewById(R.id.btn_text);
    }

    public void setButtonText(String title) {
        tvButtonTitle.setText(title);
    }

    public void setImageResource(int resourceImage) {
        btnText.setImageResource(resourceImage);
    }

    public void setColorText(int colorText) {
        tvButtonTitle.setTextColor(colorText);
    }
}
