package com.lotosuiteplay.azar.roulette;


import com.lotosuiteplay.azar.util.view.Roulette;

import aurelienribon.tweenengine.TweenAccessor;

public class RouletteAccessor implements TweenAccessor<Roulette> {

    public static final int ROTATION1 = 4;
    public static final int ROTATION2 = 7;
    public static final int ROTATION3 = 8;

    @Override
    public int getValues(Roulette target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case ROTATION1:
                returnValues[0] = target.getDegrees(0);
                //returnValues[1] = target.getDegrees(1);
                // returnValues[2] = target.getDegrees(2);
                return 1;
            case ROTATION2:
                //returnValues[0] = target.getDegrees(0);
                returnValues[0] = target.getDegrees(1);
                //returnValues[2] = target.getDegrees(2);
                return 1;
            case ROTATION3:
                //returnValues[0] = target.getDegrees(0);
                //returnValues[1] = target.getDegrees(1);
                returnValues[0] = target.getDegrees(2);
                return 1;
            default:
                assert false;
                return -1;
        }
    }

    @Override
    public void setValues(Roulette target, int tweenType, float[] newValues) {
        switch (tweenType) {

            case ROTATION1:
                target.setDegrees(0, newValues[0]);
                break;
            case ROTATION2:
                target.setDegrees(1, newValues[0]);
                break;
            case ROTATION3:
                target.setDegrees(2, newValues[0]);
                break;
            default:
                assert false;
        }
    }

}
