package com.lotosuiteplay.azar.roulette;

import android.content.Context;
import android.media.AudioManager;
import android.util.SparseArray;

import com.lotosuiteplay.azar.R;

/**
 * Created by JoseAntonio on 8/9/2017.
 */

public class SoundPool {

    public static int SOUNDPOOLSND_WIN = 40;
    private android.media.SoundPool mSoundPool;
    private SparseArray<Integer> mSoundPoolMap;
    private AudioManager mAudioManager;
    public static boolean isSoundTurnedOff;
    public static final int maxSounds = 4;
    private static SoundPool mSoundManager;

    public static SoundPool getInstance(Context context) {
        if (mSoundManager == null) {
            mSoundManager = new SoundPool(context);
        }

        return mSoundManager;
    }

    public SoundPool(Context mContext) {
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mSoundPool = new android.media.SoundPool(maxSounds, AudioManager.STREAM_MUSIC, 0);

//        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
//            public void onLoadComplete(SoundPool soundPool, int sampleId,int status) {
//               loaded = true;
//            }
//        });

        mSoundPoolMap = new SparseArray<Integer>();
        for (int i = 0; i < 37; i++) {
            mSoundPoolMap.put(i, mSoundPool.load(mContext, R.raw.click, 1));
        }
        //mSoundPoolMap.put(0, mSoundPool.load(mContext, R.raw.click, 1));
        mSoundPoolMap.put(SOUNDPOOLSND_WIN, mSoundPool.load(mContext, R.raw.bell_own, 1));
//            // testing simultaneous playing
//            int streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//            mSoundPool.play(mSoundPoolMap.get(0), streamVolume, streamVolume, 1, 20, 1f);
//            mSoundPool.play(mSoundPoolMap.get(1), streamVolume, streamVolume, 1, 2, 1f);
//            mSoundPool.play(mSoundPoolMap.get(2), streamVolume, streamVolume, 1, 0, 1f);


    }

    public void playSound(int index) {
        if (isSoundTurnedOff)
            return;

        int streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mSoundPool.play(mSoundPoolMap.get(index), streamVolume, streamVolume, 1, 0, 1f);
    }

    public static void clear() {
        if (mSoundManager != null) {
            mSoundManager.mSoundPool = null;
            mSoundManager.mAudioManager = null;
            mSoundManager.mSoundPoolMap = null;
        }
        mSoundManager = null;
    }
}