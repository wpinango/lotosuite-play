package com.lotosuiteplay.azar.roulette;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.activities.RouletteActivity;
import com.lotosuiteplay.azar.models.GameData;
import com.lotosuiteplay.azar.models.RaffleData;
import com.lotosuiteplay.azar.models.ResultData;
import com.lotosuiteplay.azar.models.ResultNotification;
import com.lotosuiteplay.azar.models.SimpleDetail;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Roulette {

    public static void launchRouletteActivity(Context context, String value) {
        ResultNotification resultNotifications = new Gson().fromJson(value, ResultNotification.class);
        ArrayList<SimpleDetail> simpleDetails = resultNotifications.getResults();
        GameData g = new GameData();
        GameData g1 = new GameData();
        GameData g2 = new GameData();
        LinkedHashMap<String, GameData> data = new LinkedHashMap<String, GameData>();
        for (SimpleDetail s : simpleDetails) {
            switch (s.getCodeName()) {
                case Global.KEY_SAF:
                    g.chipWinner = s.getChip();
                    data.put(Global.KEY_SAF, g);
                    Global.resultImage[0] = BitmapFactory.decodeResource(context.getResources(),
                            context.getResources().getIdentifier(s.getChipDescription().trim()
                                    .toLowerCase(), "drawable", context.getPackageName()));
                    break;
                case Global.KEY_TIZ:
                    g1.chipWinner = s.getChip();
                    data.put(Global.KEY_TIZ, g1);
                    Global.resultImage[1] = BitmapFactory.decodeResource(context.getResources(),
                            context.getResources().getIdentifier(s.getChipDescription().trim()
                                    .toLowerCase(), "drawable", context.getPackageName()));
                    if (s.getChipDescription().equals(Global.KEY_PINEAPPLE)) {
                        Global.resultImage[1] = BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.pina);
                    }
                    break;
                case Global.KEY_LOT:
                    g2.chipWinner = s.getChip();
                    data.put(Global.KEY_LOT, g2);
                    Global.resultImage[2] = BitmapFactory.decodeResource(context.getResources(),
                            context.getResources().getIdentifier(s.getChipDescription().trim()
                                    .toLowerCase(), "drawable", context.getPackageName()));
                    break;
            }
        }
        String[] rotation = resultNotifications.getRotationTime().split(",");
        ResultData r = new ResultData();
        r.currentResult = new RaffleData();
        r.currentResult.results = data;
        r.timeRotation = new int[3];
        r.timeRotation[0] = Integer.valueOf(rotation[0]) ;      //Loto
        r.timeRotation[1] = Integer.valueOf(rotation[1]) ;      //Tizana
        r.timeRotation[2] = Integer.valueOf(rotation[2]) ;      //Safari
        r.currentResult.raffleWinSerial = resultNotifications.getSerial();
        r.currentResult.raffleName = resultNotifications.getRaffle();
        Intent intent = new Intent(context, RouletteActivity.class);
        intent.putExtra("roulette", new Gson().toJson(r));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
