package com.lotosuiteplay.azar.models;

import androidx.annotation.NonNull;

public class Statistics extends SimpleDetail implements Comparable<Statistics>{
    private int raffled;
    private String percent;

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public int getRaffled() {
        return raffled;
    }

    public void setRaffled(int raffled) {
        this.raffled = raffled;
    }

    @Override
    public int compareTo(@NonNull Statistics statistics) {
        return Integer.compare(statistics.getRaffled(), this.raffled);
    }
}
