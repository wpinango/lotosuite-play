package com.lotosuiteplay.azar.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wpinango on 6/7/17.
 */

public class Sell {
    public String userId;
    public String xSerial;
    public ArrayList<SellItem> sellItems;
    private String licenseTermVersion;
    private String gameTermVersion;
    private Map<String, Integer> triplePlayValid;

    public Sell() {
        sellItems = new ArrayList<>();
        triplePlayValid = new HashMap<>();
    }

    public String getLicenseTermVersion() {
        return licenseTermVersion;
    }

    public void setLicenseTermVersion(String licenseTermVersion) {
        this.licenseTermVersion = licenseTermVersion;
    }

    public String getGameTermVersion() {
        return gameTermVersion;
    }

    public void setGameTermVersion(String gameTermVersion) {
        this.gameTermVersion = gameTermVersion;
    }

    public Map<String, Integer> getTriplePlayValid() {
        return triplePlayValid;
    }

    public void setTriplePlayValid(Map<String, Integer> triplePlayValid) {
        this.triplePlayValid = triplePlayValid;
    }

    public String getUserId() {
        return userId;
    }

    public String getxSerial() {
        return xSerial;
    }

    public void setxSerial(String xSerial) {
        this.xSerial = xSerial;
    }

    public ArrayList<SellItem> getSellItems() {
        return sellItems;
    }

    public void setSellItems(ArrayList<SellItem> sellItems) {
        this.sellItems = sellItems;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

