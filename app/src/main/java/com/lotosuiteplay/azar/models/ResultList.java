package com.lotosuiteplay.azar.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/22/17.
 */

public class ResultList  {
    public ArrayList<SimpleDetail> resultMobiles;
    public String hour;
    public String date;
    private int tp;

    public ResultList(String key, ArrayList<SimpleDetail> value, String date, int tp) {
        this.hour = key;
        this.resultMobiles = value;
        this.date = date;
        this.tp = tp;
    }

    public ArrayList<SimpleDetail> getResultMobiles() {
        return resultMobiles;
    }

    public void setResultMobiles(ArrayList<SimpleDetail> resultMobiles) {
        this.resultMobiles = resultMobiles;
    }

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
