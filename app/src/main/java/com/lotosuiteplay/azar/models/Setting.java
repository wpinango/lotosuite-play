package com.lotosuiteplay.azar.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.util.FingerPrintDetection;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;
import com.lotosuiteplay.azar.util.notification.NotificationsConfig;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by wpinango on 9/5/17.
 */

public class Setting {
    private String title;
    private boolean status;
    private String TAG;

    public void setTAG(String TAG){
        this.TAG = TAG;
    }

    public String getTAG(){
        return TAG;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static Setting getSettingConfig(Context context, String notificationKey) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Global.userId, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("configNotification", "");

        Setting s = new Setting();

        ArrayList<Setting> settings;

        if (value.equals("")) {
            settings = setSettings(context);
        } else {
            settings = getSettings(context);
        }

        for (Setting setting : settings) {
            if (setting.getTAG().equals(notificationKey)) {
                s = setting;
            }
        }
        return s;

    }

    public static void saveSetting(Context context, Setting s) {
        ArrayList<Setting> settings = getSettingsConfig(context);
        for (Setting setting: settings) {
            if (s.getTAG().equals(setting.getTAG())) {
                setting.setStatus(s.status);
            }
        }
        NotificationsConfig notificationsConfig = new NotificationsConfig();
        notificationsConfig.setNotificationConfig(context, new Gson().toJson(settings));
    }

    public static ArrayList<Setting> getSettingsConfig(Context context) {
        NotificationsConfig notificationsConfig = new NotificationsConfig();
        ArrayList<Setting> settings;

        if (notificationsConfig.getNotification(context).equals("")) {
            settings = setSettings(context);
        } else {
            settings = getSettings(context);
        }

        return settings;
    }

    private static ArrayList<Setting> setSettings(Context context){
        ArrayList<Setting> settings = new ArrayList<>();
        Setting sound = new Setting();
        sound.setTitle(Global.NAME_NOTIFICATION_SOUND);
        sound.setTAG(SharedPreferenceConstants.KEY_NOTIFICATION_SOUND);
        sound.setStatus(true);
        Setting notification = new Setting();
        notification.setStatus(true);
        notification.setTAG(SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION);
        notification.setTitle(Global.NAME_NOTIFICATION);
        Setting showRaffle = new Setting();
        showRaffle.setStatus(true);
        showRaffle.setTAG(SharedPreferenceConstants.KEY_SHOW_RAFFE);
        showRaffle.setTitle(Global.NAME_RAFFLE);
        if (FingerPrintDetection.detectFingerPrintSensor(context)) {
            Setting blockApp = new Setting();
            blockApp.setStatus(false);
            blockApp.setTAG(SharedPreferenceConstants.KEY_FINGER);
            blockApp.setTitle(Global.NAME_FINGER_OPTION);
            settings.add(blockApp);
        }
        settings.add(notification);
        settings.add(sound);
        settings.add(showRaffle);
        NotificationsConfig notificationsConfig = new NotificationsConfig();
        notificationsConfig.setNotificationConfig(context, new Gson().toJson(settings));
        return settings;
    }

    private static ArrayList<Setting> getSettings(Context context) {
        ArrayList<Setting> settings = new ArrayList<>();
        settings.clear();
        NotificationsConfig notificationsConfig = new NotificationsConfig();
        Setting[] s = new Gson().fromJson(notificationsConfig.getNotification(context), Setting[].class);
        settings.addAll(Arrays.asList(s));
        return settings;
    }

}
