package com.lotosuiteplay.azar.models;

public class TriplePlayWinner {

    private String nick;
    private String amount;
    private String percent;

    public String getName() {
        return nick;
    }

    public void setName(String nick) {
        this.nick = nick;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }
}
