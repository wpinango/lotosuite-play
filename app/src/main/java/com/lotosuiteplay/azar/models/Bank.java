package com.lotosuiteplay.azar.models;

/**
 * Created by wpinango on 8/4/17.
 */

public class Bank {
    private String name;
    private String icon;
    private String account;
    private int accountId;
    private int id;
    private String owner;
    private String idn;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getIdn() {
        return idn;
    }

    public void setIdn(String idn) {
        this.idn = idn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAccountId(){
        return accountId;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setIcon (String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }
}
