package com.lotosuiteplay.azar.models;

import com.google.gson.annotations.SerializedName;

public class TriplePlayWinnerHistory {
    @SerializedName("id")
    private int id;

    @SerializedName("ticket_id")
    private long ticketId;

    @SerializedName("status")
    private int status;

    @SerializedName("raffle_hour")
    private int raffleHour;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("serial")
    private long serial;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updateAt;

    @SerializedName("award")
    private float award;

    @SerializedName("investment")
    private float investment;

    @SerializedName("percent")
    private float percent;

    @SerializedName("nick")
    private String nick;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRaffleHour() {
        return raffleHour;
    }

    public void setRaffleHour(int raffleHour) {
        this.raffleHour = raffleHour;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getSerial() {
        return serial;
    }

    public void setSerial(long serial) {
        this.serial = serial;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public float getAward() {
        return award;
    }

    public void setAward(float award) {
        this.award = award;
    }

    public float getInvestment() {
        return investment;
    }

    public void setInvestment(float investment) {
        this.investment = investment;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }
}
