package com.lotosuiteplay.azar.models;

import java.util.ArrayList;

public class TriplePlayData {

    private Double acumulated;
    private Long timeStampLastWin;
    private Integer raffleSerial;
    private ArrayList<TriplePlayWinner> lastTriplePlayWinners = new ArrayList<>();

    public Double getAcumulated() {
        return acumulated;
    }

    public void setAcumulated(Double acumulated) {
        this.acumulated = acumulated;
    }

    public Long getTimeStampLastWin() {
        return timeStampLastWin;
    }

    public void setTimeStampLastWin(Long timeStampLastWin) {
        this.timeStampLastWin = timeStampLastWin;
    }

    public Integer getRaffleSerial() {
        return raffleSerial;
    }

    public void setRaffleSerial(Integer raffleSerial) {
        this.raffleSerial = raffleSerial;
    }

    public ArrayList<TriplePlayWinner> getLastTriplePlayWinners() {
        return lastTriplePlayWinners;
    }

    public void setLastTriplePlayWinners(ArrayList<TriplePlayWinner> lastTriplePlayWinners) {
        this.lastTriplePlayWinners = lastTriplePlayWinners;
    }
}
