package com.lotosuiteplay.azar.models;

import androidx.annotation.NonNull;

import com.lotosuiteplay.azar.Global;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wpinango on 05/06/17.
 */

public class Chip implements Comparable<Chip>{
    public String chipLabel;
    public int id;
    public String description;
    public String quota;
    public int gameId;
    public boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int compareTo(@NonNull Chip chip) {
        return Integer.valueOf(this.chipLabel).compareTo(Integer.valueOf(chip.chipLabel));
    }

    public static boolean isChipSelected() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    return true;
                }
            }
        }
        return false;
    }

    public static void clearChipSelection() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                chip.setSelected(false);
                //Global.chipSelect.put(Integer.valueOf(chipLabel.chipLabel),false);
            }
        }
    }

    public static String getChipSelected() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    return chip.chipLabel;
                }
            }
        }
        return null;
    }

    public static Chip getChipByCurrentsGame (LinkedHashMap<String,Game> games, int position) {
        Game game;
        for (Map.Entry<String, Game> a :games.entrySet()) {
            game = a.getValue();
            switch (game.getCodeName()) {
                case Global.KEY_SAF:
                    return game.getChips().get(String.valueOf(position));
                case Global.KEY_TIZ:
                    return game.getChips().get(String.valueOf(position));
                case Global.KEY_LOT:
                    return game.getChips().get(String.valueOf(position));
            }
        }
        return null;
    }

    public static int getChipByGameMaxPosition(LinkedHashMap<String, Game> currentGames) {
        int temp = 0;
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            //temp++;
            if (a.getValue().getChips().size() > temp) {
                temp = a.getValue().getChips().size();
            }
            //Math.max(a.getValue().getChips().size(), temp);
        }
        return temp;
    }

    public static int getNumberChipsSelected (LinkedHashMap<String, Game> currentGames) {
        Game game;
        int count = 0;
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    count++;
                }
            }
        }
        return count;
    }

    public static int getNumberChipsSelectedPerGames(LinkedHashMap<String, Game> currentGames, String g) {
        Game game;
        int count = 0;
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            game = a.getValue();
            if (g.equals(game.getCodeName())) {
                for (Chip chip : game.getChips().values()) {
                    if (chip.isSelected()) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public static int getChipsSizePerGame(LinkedHashMap<String, Game> currentGames, String codeName) {
        int size = -1;
        for (Map.Entry<String, Game> g : currentGames.entrySet()) {
            if (g.getValue().getCodeName().equals(codeName)) {
                size = g.getValue().getChips().size();
            }
        }
        return size;
    }

    public static int getChipGrid(LinkedHashMap<String, Game> currentGames){
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            return a.getValue().getChips().size();
        }
        return 0;
    }

    public static int getResourceChip(String chip, String codeName) {
        int resource = -1;
        int index = -1;
        switch (chip) {
            case "0":
                index = 0;
                break;
            case "00":
                index = 1;
                break;
            case "000":
                index = 2;
                break;
            default:
                index = Integer.valueOf(chip) + 2;
                break;
        }
        switch (codeName) {
            case Global.KEY_LOT:
                resource = Global.newLotoImages[index];
                break;
            case Global.KEY_SAF:
                resource = Global.newSafariImages[index];
                break;
            case Global.KEY_TIZ:
                resource = Global.newTizanaImages[index];
                break;
        }
        return resource;
    }
}
