package com.lotosuiteplay.azar.models;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;

import java.util.HashMap;
import java.util.Map;

public class Rule {
    private String version;
    private String title;
    private String content;
    private boolean isAccepted;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public static void requestGameRules(AsyncktaskListener listener) {
        Map<String, String > headers = new HashMap<>();
        headers.put(Global.KEY_SCHEMA, "movilx");
        new GlobalAsyntask.GetMethodAsynctask(Global.URL_GET_GAME_RULES, headers, listener).execute();
    }

    public static void requestTermsConditions(AsyncktaskListener listener) {
        Map<String, String > headers = new HashMap<>();
        headers.put(Global.KEY_SCHEMA, "movilx");
        new GlobalAsyntask.GetMethodAsynctask(Global.URL_GET_TERM_CONDITIONS, headers, listener).execute();
    }
}
