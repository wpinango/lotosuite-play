package com.lotosuiteplay.azar.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.common.FourDigitBankAccountFormatWatcher;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.BankList;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/7/17.
 */

public class BankListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Bank> banks;
    private Context context;

    public BankListAdapter(Context context, ArrayList<Bank> banks){
        this.inflater = LayoutInflater.from(context);
        this.banks = banks;
        this.context = context;
    }

    @Override
    public int getCount() {
        return banks.size();
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_producer_bank, null);
        }
        ImageButton btnAccountCopy = view.findViewById(R.id.imageButton14);
        ImageButton btnOwnerCopy = view.findViewById(R.id.imageButton15);
        ImageButton btnDniCopy = view.findViewById(R.id.imageButton16);
        CardView cvComment = view.findViewById(R.id.cv_bank);
        TextView tvAccountType = view.findViewById(R.id.tv_account_type);
        TextView tvBankName = view.findViewById(R.id.tv_name_producer);
        TextView tvAccountNumber = view.findViewById(R.id.tv_comment_account_number);
        TextView tvOwner = view.findViewById(R.id.tv_owner);
        TextView tvIdn = view.findViewById(R.id.tv_idn);
        ImageView ivBank = view.findViewById(R.id.iv_bank);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        try {
            tvAccountNumber.setText(banks.get(position).getAccount().replaceAll("-",""));
            tvBankName.setText(banks.get(position).getName());
            cvComment.setCardBackgroundColor(Color.WHITE);
            ivBank.setImageResource(BankList.getBankImages(context, banks.get(position).getIcon()));
            tvOwner.setText(banks.get(position).getOwner());
            tvIdn.setText(banks.get(position).getIdn());
            tvAccountType.setText(banks.get(position).getType());
            btnAccountCopy.setOnClickListener(view1 -> {
                ClipData clip = ClipData.newPlainText("Cuenta", banks.get(position).getAccount());
                clipboard.setPrimaryClip(clip);
                Global.Toaster.get().showToast(context, "Cuenta copiada al portapapeles", Toast.LENGTH_SHORT);
            });
            btnDniCopy.setOnClickListener(view12 -> {
                ClipData clip = ClipData.newPlainText("Cedula", banks.get(position).getIdn());
                clipboard.setPrimaryClip(clip);
                Global.Toaster.get().showToast(context, "Rif copiado al portapapeles", Toast.LENGTH_SHORT);
            });
            btnOwnerCopy.setOnClickListener(view13 -> {
                ClipData clip = ClipData.newPlainText("Nombre", banks.get(position).getOwner());
                clipboard.setPrimaryClip(clip);
                Global.Toaster.get().showToast(context, "Nombre copiado al portapapeles", Toast.LENGTH_SHORT);
            });
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
