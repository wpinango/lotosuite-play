package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Ticket;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/23/17.
 */

public class TicketListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Ticket> tickets;

    public TicketListAdapter(Context cContexto, ArrayList<Ticket> tickets) {
        this.context = cContexto;
        this.tickets = tickets;
    }

    @Override
    public int getCount() {
        return tickets.size();
    }

    @Override
    public Object getItem(int position) {
        return tickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_ticket, parent, false);
        }
        Ticket ticket = tickets.get(position);
        CardView cardView = (CardView) itemView.findViewById(R.id.cv_ticket);
        TextView tvTitle = (TextView) itemView.findViewById(R.id.tv_ticket);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_prize);
        try {
            tvTitle.setText("Ticket: " + tickets.get(position).label + "\n" + tickets.get(position).details.getTicketDate()
            + " " + tickets.get(position).details.getTicketTime());
            cardView.setBackgroundColor(Color.WHITE);
            if (tickets.get(position).details.getStatus() == 0) {
                imageView.setImageResource(R.drawable.ic_receipt_black_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 1) {
                imageView.setImageResource(R.drawable.prize);
            }
            else if (tickets.get(position).details.getStatus() == 2) {
                imageView.setImageResource(R.drawable.ic_close_red_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 3) {
                imageView.setImageResource(R.drawable.ic_check_green_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 4) {
                imageView.setImageResource(R.drawable.ic_receipt_gray_36dp);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        if (Global.selectedPosition == position){
            cardView.setBackgroundResource(R.color.darkGrayBackground);
            tvTitle.setTextColor(context.getResources().getColor(R.color.yellowBackground));
            if (tickets.get(position).details.getStatus() == 0) {
                imageView.setImageResource(R.drawable.ic_receipt_yellow_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 1) {
                imageView.setImageResource(R.drawable.prize);
            }
            else if (tickets.get(position).details.getStatus() == 2) {
                imageView.setImageResource(R.drawable.ic_close_yellow_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 3) {
                imageView.setImageResource(R.drawable.ic_check_yellow_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 4) {
                imageView.setImageResource(R.drawable.ic_receipt_yellow_36dp);
            }
        }else {
            cardView.setBackgroundColor(Color.WHITE);
            tvTitle.setTextColor(context.getResources().getColor(R.color.black));
            if (tickets.get(position).details.getStatus() == 0) {
                imageView.setImageResource(R.drawable.ic_receipt_black_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 1) {
                imageView.setImageResource(R.drawable.prize);
            }
            else if (tickets.get(position).details.getStatus() == 2) {
                imageView.setImageResource(R.drawable.ic_close_red_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 3) {
                imageView.setImageResource(R.drawable.ic_check_green_36dp);
            }
            else if (tickets.get(position).details.getStatus() == 4) {
                imageView.setImageResource(R.drawable.ic_receipt_gray_36dp);
            }
        }
        return itemView;
    }

    public String getSerial(int position){
        return tickets.get(position).label;
    }

    public void setArrayListUpdate(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
        notifyDataSetChanged();
    }
}
