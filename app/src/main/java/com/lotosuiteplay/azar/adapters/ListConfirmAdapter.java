package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.models.SellItem;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.Format;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/27/17.
 */

public class ListConfirmAdapter extends BaseAdapter{
    private ArrayList<SellItem> sellItems;
    private Context Contexto;
    private boolean isConfirmCalled;

    public ListConfirmAdapter (Context cContexto, ArrayList<SellItem> sellItems, boolean isConfirmCalled) {
        this.Contexto = cContexto;
        this.sellItems = sellItems;
        this.isConfirmCalled = isConfirmCalled;
    }
    @Override
    public int getCount() {
        return sellItems.size();
    }

    @Override
    public Object getItem(int position) {
        return sellItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        SellItem sellItem = sellItems.get(position);
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) Contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_list, parent, false);
        }
        TextView tvName = itemView.findViewById(R.id.lblNombr);
        ImageView imageCheck = itemView.findViewById(R.id.imgFot);
        if (isConfirmCalled) {
            imageCheck.setImageResource(R.color.transparent);
        } else {
            if (sellItem.effectiveSell == sellItem.amount) {
                imageCheck.setImageResource(R.drawable.ok);
            } else if (sellItem.raffle <= Time.getCurrentTime() || sellItem.effectiveSell == 0) {
                imageCheck.setImageResource(R.drawable.no);
            }
        }
        String gameToUpperCase = sellItem.game.substring(0, 1).toUpperCase() + sellItem.game.substring(1);
        tvName.setText(gameToUpperCase + " " + Format.getRaffleHour(sellItem.raffle) +
                " \n[ " + sellItem.chip + " ] " + sellItem.description + " x " + sellItem.amount);
        return itemView;
    }
}
