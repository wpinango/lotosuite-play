package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.SimpleDetail;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.util.Util;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/23/17.
 */

public class TicketListDetailAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SimpleDetail> simpleDetails;

    public TicketListDetailAdapter(Context cContexto, ArrayList<SimpleDetail> simpleDetails) {
        this.context = cContexto;
        this.simpleDetails = simpleDetails;
    }

    @Override
    public int getCount() {
        return simpleDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return simpleDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_ticket_detail, parent, false);
        }
        SimpleDetail simpleDetail = simpleDetails.get(position);
        CardView cardView = itemView.findViewById(R.id.cv_ticket);
        TextView tv_title = itemView.findViewById(R.id.tv_ticket);
        ImageView imPlay = itemView.findViewById(R.id.img_play);
        try {
            String [] temp = simpleDetail.getLabel().split("] ");
            String[] chipDescription = temp[1].split(" ");
            imPlay.setImageResource(context.getResources().getIdentifier(Util.stripAccents(chipDescription[0]).toLowerCase().trim(),
                    "drawable",context.getPackageName()));
            if (chipDescription[0].equals(Global.KEY_PINEAPPLE)){
                imPlay.setImageResource(R.drawable.pina);
            }
            tv_title.setText(simpleDetail.getLabel());
            if (simpleDetail.getStatus() == 0 && simpleDetail.getProcessed() == 0) {
                cardView.setBackgroundColor(Color.WHITE);
            } else if (simpleDetail.getStatus() == 1 && simpleDetail.getProcessed() == 1) {
                cardView.setBackgroundResource(R.color.colorAward);
            } else if (simpleDetail.getStatus() == 0 && simpleDetail.getProcessed() == 1) {
                cardView.setBackgroundResource(R.color.red_processed);
            } else if (simpleDetail.getStatus() == 2) {
                cardView.setBackgroundResource(R.color.gray);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return itemView;
    }
}
