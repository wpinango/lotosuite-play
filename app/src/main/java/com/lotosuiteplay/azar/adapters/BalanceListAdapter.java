package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.models.Balance;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/27/17.
 */

public class BalanceListAdapter extends BaseAdapter{
    private LayoutInflater inflter;
    private ArrayList<Balance> myBalances;
    private String operationType;

    public BalanceListAdapter(Context context, ArrayList<Balance> myBalances){
        inflter = LayoutInflater.from(context);
        this.myBalances = myBalances;
    }

    @Override
    public int getCount() {
        return myBalances.size();
    }

    @Override
    public Object getItem(int position) {
        return myBalances.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflter.inflate(R.layout.item_list_balance1, null);
        }
        CardView cvRecharge = view.findViewById(R.id.cv_balance);
        TextView tvDate = view.findViewById(R.id.tv_name_comment);
        TextView tvAmount = view.findViewById(R.id.tv_amount_balance);
        TextView tvType = view.findViewById(R.id.tv_type_balance);
        TextView tvReference = view.findViewById(R.id.tv_reference_balance);
        TextView tvBalance = view.findViewById(R.id.tv_balance);
        try {
            switch (myBalances.get(position).type) {
                case 1:
                    operationType = "RB";
                    break;
                case 2:
                    operationType = "CT";
                    break;
                case 3:
                    operationType = "TJ";
                    break;
                case 4:
                    operationType = "TP";
                    break;
                case 5:
                    operationType = "TA";
                    break;
                case 6:
                    operationType = "SR";
                    break;
                case 7:
                    operationType = "ST";
                    break;
                case 8:
                    operationType = "RE";
                    break;
                case 9:
                    operationType = "CE";
                    break;
                case 10:
                    operationType = "3P";
                    break;
                case 12:
                    operationType = "SE";
                    break;
            }
            if (!myBalances.get(position).operationDate.contains("\n")) {
                String date = myBalances.get(position).operationDate;
                String buildDateView = date.split(" ")[0] + " \n" + date.split(" ")[1];
                tvDate.setText(buildDateView);
            } else {
                tvDate.setText(myBalances.get(position).operationDate);
            }
            tvAmount.setText(Format.getCashFormat(myBalances.get(position).amount));
            tvType.setText(operationType);
            tvReference.setText(myBalances.get(position).reference);
            tvBalance.setText(Format.getCashFormat(myBalances.get(position).getBalance()));
            if (myBalances.get(position).reference == null) {
                tvReference.setText("0000" + (position));
            }
            cvRecharge.setBackgroundResource(R.color.white);
            if (position %2 == 0) {
                cvRecharge.setBackgroundResource(R.color.colorSelect);
            }
        } catch (Exception e) {

        }
        return view;
    }
}
