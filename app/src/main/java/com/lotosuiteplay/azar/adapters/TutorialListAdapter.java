package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Tutorial;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/17/17.
 */

public class TutorialListAdapter extends BaseAdapter{
    private ArrayList<Tutorial> tutorials;
    private Context context;
    private LayoutInflater inflater;

    public TutorialListAdapter(Context context, ArrayList<Tutorial> tutorials){
        this.context = context;
        this.tutorials = tutorials;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return tutorials.size();
    }

    @Override
    public Object getItem(int i) {
        return tutorials.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_tutorial, null);
        }
        Tutorial tutorial = tutorials.get(i);
        CardView cvTutorial = view.findViewById(R.id.cv_tutorial);
        TextView tvTitle = view.findViewById(R.id.tv_title_tutorial);
        try {
            tvTitle.setText(tutorial.getTitle());
        } catch (Exception e) {

        }
        return view;
    }
}
