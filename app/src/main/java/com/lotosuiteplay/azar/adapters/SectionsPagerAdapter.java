package com.lotosuiteplay.azar.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.lotosuiteplay.azar.fragments.NotificationsFragment;
import com.lotosuiteplay.azar.fragments.BalanceFragment;
import com.lotosuiteplay.azar.fragments.PrimaryFragment;
import com.lotosuiteplay.azar.fragments.ResultFragment;
import com.lotosuiteplay.azar.fragments.StatisticsFragment;
import com.lotosuiteplay.azar.fragments.TicketFragment;
import com.lotosuiteplay.azar.fragments.TriplePlayFragment;

/**
 * Created by wpinango on 6/1/17.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private PrimaryFragment primaryFragment = new PrimaryFragment();
    private TicketFragment ticketFragment = new TicketFragment();
    private NotificationsFragment notificationsFragment = new NotificationsFragment();
    private BalanceFragment balanceFragment = new BalanceFragment();
    private ResultFragment resultFragment = new ResultFragment();
    private TriplePlayFragment triplePlayFragment = new TriplePlayFragment();
    private StatisticsFragment statisticsFragment = new StatisticsFragment();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return primaryFragment;
            case 1:
                return ticketFragment;
            case 2:
                return resultFragment;
            case 3:
                return triplePlayFragment;
            case 4:
                return balanceFragment;
            case 5:
                return statisticsFragment;
            case 6:
                return notificationsFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Jugar";
            case 1:
                return "Mis Jugadas";
            case 2:
                return "Resultados";
            case 4:
                return "Mi Balance";
            case 3:
                return "Triple Play";
            case 6:
                return "Notificaciones";
            case 5:
                return "Estadisticas";
        }
        return null;
    }

    public void setCash() {
        try {
            primaryFragment.setCash();
        }catch (Exception e ) {
            e.getMessage();
        }
    }

    public void requestResultList(){
        resultFragment.refreshContent();
    }

    /*public void requestPendingPayments(){
        paymentFragment.requestPendingTransaction();
    }*/

    public void refreshBalance(){
        balanceFragment.refreshContent();
    }

    public void refreshTickets(){
        ticketFragment.refreshContent();
    }

    public void refreshRafflesAndChips(){
        primaryFragment.refreshContent();
    }

    /*public void requestPendingRecharge(){
        rechargeFragment.requestPendingTransaction();
    }*/

    public void updateBalanceAmount(){
        balanceFragment.updateBalanceAmounts();
    }

    /*public void setRechargeCash(){
        rechargeFragment.setNewCash();
    }*/

    /*public void updateRechargeCash(){
        rechargeFragment.setNewCash();
    }*/

    public void updateNotificationReceive(String value){
        notificationsFragment.updateUIOnReceiverValue2(value);
    }

    public void updateTriplePlayData(){
        triplePlayFragment.requestAccumulated();
    }
}
