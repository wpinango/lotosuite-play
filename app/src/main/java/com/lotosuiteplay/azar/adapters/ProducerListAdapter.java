package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.BankList;
import com.lotosuiteplay.azar.models.Producer;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/7/17.
 */

public class ProducerListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Producer> producers;
    private Context context;

    public ProducerListAdapter(Context context, ArrayList<Producer> producers) {
        this.inflater = LayoutInflater.from(context);
        this.producers = producers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return producers.size();
    }

    @Override
    public Object getItem(int position) {
        return producers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_producer, null);
        }
        final Producer producer = producers.get(position);
        TextView tvNameProducer = view.findViewById(R.id.tv_name_producer);
        TextView tvRating = view.findViewById(R.id.tv_rating_producer_signup);
        TextView tvUsersCount = view.findViewById(R.id.tv_users_count);
        CardView cardView = view.findViewById(R.id.cv_producer_signup);
        LinearLayout linearLayout = view.findViewById(R.id.ly_bank);
        try {
            linearLayout.removeAllViews();
            for (int i = 0; i < producer.getBanks().size(); i++) {
                Bank bank = producer.getBanks().get(i);
                ImageView iv = new ImageView(context);
                iv.setLayoutParams(new LinearLayout.LayoutParams(90, 90));
                iv.setImageResource(BankList.getBankImages(context, bank.getIcon().trim()));
                linearLayout.addView(iv);
            }
            tvUsersCount.setText(producer.getUsersCount());
            tvNameProducer.setText(producer.getName());
            tvRating.setText(Format.getFormattedStringOneDecimal(producer.getScore()));
            cardView.setCardBackgroundColor(Color.WHITE);
            view.setOnClickListener(v -> {
                Producer.selectProducer(producers, position);
                notifyDataSetChanged();
            });
            linearLayout.setOnClickListener(view1 -> {
                Producer.selectProducer(producers, position);
                notifyDataSetChanged();
            });
            if (producer.isSelected()) {
                cardView.setCardBackgroundColor(Color.LTGRAY);
            } else {
                cardView.setCardBackgroundColor(Color.WHITE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
