package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.BankList;

import java.util.ArrayList;

public class SpinnerRechargeAdapter extends ArrayAdapter<Bank> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Bank> banks;

    public SpinnerRechargeAdapter(Context context, int resource, ArrayList<Bank> banks) {
        super(context, resource, banks);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.banks = banks;
    }




    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if(row == null) {
            row = inflater.inflate(R.layout.spinner_layout, parent, false);
        }

        TextView tvTitle = row.findViewById(R.id.tv_spinner_title);
        TextView tvAccountNumber = row.findViewById(R.id.tv_account_spinner);
        ImageView imageView = row.findViewById(R.id.img_spinner);

        try {
            tvTitle.setText(banks.get(position).getName());
            tvAccountNumber.setText(banks.get(position).getAccount());
            imageView.setImageResource(BankList.getBankImages(context, banks.get(position).getAccount().substring(0,4)));
        } catch (Exception e) {

        }

        return row;
    }

    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        View row = convertView;
        if(row == null) {
            row = inflater.inflate(R.layout.spinner_dropdown_view, parent, false);
        }

        TextView tvTitle = row.findViewById(R.id.tv_spinner_title2);
        TextView tvAccountNumber = row.findViewById(R.id.tv_account_spinner2);
        ImageView imageView = row.findViewById(R.id.img_spinner2);

        try {
            tvTitle.setText(banks.get(position).getName());
            tvAccountNumber.setText(banks.get(position).getAccount());
            imageView.setImageResource(BankList.getBankImages(context, banks.get(position).getAccount().substring(0,4)));
        } catch (Exception e) {

        }

        return row;
    }
}
