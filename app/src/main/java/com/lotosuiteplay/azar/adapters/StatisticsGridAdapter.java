package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.models.Statistics;
import com.lotosuiteplay.azar.util.Util;
import com.lotosuiteplay.azar.util.chart.Chart;

import java.util.ArrayList;

public class StatisticsGridAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Statistics> statistics;
    private Context context;
    private HorizontalBarChart chart;

    public StatisticsGridAdapter(Context context, ArrayList<Statistics> statistics) {
        this.inflater = LayoutInflater.from(context);
        this.statistics = statistics;
        this.context = context;
    }

    @Override
    public int getCount() {
        return statistics.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_statistics, viewGroup, false);
        }
        Statistics s = statistics.get(i);
        TextView tvLabel = view.findViewById(R.id.tv_label);
        ImageView imageView = view.findViewById(R.id.cv_chip);
        BootstrapLabel tvraffledOff = view.findViewById(R.id.tv_raffled_off);
        BootstrapLabel tvPercent = view.findViewById(R.id.tv_percent);
        chart = view.findViewById(R.id.chart);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setClickable(false);
        try {
            imageView.setImageResource(context.getResources().getIdentifier(Util.stripAccents(s.getLabel().split("-")[1]).toLowerCase().trim(),
                    "drawable", context.getPackageName()));
            if (s.getLabel().split("-")[1].trim().equals(Global.KEY_PINEAPPLE)){
                imageView.setImageResource(R.drawable.pina);
            }
            String percent = s.getPercent() == null ? "0" : s.getPercent();
            Chart.paintChipChart(chart, Float.valueOf(percent));
            tvLabel.setText(s.getLabel());
            tvraffledOff.setText(String.valueOf(s.getRaffled()));
            tvPercent.setText(Format.getFormattedStringOneDecimal(Float.valueOf(percent)) + "%");
        } catch (Exception e) {

        }
        return view;
    }

}
