package com.lotosuiteplay.azar.adapters.GameListAdapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/25/17.
 */

public class GameListAdapter extends BaseAdapter {
    private Context Contexto;
    private ArrayList<String> Game;
    private int[] Foto;
    private LayoutInflater inflater;
    private CardView cardView;


    public GameListAdapter(Context cContexto, ArrayList<String> game, int[] cFoto) {
        this.Contexto = cContexto;
        this.Game = game;
        this.Foto = cFoto;
    }

    @Override
    public int getCount() {
        return Game.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        TextView Game;
        ImageView Check;
        if (itemView == null) {
            inflater = (LayoutInflater) Contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_list_games, parent, false);
        }
        cardView = itemView.findViewById(R.id.cv_comment);
        Game = itemView.findViewById(R.id.tv_game_game);
        Check = itemView.findViewById(R.id.iv_game);
        try {
            Game.setText(this.Game.get(position));
            Check.setImageResource(Foto[position]);
            if (Global.mycheked.get(position).equals(true)) {
                cardView.setBackgroundResource(R.color.colorSelectBlueTransparen);
            } else if (Global.mycheked.get(position).equals(false)) {
                cardView.setBackgroundResource(R.color.white);
            }
        } catch (Exception e) {

        }
        return itemView;
    }
}
