package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.models.TriplePlayWinner;

import java.util.ArrayList;

public class TriplePlayWinnersListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<TriplePlayWinner> triplePlayWinners;

    public TriplePlayWinnersListAdapter(Context context, ArrayList<TriplePlayWinner> triplePlayWinners) {
        this.inflater = LayoutInflater.from(context);
        this.triplePlayWinners = triplePlayWinners;
    }

    @Override
    public int getCount() {
        return triplePlayWinners.size();
    }

    @Override
    public Object getItem(int i) {
        return triplePlayWinners.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_winners, viewGroup, false);
        }
        TextView  tvName, tvAmount, tvPercent;
        tvName = view.findViewById(R.id.tv_winner_name);
        tvAmount = view.findViewById(R.id.tv_winner_amount);
        tvPercent = view.findViewById(R.id.tv_winner_percent);
        try {
            tvAmount.setText(Format.getCashFormat(Float.valueOf(triplePlayWinners.get(i).getAmount())));
            tvName.setText(triplePlayWinners.get(i).getName());
            tvPercent.setText(Format.getFormattedStringOneDecimal(Float.valueOf(triplePlayWinners.get(i).getPercent())) + "%");
            if (i %2 == 0) {
                view.setBackgroundResource(R.color.white);
            } else {
                view.setBackgroundResource(R.color.colorSelectBlueTransparen);
            }
        } catch (Exception e) {

        }
        return view;
    }
}
