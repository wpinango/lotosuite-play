package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.interfaces.SpinnerInterface;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.BankList;
import com.lotosuiteplay.azar.models.Producer;

import java.util.ArrayList;

public class SpinnerProducerAdapter extends ArrayAdapter<Producer> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Producer> producers;
    private SpinnerInterface spinnerInterface;

    public SpinnerProducerAdapter(Context context, int resource, ArrayList<Producer> producers, SpinnerInterface spinnerInterface) {
        super(context, resource, producers);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.producers = producers;
        this.spinnerInterface = spinnerInterface;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.item_list_producer, parent, false);
        }
        final Producer producer = producers.get(position);
        TextView tvNameProducer = row.findViewById(R.id.tv_name_producer);
        TextView tvRating = row.findViewById(R.id.tv_rating_producer_signup);
        TextView tvUsersCount = row.findViewById(R.id.tv_users_count);
        CardView cardView = row.findViewById(R.id.cv_producer_signup);
        LinearLayout linearLayout = row.findViewById(R.id.ly_bank);
        try {
            linearLayout.removeAllViews();
            for (int i = 0; i < producer.getBanks().size(); i++) {
                Bank bank = producer.getBanks().get(i);
                ImageView iv = new ImageView(context);
                iv.setLayoutParams(new LinearLayout.LayoutParams(90, 90));
                iv.setImageResource(BankList.getBankImages(context, bank.getIcon().trim()));
                linearLayout.addView(iv);
            }
            tvUsersCount.setText(producer.getUsersCount());
            tvNameProducer.setText(producer.getName());
            tvRating.setText(Format.getFormattedStringOneDecimal(producer.getScore()));
            cardView.setCardBackgroundColor(Color.WHITE);
        } catch (Exception e) {
            e.getMessage();
        }

        return row;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.spinner_producer_view, parent, false);
        }
        EditText etProducer = row.findViewById(R.id.et_producer);
        etProducer.setText("algo");
        etProducer.setOnClickListener(view -> spinnerInterface.onClick());
        try {
            etProducer.setText(producers.get(position).getName());
        } catch (Exception e) {

        }
        return row;
    }
}
