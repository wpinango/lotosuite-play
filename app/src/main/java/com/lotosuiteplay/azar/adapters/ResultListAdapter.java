package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.models.Chip;
import com.lotosuiteplay.azar.models.ResultList;
import com.lotosuiteplay.azar.models.SimpleDetail;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.util.Util;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/22/17.
 */

public class ResultListAdapter extends BaseAdapter {
    private ArrayList<ResultList> resultLists;
    private LayoutInflater inflter;
    private int awardCount = 0;
    private Context context;

    public ResultListAdapter(Context context, ArrayList<ResultList> resultLists) {
        this.resultLists = resultLists;
        inflter = (LayoutInflater.from(context));
        this.context = context;
    }

    @Override
    public int getCount() {
        return resultLists.size();
    }

    @Override
    public Object getItem(int position) {
        return resultLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflter.inflate(R.layout.item_list_result, null);
        }
        TextView tvHour = view.findViewById(R.id.tv_raffle_result);
        ImageView cvSafari = view.findViewById(R.id.cv_safari);
        ImageView cvTizana = view.findViewById(R.id.cv_tizana);
        ImageView cvLoto = view.findViewById(R.id.cv_loto);
        TextView tvSafari = view.findViewById(R.id.tv_safari);
        TextView tvTizana = view.findViewById(R.id.tv_tizana_result);
        TextView tvLoto = view.findViewById(R.id.tv_loto_result);
        TextView tvDate = view.findViewById(R.id.tv_date_result);
        TextView tvNumberResult = view.findViewById(R.id.tv_number_result);
        CardView cardVSafari = view.findViewById(R.id.cv_safari_result);
        CardView cardVTizana = view.findViewById(R.id.cv_tizana_result);
        CardView cardVLoto = view.findViewById(R.id.cv_loto_result);
        CardView cardResult = view.findViewById(R.id.cv_chips);
        tvTizana.setText("Sin Resultado");
        cvTizana.setImageResource(R.color.white);
        tvSafari.setText("Sin Resultado");
        cvSafari.setImageResource(R.color.white);
        tvLoto.setText("Sin Resultado");
        cvLoto.setImageResource(R.color.white);
        try {
            ResultList rs = resultLists.get(position);
            String hour = Format.getRaffleHour(Integer.parseInt(rs.hour));
            tvHour.setText(hour);
            tvDate.setText(rs.date);
            for (SimpleDetail simpleDetail : rs.resultMobiles) {
                tvNumberResult.setText("Sorteo: " + simpleDetail.getSerial());
                String[] temp = simpleDetail.getLabel().split(" - ");
                String chipDescription = temp[1];
                switch (simpleDetail.getCodeName()) {
                    case "sgt-lot":
                        tvLoto.setText(simpleDetail.getLabel());
                        cvLoto.setImageResource(Chip.getResourceChip(Util.stripAccents(simpleDetail.getChip()), simpleDetail.getCodeName()));
                        //cvLoto.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim(),
                        //        "drawable",context.getPackageName()));
                        if (simpleDetail.getStatus() == 1) {
                            cardVLoto.setBackgroundResource(R.color.colorSelectBlueTransparen);
                        } else {
                            cardVLoto.setBackgroundResource(R.color.white);
                        }
                        if (simpleDetail.getTp() == 1){
                            awardCount++;
                        }
                        break;
                    case "sgt-tiz":
                        tvTizana.setText(simpleDetail.getLabel());
                        cvTizana.setImageResource(Chip.getResourceChip(Util.stripAccents(simpleDetail.getChip()), simpleDetail.getCodeName()));
                        //cvTizana.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim(),
                        //        "drawable",context.getPackageName()));
                        /*if (chipDescription.equals(Global.KEY_PINEAPPLE)){
                            cvTizana.setImageResource(R.drawable.pina);
                        }*/
                        if (simpleDetail.getStatus() == 1) {
                            cardVTizana.setBackgroundResource(R.color.colorSelectBlueTransparen);
                        } else {
                            cardVTizana.setBackgroundResource(R.color.white);
                        }
                        if (simpleDetail.getTp() == 1){
                            awardCount++;
                        }
                        break;
                    case "sgt-saf":
                        tvSafari.setText(simpleDetail.getLabel());
                        cvSafari.setImageResource(Chip.getResourceChip(Util.stripAccents(simpleDetail.getChip()), simpleDetail.getCodeName()));
                        //cvSafari.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim(),
                        //        "drawable",context.getPackageName()));
                        if (simpleDetail.getStatus() == 1) {
                            cardVSafari.setBackgroundResource(R.color.colorSelectBlueTransparen);
                        } else {
                            cardVSafari.setBackgroundResource(R.color.white);
                        }
                        if (simpleDetail.getTp() == 1){
                            awardCount++;
                        }
                        break;
                }
                if (awardCount >= 1){
                    cardResult.setBackgroundResource(R.color.colorAward);
                } else  {
                    cardResult.setBackgroundResource(R.color.white);
                }
            }
            if (rs.getTp() == 1) {
                cardResult.setBackgroundResource(R.color.colorAward);
            }
        } catch (Exception e) {
        }
        awardCount = 0;
        return view;
    }
}
