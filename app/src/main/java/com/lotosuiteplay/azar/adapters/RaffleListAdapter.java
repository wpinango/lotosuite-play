package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.models.RaffleLabel;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/16/17.
 */

public class RaffleListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<RaffleLabel> raffleLabels;
    private LayoutInflater inflater;
    private CardView cardView;
    public RaffleListAdapter(Context cContexto, ArrayList<RaffleLabel> raffleLabels)   {
        this.context = cContexto;
        this.raffleLabels = raffleLabels;
    }
    @Override
    public int getCount()
    {
        return raffleLabels.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent)    {
        TextView raffle;
        RaffleLabel raffleLabel = raffleLabels.get(position);
        if (itemView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_list3, parent, false);
        }
        cardView = (CardView)itemView.findViewById(R.id.cv_ticket);
        raffle = (TextView) itemView.findViewById(R.id.tv_ticket);
        raffle.setText("Sorteo: " + Format.getRaffleHour(raffleLabel.getHour()));
        if (raffleLabel.isSelected()) {
            cardView.setBackgroundResource(R.color.darkGrayBackground);
            raffle.setTextColor(context.getResources().getColor(R.color.yellowBackground));
        } else {
            cardView.setBackgroundResource(R.color.white);
            raffle.setTextColor(context.getResources().getColor(R.color.black));
        }
        return itemView;
    }
}
