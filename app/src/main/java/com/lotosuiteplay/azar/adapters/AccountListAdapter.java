package com.lotosuiteplay.azar.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.azar.common.FourDigitBankAccountFormatWatcher;
import com.lotosuiteplay.azar.models.AccountNumber;
import com.lotosuiteplay.azar.models.BankList;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/22/17.
 */

public class AccountListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<AccountNumber> accountNumbers;
    private Context context;

    public AccountListAdapter(Context context, ArrayList<AccountNumber> accountNumbers){
        this.inflater = LayoutInflater.from(context);
        this.accountNumbers = accountNumbers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return accountNumbers.size();
    }

    @Override
    public Object getItem(int position) {
        return accountNumbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_user_bank, null);
        }
        AccountNumber accountNumber = accountNumbers.get(position);
        CardView cvComment = view.findViewById(R.id.cv_bank);
        TextView tvBankName = view.findViewById(R.id.tv_name_producer);
        TextView tvAccountNumber = view.findViewById(R.id.tv_comment_account_number);
        TextView tvType = view.findViewById(R.id.tv_account_type);
        ImageView ivBank = view.findViewById(R.id.iv_bank);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvAccountNumber.setText(accountNumber.getAccountNumber());
            tvBankName.setText(accountNumber.getBank());
            ivBank.setImageResource(BankList.getBankImages(context, accountNumber.getAccountNumber().substring(0,4)));
            tvType.setText(accountNumbers.get(position).getType());
            if (accountNumber.isSelected()) {
                cvComment.setBackgroundResource(R.color.colorSelect);
            } else {
                cvComment.setBackgroundResource(R.color.white);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
