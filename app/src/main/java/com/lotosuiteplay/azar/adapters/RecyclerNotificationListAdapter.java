package com.lotosuiteplay.azar.adapters;

import android.graphics.Color;
import android.os.Handler;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wpinango on 9/8/17.
 */

public class RecyclerNotificationListAdapter extends RecyclerView.Adapter  {
    private static final int PENDING_REMOVAL_TIMEOUT = 3000;
    private ArrayList<Notification>items;
    List<Notification> itemsPendingRemoval;
    int lastInsertedIndex;
    boolean undoOn;
    ClickListener clickListener;

    private Handler handler = new Handler();
    HashMap<Notification, Runnable> pendingRunnables = new HashMap<>();

    public RecyclerNotificationListAdapter(ArrayList<Notification> notifications) {
        this.items = notifications;
        itemsPendingRemoval = new ArrayList<>();
        lastInsertedIndex = 15;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TestViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TestViewHolder viewHolder = (TestViewHolder)holder;
        final Notification item = items.get(position);

        holder.itemView.setOnClickListener(view -> {
            if (clickListener != null){
                clickListener.ItemClicked(view, position);
            }
        });
        if (itemsPendingRemoval.contains(item)) {
            viewHolder.itemView.setBackgroundColor(Color.RED);
            viewHolder.titleTextView.setVisibility(View.GONE);
            viewHolder.undoButton.setVisibility(View.VISIBLE);
            viewHolder.undoButton.setOnClickListener(v -> {
                Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                pendingRunnables.remove(item);
                if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                itemsPendingRemoval.remove(item);
                notifyItemChanged(items.indexOf(item));
            });
        } else {
            viewHolder.titleTextView.setVisibility(View.VISIBLE);
            viewHolder.titleTextView.setText(item.getTitle());
            viewHolder.tvHour.setText(item.getHour());
            viewHolder.undoButton.setVisibility(View.GONE);
            viewHolder.undoButton.setOnClickListener(null);
            if (item.isSelected()){
                viewHolder.cvNotification.setCardBackgroundColor(Color.LTGRAY);
            } else {
                viewHolder.cvNotification.setCardBackgroundColor(Color.WHITE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    public boolean isUndoOn() {
        return undoOn;
    }

    public void pendingRemoval(int position) {
        final Notification item = items.get(position);
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            notifyItemChanged(position);
            Runnable pendingRemovalRunnable = () -> remove(items.indexOf(item));
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }

    public void remove(int position) {
        Notification item = items.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
        }
        if (items.contains(item)) {
            notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        Notification item = items.get(position);
        return itemsPendingRemoval.contains(item);
    }


    static class TestViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView, tvHour;
        Button undoButton;
        CardView cvNotification;

        private TestViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view, parent, false));
            titleTextView = itemView.findViewById(R.id.title_text_view);
            undoButton = itemView.findViewById(R.id.undo_button);
            tvHour = itemView.findViewById(R.id.tv_hour_not);
            cvNotification = itemView.findViewById(R.id.cv_notification);
        }

    }
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void ItemClicked(View v, int position);
    }
}
