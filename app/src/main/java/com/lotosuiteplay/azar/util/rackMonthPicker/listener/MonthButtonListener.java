package com.lotosuiteplay.azar.util.rackMonthPicker.listener;

import com.lotosuiteplay.azar.util.rackMonthPicker.MonthRadioButton;

/**
 * Created by kristiawan on 31/12/16.
 */

public interface MonthButtonListener {
    public void monthClick(MonthRadioButton objectMonth);
}
