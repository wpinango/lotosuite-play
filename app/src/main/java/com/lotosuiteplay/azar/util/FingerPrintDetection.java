package com.lotosuiteplay.azar.util;

import android.content.Context;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

public class FingerPrintDetection {

    public static boolean detectFingerPrintSensor(Context context) {
        boolean isDetect = false;
        FingerprintManagerCompat fingerprintManagerCompat = FingerprintManagerCompat.from(context);

        if (!fingerprintManagerCompat.isHardwareDetected()) {
            // Device doesn't support fingerprint authentication
        } else if (!fingerprintManagerCompat.hasEnrolledFingerprints()) {
            // User hasn't enrolled any fingerprints to authenticate with
        } else {
            isDetect = true;
            // Everything is ready for fingerprint authentication
        }
        return isDetect;
    }
}
