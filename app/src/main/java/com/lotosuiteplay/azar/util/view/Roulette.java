package com.lotosuiteplay.azar.util.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.lotosuiteplay.azar.R;


/**
 * Created by wpinango on 9/8/17.
 */
public class Roulette extends View {

    private Bitmap ringFix;
    private Bitmap centerButton;
    private Bitmap ring1;
    private Bitmap ring2;
    private Bitmap dateSerial;
    private Bitmap resultBack;
    private Bitmap backGround;
    private Bitmap ring3;
    private Bitmap[] resulFrontImage;
    private int rotateOrigin;
    private Matrix ringFixMatrix;
    private Matrix dateSerialtMatrix;
    private Matrix resultBackMatrix;
    private Matrix backGroundMatrix;
    private Matrix[] resultFrontMatrix;
    private Matrix[] ringsMatrix;
    private float[] degrees;
    private int newSize;
    private float scaleSize;
    public final static int SIZE_ROULETTE = 36;
    public final static float RELATION = (360f / SIZE_ROULETTE);
    private int indexMAxRotate;
    private int floorDegree;
    private int rouletteDegree;
    private int currentValue;
    private int lastValue;
    private RouletteListener rouletteListener;
    private float scaleResult;
    private float scaleWidthDate;
    private float scaleHeigthDate;
    private float scaleWidhtBackGround;
    private float scaleHegthBackGround;
    private boolean drawResult;
    private String serialAndDate;

    private final float[] percentFront = new float[]{0.0714285f, 0.3928574f, 0.7071428f};
    private final float percentHeigthFront = 0.6333333333333f;

    public Roulette(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ringFix = BitmapFactory.decodeResource(getResources(), R.drawable.pieza_1);
        ring1 = BitmapFactory.decodeResource(getResources(), R.drawable.pieza_2);
        ring2 = BitmapFactory.decodeResource(getResources(), R.drawable.pieza_3);
        ring3 = BitmapFactory.decodeResource(getResources(), R.drawable.pieza_4);
        centerButton = BitmapFactory.decodeResource(getResources(), R.drawable.pieza_5);
        resultBack = BitmapFactory.decodeResource(getResources(), R.drawable.resultbackground);
        dateSerial = BitmapFactory.decodeResource(getResources(), R.drawable.dateserial);
        backGround = BitmapFactory.decodeResource(getResources(), R.drawable.background1);
        ringsMatrix = new Matrix[3];
        degrees = new float[3];
        resultFrontMatrix = new Matrix[3];
        resulFrontImage = new Bitmap[3];
        serialAndDate = "";
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int h = canvas.getHeight();
        int w = canvas.getWidth();
        int test = 0;
        test   |= ((resultBack.getWidth()>canvas.getWidth()?1:0) << 0);
        test   |= ((resultBack.getHeight()>canvas.getHeight()?1:0) << 1);
        switch (test){

            case 1:
                scaleResult = ((float) w) / resultBack.getWidth();
                break;
            case 2:
                scaleResult = (h * 0.32f) / resultBack.getHeight();
                break;
            case 3:
            case 0:
                if(Math.abs(resultBack.getWidth()-canvas.getWidth())>Math.abs(resultBack.getHeight()-canvas.getHeight())){  //mayor abncho
                    scaleResult = ((float) w) / resultBack.getWidth();
                }else{
                    scaleResult = (h * 0.32f) / resultBack.getHeight();
                }
                break;
        }
        scaleSize = (h * 0.60f) / ringFix.getHeight();
        rotateOrigin = (int) ((ringFix.getHeight() * scaleSize) / 2);

        scaleWidthDate = (w * 0.8f) / dateSerial.getWidth();
        scaleHeigthDate = (h * 0.08f) / dateSerial.getHeight();

        scaleWidhtBackGround = ((float) w) / backGround.getWidth();
        scaleHegthBackGround = ((float) h) / backGround.getHeight();

        floorDegree = (int) (degrees[indexMAxRotate] / RELATION);
        rouletteDegree = SIZE_ROULETTE * (int) (degrees[indexMAxRotate] / 360);
        currentValue = Math.abs((int) Math.floor(floorDegree - rouletteDegree));
        if (lastValue != currentValue) {
            if (rouletteListener != null) {
                rouletteListener.change(currentValue);
            }
            lastValue = currentValue;
        }
        for (int i = 0; i < 3; i++) {
            ringsMatrix[i] = new Matrix();
            ringsMatrix[i].postScale(scaleSize, scaleSize);
            ringsMatrix[i].postRotate(degrees[i], rotateOrigin, rotateOrigin);
            ringsMatrix[i].postTranslate((w - (ringFix.getWidth() * scaleSize)) / 2, (dateSerial.getHeight() * scaleHeigthDate) * 1.20f);
        }
        ringFixMatrix = new Matrix();
        resultBackMatrix = new Matrix();
        dateSerialtMatrix = new Matrix();
        backGroundMatrix = new Matrix();

        ringFixMatrix.postScale(scaleSize, scaleSize);
        ringFixMatrix.postTranslate((w - (ringFix.getWidth() * scaleSize)) / 2, (dateSerial.getHeight() * scaleHeigthDate) * 1.20f);

        resultBackMatrix.postScale(scaleResult, scaleResult);
        resultBackMatrix.postTranslate((w - (resultBack.getWidth() * scaleResult)) / 2, h - (resultBack.getHeight() * scaleResult));

        dateSerialtMatrix.postScale(scaleWidthDate, scaleHeigthDate);
        dateSerialtMatrix.postTranslate((w - (dateSerial.getWidth() * scaleWidthDate)) / 2, 0);

        backGroundMatrix.postScale(scaleWidhtBackGround, scaleHegthBackGround);

        for (int i = 0; i < 3; i++) {
            resultFrontMatrix[i] = new Matrix();
            resultFrontMatrix[i].postScale(scaleResult, scaleResult);
            resultFrontMatrix[i].postTranslate((percentFront[i] * scaleResult * resultBack.getWidth())
                            + ((w - (resultBack.getWidth() * scaleResult)) / 2),
                    (h - (resultBack.getHeight() * scaleResult)) +
                            resultBack.getHeight() * scaleResult * percentHeigthFront);
        }

        canvas.drawBitmap(backGround, backGroundMatrix, null);
        canvas.drawBitmap(ring1, ringsMatrix[0], null);
        canvas.drawBitmap(ring2, ringsMatrix[1], null);
        canvas.drawBitmap(ring3, ringsMatrix[2], null);
        canvas.drawBitmap(ringFix, ringFixMatrix, null);
        canvas.drawBitmap(centerButton, ringFixMatrix, null);
        canvas.drawBitmap(resultBack, resultBackMatrix, null);
        canvas.drawBitmap(dateSerial, dateSerialtMatrix, null);
        if (drawResult) {
            for (int i = 0; i < 3; i++) {
                canvas.drawBitmap(resulFrontImage[i], resultFrontMatrix[i], null);
            }
            drawResult = false;
        }
        Rect r = new Rect();
        Paint paint = new Paint();
        //String line1 = "Sorteo: 4042 6:00 PM";
        paint.setColor(Color.BLACK);
        paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin));
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint.getTextBounds(serialAndDate, 0, serialAndDate.length(), r);
        float x = (w - r.width()) / 2f;
        float y = (((dateSerial.getHeight() * scaleHeigthDate) - r.height())/2f);
        canvas.drawText(serialAndDate, x, y * 1.5f, paint);
    }

    public void rotate(float degrees1, float degrees2, float degrees3, int indexMaxRotate) {
        this.degrees[0] = degrees1;
        this.degrees[1] = degrees2;
        this.degrees[2] = degrees3;
        this.indexMAxRotate = indexMaxRotate;
        invalidate();
    }

    public void setSerialAndDate(String serialAndDate){
        this.serialAndDate  = serialAndDate;
    }

    public float getDegrees(int index) {
        return degrees[index];
    }

    public void setDegrees(float[] values) {
        degrees = values;
        invalidate();
    }

    public void setDegrees(int index, float value) {
        degrees[index] = value;
        invalidate();
    }

    public interface RouletteListener {
        void change(int value);
    }

    public void setRouletteListener(RouletteListener ppl) {
        rouletteListener = ppl;
    }


    public void setResultImage(Bitmap[] resultImage) {
        for (int i = 0; i < 3; i++) {
            resulFrontImage[i] = resultImage[i];
        }
        drawResult = true;
        invalidate();
    }

    public void clear() {
        ringFix = null;
        ring1 = null;
        ring2 = null;
        ring3 = null;
        centerButton = null;
        resultBack = null;
        dateSerial = null;
        backGround = null;
        ringsMatrix = null;
        degrees = null;
        resultFrontMatrix = null;
        resulFrontImage = null;

    }

}
