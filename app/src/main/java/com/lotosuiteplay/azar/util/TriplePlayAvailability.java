package com.lotosuiteplay.azar.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.SellItem;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class TriplePlayAvailability {

    public static void saveTicketTpStatus(Context context, Map<String, Integer> ticketTp, Date date) {
        Map<String, Integer> t = getSaveTpStatus(context, date);
        if (t != null && t.size() > 0) {
            for (Map.Entry<String, Integer> b : ticketTp.entrySet()) {
                for (Map.Entry<String, Integer> a : t.entrySet()) {
                    if (!b.getKey().equals(a.getKey())) {
                        ticketTp.put(a.getKey(), a.getValue());
                    }
                }
            }
        }
        String d = new SimpleDateFormat("dd-MM-yyyy").format(date);
        SharedPreferenceConstants.saveSharedPreferencesData(context, "tp" + Global.userId, d, new Gson().toJson(ticketTp));
    }

    private static Map<String, Integer> getSaveTpStatus(Context context, Date date) {
        String d = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String json = SharedPreferenceConstants.getSharedPreferencesData(context, "tp" + Global.userId, d);
        Type custom = new TypeToken<Map<String, Integer>>() {
        }.getType();
        Map<String, Integer> ticketTp = new Gson().fromJson(json, custom);
        return ticketTp;
    }

    public static boolean checkTpAvailability(Context context, ArrayList<SellItem> sellItems) {
        Map<String, Integer> ticketTp = getSaveTpStatus(context, new Date());
        if (ticketTp != null && ticketTp.size() > 0) {
            for (Map.Entry<String, Integer> a : ticketTp.entrySet()) {
                for (SellItem s : sellItems) {
                    if (s.raffle == Integer.parseInt(a.getKey())) {
                        if (a.getValue() == 2) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
