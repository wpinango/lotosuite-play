package com.lotosuiteplay.azar.util.random;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Chip;
import com.lotosuiteplay.azar.models.Game;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class RandomUtil {

    public static Chip getRandomChip(String codeName) {
        Chip chip = new Chip();
        for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
            Game sg = games.getValue();
            LinkedHashMap<String, Chip> chips = sg.getChips();
            if (codeName.equals(sg.getCodeName())) {
                ArrayList<Chip> ids = new ArrayList(chips.values());
                chip = randomItem1(ids);
                while (chip.isSelected()) {
                    chip = randomItem1(ids);
                }
            }
        }
        return chip;
    }

    private static Chip randomItem1(ArrayList<Chip> myList) {
        Random rand = new Random();
        return myList.get(rand.nextInt(myList.size()));
    }
}
