package com.lotosuiteplay.azar.util;

import android.text.Editable;
import android.text.TextWatcher;

import com.lotosuiteplay.azar.interfaces.TextListener;

public class AmountWatcher implements TextWatcher {
    private TextListener textListener;

    public AmountWatcher(TextListener textListener) {
        this.textListener = textListener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        textListener.onChangeText(s.toString());
    }
}
