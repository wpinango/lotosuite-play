package com.lotosuiteplay.azar.util;

import com.lotosuiteplay.azar.Global;

import java.util.Date;

public class CompareDate {

    public static boolean isAppDateChanged() {
        return new Date().getDay() == new Date(Global.timestamp).getDay();
    }
}
