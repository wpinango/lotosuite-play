package com.lotosuiteplay.azar.util.notification;

import android.content.Context;
import android.content.SharedPreferences;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;

/**
 * Created by wpinango on 9/5/17.
 */

public class NotificationsConfig {

    public void setNotificationConfig(Context context,String value){
        SharedPreferences preferences = context.getSharedPreferences(Global.userId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("configNotification", value);
        edit.apply();
    }

    public String getNotification(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Global.userId, Context.MODE_PRIVATE);
        return sharedPreferences.getString("configNotification","");
    }

}
