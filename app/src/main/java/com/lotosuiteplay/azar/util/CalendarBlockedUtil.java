package com.lotosuiteplay.azar.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarBlockedUtil {

    public static void saveCalendarDatesDisable(Context context, List<Calendar> calendars) {
        Calendar calendar = Calendar.getInstance();
        List<Calendar> calendars1 = new ArrayList<>();
        calendars.addAll(getDisableDates(context));
        for (Calendar c: calendars) {
            if (c.getTime().getYear() == calendar.getTime().getYear()) {
                if (c.getTime().getMonth() == calendar.getTime().getMonth()) {
                    if (c.getTime().getDay() >= calendar.getTime().getDay()) {
                        calendars1.add(c);
                    }
                }
            }
        }
        Map<String, Calendar> map = new HashMap<>();
        for (Calendar b : calendars1) {
            if (!map.containsKey(b.getTime().getDay() + "/" +  b.getTime().getMonth())) {
                map.put(b.getTime().getDay() + "/" + b.getTime().getMonth() , b);
            }
        }
        calendars1.clear();
        calendars1.addAll(map.values());
        SharedPreferenceConstants.saveSharedPreferencesData(context, "datesBlocked", "calendar", new Gson().toJson(calendars1));
    }

    public static List<Calendar> getDisableDates(Context context) {
        List<Calendar> calendars = new ArrayList<>();
        if (!SharedPreferenceConstants.getSharedPreferencesData(context, "datesBlocked", "calendar").isEmpty()) {
            Type custom = new TypeToken<List<Calendar>>() {
            }.getType();
            calendars = new Gson().fromJson(SharedPreferenceConstants.getSharedPreferencesData(context, "datesBlocked", "calendar"), custom);
        }
        return calendars;
    }

    public static boolean isTodayBlocked(Context context) {
        boolean isBlocked = false;
        Calendar calendar = Calendar.getInstance();
        List<Calendar> calendars = new ArrayList<>(getDisableDates(context));
        for(Calendar c: calendars) {
            if (c.getTime().getYear() == calendar.getTime().getYear()) {
                if (c.getTime().getMonth() == calendar.getTime().getMonth()) {
                    if (c.getTime().getDay() == calendar.getTime().getDay()) {
                        isBlocked = true;
                        break;
                    }
                }
            }
        }
        return isBlocked;
    }
}
