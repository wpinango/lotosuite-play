package com.lotosuiteplay.azar.util.rackMonthPicker.listener;

import androidx.appcompat.app.AlertDialog;

/**
 * Created by kristiawan on 31/12/16.
 */

public interface OnCancelMonthDialogListener {
    public void onCancel(AlertDialog dialog);
}
