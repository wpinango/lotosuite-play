package com.lotosuiteplay.azar.util;

import android.text.Editable;
import android.text.TextWatcher;

import com.lotosuiteplay.azar.interfaces.TextListener;

public class UsernameWatcher implements TextWatcher {

    private TextListener textListener;
    private String beforeTextChanged;

    public UsernameWatcher(TextListener textListener) {
        this.textListener = textListener;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        try {
            beforeTextChanged = charSequence.toString();
        } catch (Exception e) {

        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        try {
            if (charSequence.toString().charAt(0) != 'M') {
                textListener.onChangeText("M" + beforeTextChanged.replace("M", ""));
            }
        }catch (Exception e) {

        }

    }

    @Override
    public void afterTextChanged(Editable editable) {
        try {
            if (editable.length() == 0) {
                editable.append("M");
            }
        } catch (Exception e) {

        }
    }
}
