package com.lotosuiteplay.azar.activities;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.beardedhen.androidbootstrap.BootstrapButton;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.activities.login.LoginActivity;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.interfaces.FingerAuth;
import com.lotosuiteplay.azar.util.FingerprintHandler;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import cn.pedant.SweetAlert.SweetAlertDialog;


@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerPrintActivity extends AppCompatActivity implements FingerAuth, AsyncktaskListener {

    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private static final String KEY_NAME = "yourKey";
    private TextView tvMessage;
    private ImageView imgFingerDetection;
    private BootstrapButton btnEnter;
    private EditText etInputPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger);
        btnEnter = findViewById(R.id.button2);
        etInputPass = findViewById(R.id.et_input_pass);
        tvMessage = findViewById(R.id.tv_finger_message);
        imgFingerDetection = findViewById(R.id.img_finger_detection);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            try {
                generateKey();
            } catch (Exception e) {

            }

            if (initCipher()) {
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
                FingerprintHandler helper = new FingerprintHandler(this, this);
                helper.startAuth(fingerprintManager, cryptoObject);
            }

        }
        btnEnter.setOnClickListener(view -> {
            if (!etInputPass.getText().toString().isEmpty()) {
                etInputPass.setError(null);
                SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                if (sharedPreferences.getString("pass","").equals(etInputPass.getText().toString())){
                    openMainActivity();
                } else {
                    new SweetAlertDialog(FingerPrintActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Datos Incorrectos")
                            .setContentText("Hubo un problema en la comprobacion de los datos")
                            .show();
                }
            } else {
                etInputPass.setError("Ingregse la clave");
            }
        });
        findViewById(R.id.btn_logout).setOnClickListener(view -> {
            LoginState.setLoginState(FingerPrintActivity.this, false);
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_CLOSE_TOKEN, "",  new HashMap<>(), FingerPrintActivity.this).execute();
            finish();
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu. menu_finger_print, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_close_session) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Importante")
                    .setMessage("Desea cerrar la sesion?")
                    .setPositiveButton("Ok", (dialog, which) -> {
                        dialog.cancel();
                        LoginState.setLoginState(FingerPrintActivity.this, false);
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        new GlobalAsyntask.PostMethodAsynctask(Global.URL_CLOSE_TOKEN, "",  new HashMap<>(), this).execute();
                        finish();
                    });
            builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
            builder.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
*/
    private void generateKey() throws Exception {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");


            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new Exception(exc);
        }


    }


    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onSuccess() {
        //tvMessage.setText("Verificacion correcta");
        //imgFingerDetection.setImageResource(R.drawable.ic_check_green_36dp);
        Global.Toaster.get().showToast(this, "Verificacion correcta", Toast.LENGTH_SHORT);
        openMainActivity();
    }

    @Override
    public void onFailed() {
        Global.Toaster.get().showToast(this, "Verificacion fallida", Toast.LENGTH_SHORT);
        //tvMessage.setText("Verificacion fallida");
    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {

    }
}
