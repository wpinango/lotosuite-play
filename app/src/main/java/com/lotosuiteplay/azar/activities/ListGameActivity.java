package com.lotosuiteplay.azar.activities;

import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.lotosuiteplay.azar.adapters.GameListAdapters.GameListAdapter;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.GamesChipGridAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class ListGameActivity extends BaseActivity {

    private GamesChipGridAdapter Adapter;
    private String[] animalName, fruitName, lottoPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Juegos disponible");
        ArrayList<String> lotoName = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.LottoNames)));
        GameListAdapter listAdapter2 = new GameListAdapter(this, lotoName, Global.lotoLogo);
        SwipeMenuListView gameList = findViewById(R.id.gameList);
        gameList.setAdapter(listAdapter2);
        gameList.setOnItemClickListener((parent, view, position, id) -> MosAlerta(position));
        animalName = getResources().getStringArray(R.array.animal_names);
        fruitName = getResources().getStringArray(R.array.fruit_name);
        lottoPlay = getResources().getStringArray(R.array.lottery_name);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_list_game;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MosAlerta(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.grid_view_general, null);
        GridView xGrilla = ViewAlerta.findViewById(R.id.grdGrilla);
        TextView xTitulo = ViewAlerta.findViewById(R.id.lblTitu);

        switch (position){
            case 0:
                Adapter = new GamesChipGridAdapter(this, animalName, Global.newSafariImages);
                xTitulo.setText("Safari Play");
                break;
            case 1:
                Adapter = new GamesChipGridAdapter(this, fruitName, Global.newTizanaImages);
                xTitulo.setText("Tizana Play");
                break;
            case 2:
                Adapter = new GamesChipGridAdapter(this, lottoPlay, Global.newLotoImages);
                xTitulo.setText("Loto Play");
                break;
        }

        xGrilla.setAdapter(Adapter);
        builder.setNegativeButton("Cerrar", null);
        builder.setView(ViewAlerta);
        AlertDialog iconDialog = builder.create();
        iconDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

}
