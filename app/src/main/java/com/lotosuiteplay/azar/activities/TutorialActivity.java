package com.lotosuiteplay.azar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.TutorialListAdapter;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.Tutorial;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by wpinango on 10/17/17.
 */

public class TutorialActivity extends BaseActivity {
    private ListView lvTutorial;
    private ArrayList<Tutorial> tutorialList = new ArrayList<>();
    private TutorialListAdapter tutorialListAdapter;
    private ProgressBar progressBar;
    private ImageButton btnRefresh;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_tutorial;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lvTutorial = findViewById(R.id.lv_tutorial);
        progressBar = findViewById(R.id.progressBar11);
        progressBar.setVisibility(View.INVISIBLE);
        btnRefresh = findViewById(R.id.imageButton12);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Tutoriales");
        SharedPreferences preferences = getSharedPreferences("Label", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean("isShow", false);
        edit.apply();
        tutorialListAdapter = new TutorialListAdapter(this,tutorialList);
        lvTutorial.setAdapter(tutorialListAdapter);
        new GetVideoTutorials().execute(Global.URL_GET_TUTORIAL);
        lvTutorial.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(TutorialActivity.this, YoutubePlayerActivity.class);
            intent.putExtra("video",tutorialList.get(i).getUrl());
            intent.putExtra("videoName",tutorialList.get(i).getTitle());
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(TutorialActivity.this);
        });
        btnRefresh.setOnClickListener(view -> new GetVideoTutorials().execute(Global.URL_GET_TUTORIAL));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private class GetVideoTutorials extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(TutorialActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                tutorialList.clear();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    Tutorial[] t = gson.fromJson(response.message,Tutorial[].class);
                    Collections.addAll(tutorialList, t);
                    tutorialListAdapter.notifyDataSetChanged();
                } else {
                    Global.Toaster.get().showToast(TutorialActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
