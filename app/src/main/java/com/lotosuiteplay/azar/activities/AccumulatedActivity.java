package com.lotosuiteplay.azar.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.TriplePlayWinnersListAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.TriplePlayData;
import com.lotosuiteplay.azar.models.TriplePlayWinner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class AccumulatedActivity extends BaseActivity implements AsyncktaskListener {
    private androidx.appcompat.app.ActionBar actionBar;
    private TextView tvAccumulated, tvWinnerAmount, tvAccumulatedHour, tvSerial, tvWinnerCount;
    private TriplePlayWinnersListAdapter triplePlayWinnersListAdapter;
    private ArrayList<TriplePlayWinner> triplePlayWinners = new ArrayList<>();
    private ProgressBar progressBar;
    private ImageButton btnRefresh, btnHistoric;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_triple_play;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Acumulado");
        progressBar = findViewById(R.id.progressBar14);
        triplePlayWinnersListAdapter = new TriplePlayWinnersListAdapter(this, triplePlayWinners);
        ListView lvWinners = findViewById(R.id.lv_winners);
        btnHistoric = findViewById(R.id.btn_search_triple_play);
        btnRefresh = findViewById(R.id.btn_refresh_triple_play);
        lvWinners.setAdapter(triplePlayWinnersListAdapter);
        tvAccumulated = findViewById(R.id.tv_accumulated_amount);
        tvWinnerAmount = findViewById(R.id.tv_winner_amount);
        tvAccumulatedHour = findViewById(R.id.tv_accumulated_time);
        tvSerial = findViewById(R.id.tv_accumulated_serial);
        tvWinnerCount = findViewById(R.id.tv_winner_count);
        requestAccumulated();
        btnRefresh.setOnClickListener(view -> requestAccumulated());
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    void requestAccumulated() {
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        progressBar.setVisibility(View.VISIBLE);
        new GlobalAsyntask.GetMethodAsynctask(Global.URL_GET_TRIPLE_PLAY_WINNERS, headers, this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        progressBar.setVisibility(View.INVISIBLE);
        if (!response.isEmpty() || !response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.status == 200 && res.error.equals("")) {
                try {
                    TriplePlayData triplePlayData = new Gson().fromJson(res.message, TriplePlayData.class);
                    int value = triplePlayData.getRaffleSerial() == null ? 0 : triplePlayData.getRaffleSerial();
                    tvSerial.setText(String.valueOf(value));
                    tvAccumulated.setText(Format.getCashFormat(triplePlayData.getAcumulated()));
                    triplePlayWinners.addAll(triplePlayData.getLastTriplePlayWinners());
                    triplePlayWinnersListAdapter.notifyDataSetChanged();
                    float amount = 0.0f;
                    for (TriplePlayWinner t : triplePlayData.getLastTriplePlayWinners()) {
                        amount += Double.parseDouble(t.getAmount());
                    }
                    tvWinnerAmount.setText(Format.getCashFormat(amount));
                    tvAccumulatedHour.setText(Time.convertToHours(triplePlayData.getTimeStampLastWin()));
                    int count = triplePlayData.getLastTriplePlayWinners() == null ? 0 : triplePlayData.getLastTriplePlayWinners().size();
                    tvWinnerCount.setText("(" + String.valueOf(count) + ")");
                } catch (Exception e) {
                    Global.Toaster.get().showToast(AccumulatedActivity.this, "No hay datos", Toast.LENGTH_SHORT);
                }
            }
        }
    }
}
