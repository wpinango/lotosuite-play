package com.lotosuiteplay.azar.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.interfaces.DialogInterface;
import com.lotosuiteplay.azar.util.CalendarBlockedUtil;
import com.lotosuiteplay.azar.widgets.ImageTextButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarActivity extends BaseActivity {

    private ImageTextButton btnLockDay;
    private CalendarView calendarView;
    private List<EventDay> eventDays = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        calendarView = findViewById(R.id.calendarView);
        if (CalendarBlockedUtil.getDisableDates(this).size() > 0) {
            setEventDays(CalendarBlockedUtil.getDisableDates(this));
        }
        btnLockDay = findViewById(R.id.btn_lock_day);
        btnLockDay.setOnClickListener(view -> {
            Dialog.showDialogQuestionDialog(CalendarActivity.this, "Importante",
                    "Quiere bloquear el acceso a la aplicacion los dias seleccionados?\n" +
                            "No podra reversar la operacion luego",
                    new DialogInterface() {
                        @Override
                        public void positiveClick(String value) {
                            List<Calendar> clickedDayCalendar = calendarView.getSelectedDates();
                            setEventDays(clickedDayCalendar);
                            CalendarBlockedUtil.saveCalendarDatesDisable(CalendarActivity.this, clickedDayCalendar);
                        }

                        @Override
                        public void negativeClick() {

                        }
                    }, "Acepta", "Cancelar");
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_calendar;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void setEventDays(List<Calendar> clickedDayCalendar) {
        Calendar calendar = Calendar.getInstance();
        for (Calendar c : clickedDayCalendar) {
            if (c.getTime().getYear() == calendar.getTime().getYear()) {
                if (c.getTime().getMonth() == calendar.getTime().getMonth()) {
                    if (c.getTime().getDay() >= calendar.getTime().getDay()) {
                        eventDays.add(new EventDay(c, R.drawable.ic_lock_black_36dp));
                    } else {
                        Global.Toaster.get().showToast(this, "No puede bloquear este(os) dia(s)", Toast.LENGTH_SHORT);
                    }
                }
            }
        }
        calendarView.setEvents(eventDays);
    }
}
