package com.lotosuiteplay.azar.activities;

import androidx.appcompat.app.AlertDialog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.adapters.ProducerListAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.BooleanResponse;
import com.lotosuiteplay.azar.models.Producer;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.R;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 7/25/17.
 */

public class ChangeProducerActivity extends BaseActivity {
    private ArrayList<Producer> producers = new ArrayList<>();
    private ProducerListAdapter producerListAdapter;
    private int targetId;
    private String pass;
    private EditText etPassword;
    private ImageButton btnSend, btnRefresh;
    private EditText etProducerName;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etProducerName = findViewById(R.id.et_producer_name_change);
        producerListAdapter = new ProducerListAdapter(this, producers);
        etPassword = findViewById(R.id.et_password_change);
        final ListView lvProducer = findViewById(R.id.lv_producer_change);
        lvProducer.setAdapter(producerListAdapter);
        btnSend = findViewById(R.id.btn_send_change);
        btnRefresh = findViewById(R.id.btn_refresh_change);
        progressBar = findViewById(R.id.progressBar7);
        progressBar.setVisibility(View.INVISIBLE);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Cambie de asesor");
        new GetProducers().execute(Global.URL_GET_PRODUCER);
        try {
            etProducerName.setText(Global.producerName);
        } catch (Exception e) {
            e.getMessage();
        }
        btnSend.setOnClickListener(view -> {
            if (validation()) {
                btnSend.setEnabled(false);
                showConfirmDialog();
            }
        });
        btnRefresh.setOnClickListener(view -> new GetProducers().execute(Global.URL_GET_PRODUCER));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_change_producer;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean validation () {
        boolean valid = true;
        String pass = etPassword.getText().toString();
        if (pass.isEmpty()) {
            Toast.makeText(ChangeProducerActivity.this, "Debe introducir la contraseña" , Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (!Producer.isProducerSelected(producers)){
            Toast.makeText(ChangeProducerActivity.this,"Debe seleccionar un asesor" , Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ChangeProducerActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea cambiar de asesor?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", (dialogInterface, i) -> {
            Producer p = Producer.getProducerSelected(producers);
            targetId = p.getId();
            try {
                pass = Format.getPEncrypted(etPassword.getText().toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            new ChangeProducer().execute(Global.URL_CHANGE_PRODUCER);
            btnSend.setEnabled(true);
        });
        dialog.setNegativeButton("Cancelar", (dialogInterface, i) -> btnSend.setEnabled(true));
        dialog.show();
    }

    private class ChangeProducer extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("targetProducerId",targetId);
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SECURITY, pass.trim())
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(ChangeProducerActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(ChangeProducerActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("OK")
                                .setContentText("Se realizo cambio de asesor")
                                .show();
                        Producer p = Producer.getProducerSelected(producers); //TODO aqui se debe guardar la data del nuevo productor
                        Global.producerId = p.getId();
                        Global.producerName = p.getName();
                        Global.producerRating = p.getScore();
                        etPassword.setText("");
                        etProducerName.setText(p.getName());
                        Producer.deselectProducer(producers);
                        producerListAdapter.notifyDataSetChanged();
                    } else {
                        new SweetAlertDialog(ChangeProducerActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("No se pudo realizar el cambio de asesor")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(ChangeProducerActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetProducers extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            producers.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .connectTimeout(5000)
                        .header(Global.KEY_PRODUCER_ID,Global.producerId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .readTimeout(Global.httpRequestTimeout)
                        .connectTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(ChangeProducerActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    Type custom = new TypeToken<ArrayList<Producer>>() {
                    }.getType();
                    ArrayList<Producer> p = gson.fromJson(response.message, custom);
                    producers.addAll(p);
                    producerListAdapter.notifyDataSetChanged();
                } else {
                    Global.Toaster.get().showToast(ChangeProducerActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
