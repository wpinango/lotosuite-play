package com.lotosuiteplay.azar.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.lotosuiteplay.azar.Global;

/**
 * Created by wpinango on 9/13/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityPaused();
        //TransitionAnimation.setOutActivityTransition(this);
        //finish();
    }

    protected abstract int getLayoutResourceId();

    public static void activityResumed() {
        Global.activityVisible = true;
    }

    public static void activityPaused() {
        Global.activityVisible = false;
    }

}
