package com.lotosuiteplay.azar.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.ResultData;
import com.lotosuiteplay.azar.roulette.RouletteAccessor;
import com.lotosuiteplay.azar.roulette.SoundConfig;
import com.lotosuiteplay.azar.roulette.SoundPool;
import com.lotosuiteplay.azar.util.view.Roulette;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Quint;

import static com.lotosuiteplay.azar.roulette.SoundPool.SOUNDPOOLSND_WIN;

/**
 * Created by wpinango on 9/7/17.
 */

public class RouletteActivity extends AppCompatActivity {
    //public static boolean isBackOnPressedEnable = false;
    private TweenManager tweenManager;
    private boolean isAnimationRunning = true;
    private Roulette roulette;
    private LinkedHashMap<String, Integer> numberList;
    private LinkedHashMap<String, Integer> numberListReverse;
    public static final String LOTO = "sgt-lot";
    public static final String TIZANA = "sgt-tiz";
    public static final String SAFARI = "sgt-saf";
    public final static int SIZE_ROULETTE = 36;
    public final static float RELATION = (360f / SIZE_ROULETTE);
    private SoundPool soundManager;
    private ToggleButton btnAudio;
    private boolean isAudioActivated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roulette);
        TransitionAnimation.setInUpActiviTrantition(this);
        btnAudio = (ToggleButton)findViewById(R.id.toggleButton2);
        numberList = new Gson().fromJson(loadJSONFromAsset("number.json"),
                new TypeToken<LinkedHashMap<String, Integer>>() {
                }.getType());
        numberListReverse = new Gson().fromJson(loadJSONFromAsset("numberReverse.json"),
                new TypeToken<LinkedHashMap<String, Integer>>() {
                }.getType());
        roulette = (Roulette) findViewById(R.id.roulette);
        soundManager = new SoundPool(this.getApplicationContext());
        isAudioActivated = SoundConfig.getRouletteSoundStatus(this);
        roulette.setRouletteListener(new Roulette.RouletteListener() {
            @Override
            public void change(final int value) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(isAudioActivated) {
                            soundManager.playSound(value);
                        }
                    }
                }).start();
            }
        });

        setTweenEngine();
        ResultData r = new Gson().fromJson(getIntent().getStringExtra(Global.KEY_VALUE_ROULETTE),ResultData.class);
        settingValueRotation(r);
        roulette.setSerialAndDate("Sorteo: " + r.currentResult.raffleWinSerial + " " + r.currentResult.raffleName);
        btnAudio.setChecked(SoundConfig.getRouletteSoundStatus(this));
        btnAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SoundConfig.setRouletteSoundStatus(RouletteActivity.this,b);
                isAudioActivated = SoundConfig.getRouletteSoundStatus(RouletteActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        //if (isBackOnPressedEnable){
            super.onBackPressed();
            setAnimationFalse();
            SoundPool.clear();
            TransitionAnimation.setInDownActivityTrantition(this);
        //    isBackOnPressedEnable = false;
        //}
    }

    private void setAnimationThread() {
        new Thread(new Runnable() {
            private long lastMillis = -1;
            @Override
            public void run() {
                while (isAnimationRunning) {
                    if (lastMillis > 0) {
                        long currentMillis = System.currentTimeMillis();
                        final float delta = (currentMillis - lastMillis) / 1000f;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tweenManager.update((float) (delta));
                            }
                        });
                        lastMillis = currentMillis;
                    } else {
                        lastMillis = System.currentTimeMillis();
                    }
                    try {
                        Thread.sleep(1000 / 60);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        }).start();
    }


    private void rotate(float value1, float value2, float value3, int[] timeRotation) {
        int randon[];
        randon = timeRotation;
        float largeValue = 0;
        int indexLarge = 0;
        for (int i = 0; i < randon.length; i++) {
            if (randon[i] > largeValue) {
                largeValue = randon[i];
                indexLarge = i;
            }
        }
        roulette.rotate(0, 0, 0, indexLarge);
        Timeline.createSequence()
                .beginParallel()
                .push(Tween.to(roulette, RouletteAccessor.ROTATION1, (float) (randon[0] + 10))
                        .target(value1)
                        .ease(Quint.OUT))
                .push(Tween.to(roulette, RouletteAccessor.ROTATION2, (float) (randon[1] + 10))
                        .target(value2)
                        .ease(Quint.OUT))
                .push(Tween.to(roulette, RouletteAccessor.ROTATION3, (float) (randon[2] + 10))
                        .target(value3)
                        .ease(Quint.OUT))
                .setCallbackTriggers(TweenCallback.COMPLETE)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        roulette.setResultImage(Global.resultImage);
                        if (isAudioActivated) {
                            soundManager.playSound(SOUNDPOOLSND_WIN);
                        }
                        new CountDownTimer(5000,1000) {

                            @Override
                            public void onTick(long l) {
                            }

                            @Override
                            public void onFinish() {
                                RouletteActivity.this.finish();
                                TransitionAnimation.setInDownActivityTrantition(RouletteActivity.this);
                            }
                        }.start();
                    }
                }).start(tweenManager);
    }

    public void settingValueRotation(ResultData result) {
        int temp1, temp2, temp3;
        if (result == null) {
            temp1 = (int) (Math.random() * 37);
            temp2 = (int) (Math.random() * 37);
            temp3 = (int) (Math.random() * 37);
        } else {
            temp1 = numberList.get(result.currentResult.results.get(SAFARI).chipWinner);
            temp2 = numberListReverse.get(result.currentResult.results.get(TIZANA).chipWinner);
            temp3 = numberList.get(result.currentResult.results.get(LOTO).chipWinner);
        }
        float value1 = 360 * 5;
        float value2 = 360 * 5;
        float value3 = 360 * 5;
        value2 *= -1;
        value1 += (RELATION * temp1) - (RELATION / 2);
        value2 -= (RELATION * temp2) - (RELATION / 2);
        value3 += (RELATION * temp3) - (RELATION / 2);
        rotate(value1, value2, value3, result.timeRotation);
    }

    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * Stop animation thread
     */
    private void setAnimationFalse() {
        isAnimationRunning = false;
    }

    /**
     * Make animation thread alive
     */
    private void setAnimationTrue() {
        isAnimationRunning = true;
    }

    /**
     * Initiate the Tween Engine
     */
    private void setTweenEngine() {
        tweenManager = new TweenManager();
        setAnimationThread();
        Tween.registerAccessor(Roulette.class, new RouletteAccessor());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //isBackOnPressedEnable = false;
        roulette.clear();
    }
}
