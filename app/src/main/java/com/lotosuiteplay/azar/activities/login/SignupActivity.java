package com.lotosuiteplay.azar.activities.login;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.adapters.SpinnerProducerAdapter;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.interfaces.FingerprintStatus;
import com.lotosuiteplay.azar.models.BooleanResponse;
import com.lotosuiteplay.azar.models.NewUser;
import com.lotosuiteplay.azar.models.Producer;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.R;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.models.Rule;
import com.lotosuiteplay.azar.singletons.RulesSingleton;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class SignupActivity extends AppCompatActivity implements FingerprintStatus, AsyncktaskListener {

    private TextView linkLogin;
    private BootstrapButton btnCreateUser;
    private EditText etName, etLastName, etDni, etPhone, etEmail, etPassword, etRepeatPassword, etNickname, etDate;
    private SweetAlertDialog progressDialog;
    private String producerId;
    private NewUser newUser;
    private ArrayList<Producer> producers = new ArrayList<>();
    private String dateResult;
    private boolean isAccept = false, isUpdate = false, isRules = false;
    private SpinnerProducerAdapter spinnerProducerAdapter;
    private Spinner spinner;
    private RulesSingleton rulesSingleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        etName = findViewById(R.id.input_name);
        etLastName = findViewById(R.id.et_last_name);
        etDate = findViewById(R.id.et_date);
        etDni = findViewById(R.id.input_dni);
        CheckBox checkBox = findViewById(R.id.checkBox);
        CheckBox cbGameRules = findViewById(R.id.cb_rules);
        etEmail = findViewById(R.id.input_email);
        etPassword = findViewById(R.id.input_password);
        etRepeatPassword = findViewById(R.id.input_reEnterPassword);
        etPhone = findViewById(R.id.input_phone);
        btnCreateUser = findViewById(R.id.btn_signup);
        linkLogin = findViewById(R.id.link_login);
        linkLogin.setPaintFlags(linkLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        etNickname = findViewById(R.id.input_nickname);
        spinner = findViewById(R.id.sp_producer);
        spinnerProducerAdapter = new SpinnerProducerAdapter(this, R.layout.spinner_item2, producers, () -> {
            if (isUpdate) {
                spinner.performClick();
            }
        });
        spinner.setAdapter(spinnerProducerAdapter);
        spinner.setClickable(false);
        spinner.setEnabled(false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Producer.selectProducer(producers, i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        addHintToSpinner();
        spinnerProducerAdapter.notifyDataSetChanged();
        progressDialog = new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDialog.setTitleText("Consultando Asesores");
        progressDialog.show();
        new GetProducers().execute(Global.URL_GET_PRODUCER);
        Rule.requestGameRules(this);
        Rule.requestTermsConditions(this);
        linkLogin.setOnClickListener(v -> finishSignupActivity());
        btnCreateUser.setOnClickListener(v -> {
            btnCreateUser.setEnabled(false);
            createUser();
        });
        etDate.setOnClickListener(view -> alertDatePicker());
        String termsAndConditions = "Ver Terminos y Condiciones";
        SpannableString content = new SpannableString(termsAndConditions);
        content.setSpan(new UnderlineSpan(), 0, termsAndConditions.length(), 0);
        TextView tvRules = findViewById(R.id.tv_rules);
        tvRules.setText("Ver Reglas del Juego");
        tvRules.setPaintFlags(tvRules.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rulesSingleton.getGameRules().getTitle() != null) {
                    Dialog.showRulesDialog(SignupActivity.this,
                            rulesSingleton.getGameRules());
                } else {

                }
            }
        });
        TextView tvTerms = findViewById(R.id.tv_terms);
        tvTerms.setText(termsAndConditions);
        tvTerms.setPaintFlags(tvTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTerms.setOnClickListener(v -> {
            if (rulesSingleton.getTermsConditions().getTitle() != null) {
                Dialog.showRulesDialog(SignupActivity.this,
                        rulesSingleton.getTermsConditions());
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (rulesSingleton.getTermsConditions().getTitle() != null) {
                        isAccept = b;
                        rulesSingleton.getTermsConditions().setAccepted(isAccept);
                    } else {
                        checkBox.setChecked(false);
                        Rule.requestTermsConditions(SignupActivity.this);
                        Global.Toaster.get().showToast(SignupActivity.this, "No se puede cargar los terminos y condiciones", Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        cbGameRules.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (rulesSingleton.getGameRules().getTitle() != null) {
                        isRules = isChecked;
                        rulesSingleton.getGameRules().setAccepted(isRules);
                    } else {
                        cbGameRules.setChecked(false);
                        Rule.requestGameRules(SignupActivity.this);
                        Global.Toaster.get().showToast(SignupActivity.this, "No se puede cargar las reglas de juego", Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        rulesSingleton = RulesSingleton.getInstance(this);
    }

    private void addHintToSpinner() {
        Producer producer = new Producer();
        producer.setName("");
        producer.setScore(12f);
        producer.setSelected(false);
        producer.setStatus(0);
        producer.setUsersCount("12/100");
        producers.add(producer);
    }

    private void finishSignupActivity() {
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void finishActivityAndLogin() {
        LoginActivity.isRegisterSuccess = true;
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void alertDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, (datePicker, year, month, day) -> {
            month = month + 1;
            String formattedMonth = "" + month;
            String formattedDayOfMonth = "" + day;
            if (month < 10) {
                formattedMonth = "0" + month;
            }
            if (day < 10) {
                formattedDayOfMonth = "0" + day;
            }
            dateResult = formattedDayOfMonth + "/" + formattedMonth + "/" + year;
            etDate.setText(dateResult);
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    private String getProducerId() {
        return String.valueOf(Producer.getProducerSelected(producers).getId());
    }

    private void createUser() {
        if (validate()) {
            if (etPhone.length() != 0 && etName.length() != 0 && etDni.length() != 0 && etEmail.length() != 0
                    && etPassword.length() != 0 && etRepeatPassword.length() != 0 && !producers.isEmpty() &&
                    Producer.isProducerSelected(producers)) {
                newUser = new NewUser();
                newUser.setName(etName.getText().toString() + " " + etLastName.getText().toString());
                newUser.setCedula(Integer.parseInt(etDni.getText().toString()));
                newUser.setEmail(etEmail.getText().toString());
                newUser.setPhone(etPhone.getText().toString());
                newUser.setPassword(etPassword.getText().toString());
                newUser.setUserName("M" + etDni.getText());
                newUser.setNick(etNickname.getText().toString());
                DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    newUser.setBirthday(f.parse(dateResult));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                producerId = getProducerId();
                if (isAccept && isRules) {
                    progressDialog = new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    progressDialog.setTitleText("Enviando datos de registro");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    new SendNewRegister().execute(Global.URL_GET_REGISTER_NEW_USERS);
                } else if (!isAccept){
                    Toast.makeText(SignupActivity.this, "Debe aceptar terminos y condiciones", Toast.LENGTH_SHORT).show();
                } else if(!isRules) {
                    Toast.makeText(SignupActivity.this, "Debe aceptar reglas de juego", Toast.LENGTH_SHORT).show();
                }
                btnCreateUser.setEnabled(true);
            } else if (producers.isEmpty()) {
                btnCreateUser.setEnabled(true);
                Toast.makeText(SignupActivity.this, "Debe tener conexion con internet para registrarse", Toast.LENGTH_SHORT).show();
            } else if (!Producer.isProducerSelected(producers)) {
                Toast.makeText(SignupActivity.this, "Debe seleccionar un asesor de juego", Toast.LENGTH_SHORT).show();
            } else {
                btnCreateUser.setEnabled(true);
                Toast.makeText(SignupActivity.this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(SignupActivity.this, "Fallo en registro de usuario", Toast.LENGTH_SHORT).show();
            btnCreateUser.setEnabled(true);
        }
    }

    public boolean validate() {
        boolean valid = true;
        btnCreateUser.setEnabled(true);
        TextInputLayout textInputLayout1 = findViewById(R.id.textInputLayout37);
        TextInputLayout textInputLayout2 = findViewById(R.id.textInputLayout31);
        TextInputLayout textInputLayout = findViewById(R.id.textInputLayout22);
        TextInputLayout textInputLayout3 = findViewById(R.id.textInputLayout14);
        String dni = etDni.getText().toString();
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String mobile = etPhone.getText().toString();
        String password = etPassword.getText().toString();
        String nickname = etNickname.getText().toString();
        String reEnterPassword = etRepeatPassword.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            etName.setError("Introduzca al menos 3 caracteres");
            valid = false;
        } else {
            etName.setError(null);
        }
        if (nickname.isEmpty()) {
            etNickname.setError("Introduzca un apodo valido");
            valid = false;
        } else {
            etNickname.setError(null);
        }
        if (dni.isEmpty() || dni.length() < 7) {
            etDni.setError("Introduzca un numero de cedula valido");
            valid = false;
        } else {
            etDni.setError(null);
        }
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Introduzca una direccion de correo valida");
            valid = false;
        } else {
            etEmail.setError(null);
        }
        if (mobile.isEmpty() || mobile.length() != 11) {
            etPhone.setError("Introduzca un numero de telefono valido");
            valid = false;
        } else {
            etPhone.setError(null);
        }
        if (password.isEmpty() || password.length() < 6 || password.length() > 20) {
            textInputLayout1.setError("Contraseña minimo 6, maximo 20 caracteres");
            valid = false;
        } else {
            textInputLayout1.setError(null);
        }
        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 6 || reEnterPassword.length() > 20 || !(reEnterPassword.equals(password))) {
            textInputLayout2.setError("No coincien las contraseñas");
            valid = false;
        } else {
            textInputLayout2.setError(null);
        }
        if (etDate.getText().toString().isEmpty()) {
            valid = false;
            textInputLayout.setError("Introduzca fecha de nacimiento");
        } else {
            textInputLayout.setError(null);
        }
        if (etLastName.getText().toString().isEmpty()) {
            valid = false;
            textInputLayout3.setError("Introduzca su primer apellido");
        } else {
            textInputLayout3.setError(null);
        }
        return valid;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(SignupActivity.this);
    }

    @Override
    public void onStatusChange(boolean status) {
        isAccept = status;
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        if (endPoint.contains(Global.URL_GET_TERM_CONDITIONS)) {
            progressDialog.dismiss();
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("") && !res.message.equals("")) {
                Rule rule = new Gson().fromJson(res.message, Rule.class);
                rule.setTitle("Terminos y Condiciones");
                rule.setAccepted(false);
                rulesSingleton.setTermsConditions(this, rule);
            }
        } else if (endPoint.contains(Global.URL_GET_GAME_RULES)) {
            progressDialog.dismiss();
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("") && !res.message.equals("")) {
                Rule rule = new Gson().fromJson(res.message, Rule.class);
                rule.setTitle("Reglas de Juego");
                rule.setAccepted(false);
                rulesSingleton.setGameRules(this, rule);
            }
        }
    }

    private class GetProducers extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_PRODUCER_ID, 0)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(SignupActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    producers.clear();
                    Type custom = new TypeToken<ArrayList<Producer>>() {
                    }.getType();
                    ArrayList<Producer> p = gson.fromJson(response.message, custom);
                    producers.addAll(p);
                    spinnerProducerAdapter.notifyDataSetChanged();
                    isUpdate = true;
                    if (producers.size() > 0 )  {
                        Producer.selectProducer(producers, 0);
                    }
                } else {
                    Global.Toaster.get().showToast(SignupActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SendNewRegister extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(newUser);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_PRODUCER_ID, producerId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(SignupActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("Su usuario es " + newUser.getUserName() +
                                        ", presione OK para entrar a la aplicacion.")
                                .setConfirmClickListener(sweetAlertDialog -> {
                                    Global.userName = newUser.getUserName();
                                    Global.password = newUser.getPassword();
                                    etPhone.setText("");
                                    etName.setText("");
                                    etEmail.setText("");
                                    etRepeatPassword.setText("");
                                    etPassword.setText("");
                                    etDni.setText("");
                                    etNickname.setText("");
                                    finishActivityAndLogin();
                                })
                                .show();
                        Rule rule = rulesSingleton.getGameRules();
                        rulesSingleton.setGameRules(SignupActivity.this,rule);
                        Rule r = rulesSingleton.getTermsConditions();
                        rulesSingleton.setTermsConditions(SignupActivity.this,r);

                    } else {
                        new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Datos Incorrectos")
                                .setContentText("Hubo un problema en la comprobacion de los datos")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(SignupActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
