package com.lotosuiteplay.azar.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lotosuiteplay.azar.adapters.CommentsListAdapter;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.util.chart.Chart;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.Comment;
import com.lotosuiteplay.azar.models.ProducerInformation;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/25/17.
 */

public class ProducerInformationActivity extends BaseActivity {
    private ListView lvComment;
    private CommentsListAdapter commentsListAdapter;
    private ArrayList<Comment>comments = new ArrayList<>();
    private ArrayList<Bank> banks = new ArrayList<>();
    private ProgressBar progressBar;
    private androidx.appcompat.app.ActionBar actionBar;
    private EditText etProducerName, etProducerEmail, etOwner;
    private TextView tvRating, tvTotalUser;
    private RatingBar ratingBar;
    private HorizontalBarChart chart;
    private ImageButton btnRateProducer, btnBankAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ratingBar = findViewById(R.id.rb_producer);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        ratingBar.setRating(4);
        btnBankAccounts = findViewById(R.id.btn_bank_accounts);
        //btnChangeProducer = findViewById(R.id.btn_change_producer);
        btnRateProducer = findViewById(R.id.btn_rate_producer);
        etProducerName = findViewById(R.id.et_producer_name);
        etProducerEmail = findViewById(R.id.et_producer_email);
        etOwner = findViewById(R.id.et_producer_location);
        tvRating = findViewById(R.id.tv_producer_rating);
        tvTotalUser = findViewById(R.id.tv_total_user);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Informacion de su asesor");
        progressBar = findViewById(R.id.progressBar4);
        progressBar.setVisibility(View.INVISIBLE);
        lvComment = findViewById(R.id.lv_comment);
        commentsListAdapter = new CommentsListAdapter(this, comments);
        lvComment.setAdapter(commentsListAdapter);
        chart = findViewById(R.id.chart);
        chart.setDoubleTapToZoomEnabled(false);
        btnRateProducer.setOnClickListener(view -> {
            Intent intent = new Intent(this, RateProducerActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        });
        /*btnChangeProducer.setOnClickListener(view -> {
            Intent intent = new Intent(this, ChangeProducerActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        });*/
        btnBankAccounts.setOnClickListener(view -> {
            Dialog.showBankAccountDialog(this, banks);
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_producer_information;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetProducerRating().execute(Global.URL_GET_PRODUCER_RATING);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private class GetProducerRating extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject producerId = new JsonObject();
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(producerId.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(ProducerInformationActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    ProducerInformation p = gson.fromJson(response.message,ProducerInformation.class);
                    etProducerName.setText(p.getProducerName());
                    etProducerEmail.setText(p.getEmail());
                    if (p.getBanks().size() > 0) {
                        etOwner.setText(p.getBankerName());
                    }
                    tvRating.setText(Format.getFormattedStringOneDecimal(p.getProducerRating()));
                    ratingBar.setRating(p.getProducerRating());
                    tvTotalUser.setText(String.valueOf(p.getTotalUsers()));
                    banks.clear();
                    banks.addAll(p.getBanks());
                    comments.clear();
                    if (p.getComments().size() > 0) {
                        comments.addAll(p.getComments());
                        commentsListAdapter.notifyDataSetChanged();
                    }
                    if (p.getFeatures().size() > 0) {
                        Chart.paintProducerChart(p.getFeatures(), Chart.setData(p.getFeatures()), chart);
                    }
                } else {
                    Global.Toaster.get().showToast(ProducerInformationActivity.this, "Falla de red", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
