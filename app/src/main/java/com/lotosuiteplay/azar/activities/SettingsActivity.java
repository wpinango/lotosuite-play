package com.lotosuiteplay.azar.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.view.MenuItem;

import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.interfaces.ButtonInterface;
import com.lotosuiteplay.azar.models.Setting;
import com.lotosuiteplay.azar.util.FingerPrintDetection;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;


public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Configuraciones");
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new MainPreferenceFragment()).commit();
    }

    public static class MainPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_settings);

            SwitchPreference finger = (SwitchPreference) findPreference("finger_key");
            if (!FingerPrintDetection.detectFingerPrintSensor(getActivity())){
                PreferenceCategory category = (PreferenceCategory) findPreference("category_name");
                category.removePreference(finger);
            } else {
                finger.setChecked(Setting.getSettingConfig(getActivity(), SharedPreferenceConstants.KEY_FINGER).isStatus());
            }
            finger.setOnPreferenceChangeListener((preference, o) -> {
                if (finger.isChecked()) {
                    finger.setChecked(false);
                } else {
                    Dialog.showInputPosPassDialog(getActivity(), new ButtonInterface() {
                        @Override
                        public void onPositiveButtonClick(String buttonAction) {
                            finger.setChecked(true);
                            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putString("pass", buttonAction);
                            edit.apply();
                            saveSetting(SharedPreferenceConstants.KEY_FINGER, finger.isChecked());
                        }

                        @Override
                        public void onNegativeButtonClick(String buttonAction) {

                        }
                    }, "Ingrese su clave");

                }
                saveSetting(SharedPreferenceConstants.KEY_FINGER, finger.isChecked());
                return false;
            });

            SwitchPreference notification = (SwitchPreference) findPreference("notification_key");
            notification.setChecked(Setting.getSettingConfig(getActivity(), SharedPreferenceConstants.KEY_NOTIFICATION).isStatus());
            notification.setOnPreferenceChangeListener((preference, o) -> {
                if (notification.isChecked()) {
                    notification.setChecked(false);
                } else {
                    notification.setChecked(true);
                }
                saveSetting(SharedPreferenceConstants.KEY_NOTIFICATION, notification.isChecked());
                return false;
            });

            SwitchPreference sound = (SwitchPreference) findPreference("sound_key");
            sound.setChecked(Setting.getSettingConfig(getActivity(), SharedPreferenceConstants.KEY_NOTIFICATION_SOUND).isStatus());
            sound.setOnPreferenceChangeListener((preference, o) -> {
                if (sound.isChecked()) {
                    sound.setChecked(false);
                } else {
                    sound.setChecked(true);
                }
                saveSetting(SharedPreferenceConstants.KEY_NOTIFICATION_SOUND, sound.isChecked());
                return false;
            });

            SwitchPreference showRaffle = (SwitchPreference) findPreference("show_raffle_key");
            showRaffle.setChecked(Setting.getSettingConfig(getActivity(), SharedPreferenceConstants.KEY_SHOW_RAFFE).isStatus());
            showRaffle.setOnPreferenceChangeListener((preference, o) -> {
                if (showRaffle.isChecked()) {
                    showRaffle.setChecked(false);
                } else {
                    showRaffle.setChecked(true);
                }
                saveSetting(SharedPreferenceConstants.KEY_SHOW_RAFFE, showRaffle.isChecked());
                return false;
            });
        }

        private void saveSetting(String key, boolean status) {
            Setting s = Setting.getSettingConfig(getActivity(), key);
            s.setStatus(status);
            Setting.saveSetting(getActivity(), s);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            TransitionAnimation.setOutActivityTransition(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        TransitionAnimation.setOutActivityTransition(this);
    }
}
