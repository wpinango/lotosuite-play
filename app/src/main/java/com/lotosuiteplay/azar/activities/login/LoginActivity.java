package com.lotosuiteplay.azar.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lotosuiteplay.azar.activities.MainActivity;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.interfaces.ButtonInterface;
import com.lotosuiteplay.azar.interfaces.FingerAuth;
import com.lotosuiteplay.azar.interfaces.TextListener;
import com.lotosuiteplay.azar.models.LoginMessage;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.sqlite.DBHelper;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.util.FingerPrintDetection;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;
import com.lotosuiteplay.azar.util.UsernameWatcher;
import com.lotosuiteplay.azar.util.Util;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, FingerAuth, TextListener {
    private TextView signupLink, forgotLink;
    private EditText userLogin, passwordText;
    private BootstrapButton loginButton;
    private final int REQUEST_SIGNUP = 0;
    private String status = "";
    private ProgressBar progressBar;
    private HttpRequest request;
    private Gson gson = new Gson();
    private DBHelper dbHelper;
    private boolean isTokenExist = false;
    public static boolean isRegisterSuccess = false;
    private CheckBox cbFingerLogin;
    private Dialog dialog = new Dialog();

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(this);
        setContentView(R.layout.activity_login);
        cbFingerLogin = findViewById(R.id.cb_finger_login);
        userLogin = findViewById(R.id.input_user);
        userLogin.addTextChangedListener(new UsernameWatcher(this));
        forgotLink = findViewById(R.id.tv_forgot_link);
        passwordText = findViewById(R.id.input_pass);
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        cbFingerLogin.setVisibility((FingerPrintDetection.detectFingerPrintSensor(this)) ? View.VISIBLE: View.INVISIBLE);
        cbFingerLogin.setChecked(sharedPreferences.getBoolean("fingerLogin", false));
        userLogin.setText(sharedPreferences.getString("user", "M"));
        Cursor res = dbHelper.getToken();
        Global.selectURL();
        Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        progressBar = findViewById(R.id.pb_login);
        progressBar.setVisibility(View.INVISIBLE);
        if (res.moveToFirst()) {
            if (!res.getString(res.getColumnIndex("token")).equals(null)) {
                isTokenExist = true;
            }
        }
        loginButton = findViewById(R.id.btn_login);
        signupLink = findViewById(R.id.link_signup);
        signupLink.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
            startActivityForResult(intent, REQUEST_SIGNUP);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        });
        forgotLink.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        });
        loginButton.setOnClickListener(v -> {
            try {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            } catch (Exception e) {

            }
            login();
        });
        cbFingerLogin.setOnCheckedChangeListener((compoundButton, b) -> {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("fingerLogin", b);
            edit.apply();
            if (b) {
                if (!userLogin.getText().toString().isEmpty()) {
                    Dialog.showInputPosPassDialog(LoginActivity.this, new ButtonInterface() {
                        @Override
                        public void onPositiveButtonClick(String buttonAction) {
                            Util.hideKeyboard(LoginActivity.this);
                            edit.putString("pass", buttonAction);
                            edit.apply();
                            dialog.showFingerAuthDialog(LoginActivity.this, LoginActivity.this);
                        }

                        @Override
                        public void onNegativeButtonClick(String buttonAction) {

                        }
                    }, "Debe introducir la contraseña de usuario");
                } else {
                    showToast( "Debe colocar el usuario primero");
                    cbFingerLogin.setChecked(false);
                    edit.putBoolean("fingerLogin", false);
                    edit.apply();
                }
            }
        });
        signupLink.setPaintFlags(signupLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        forgotLink.setPaintFlags(forgotLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRegisterSuccess) {
            new LoginAsyncTask().execute(Global.userName, Global.password);
            isRegisterSuccess = false;
        }
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("fingerLogin", false) && FingerPrintDetection.detectFingerPrintSensor(this)) {
            dialog.showFingerAuthDialog(LoginActivity.this, LoginActivity.this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dialog.dismissDialog();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onSuccess() {
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        Global.userName = sharedPreferences.getString("user", "");
        Global.password = sharedPreferences.getString("pass","");
        new LoginAsyncTask().execute(Global.userName, Global.password);
    }

    @Override
    public void onFailed() {
        showToast("Fallo la autenticacion");
    }

    @Override
    public void onChangeText(String text) {
        userLogin.setText(text);
        userLogin.setSelection(1);
    }

    private class LoginAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String user = params[0];
            String password = params[1];
            try {
                request = HttpRequest.post(Global.URL_LOGIN_MOBILE)
                        .accept("application/json")
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .header(Global.KEY_TOKEN_FB, Global.refreshedToken)
                        .basic(user, password)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                System.out.println("valores : " + ex.getMessage());
                setStatus("fail");
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                if (!s.isEmpty()) {
                    try {
                        Response response = new Gson().fromJson(s, Response.class);
                        if (response.error.isEmpty()) {
                            Global.token = request.header(Global.KEY_TOKEN);
                            Global.agencyId = request.header(Global.KEY_AGENCY_ID);
                            Global.userId = request.header(Global.KEY_ID_USER);
                            Global.footerTicket = request.header("x-footer");
                            Global.cash = String.valueOf(response.cash);
                            Global.promotional = response.promotional;
                            Global.timestamp = response.timestamp;
                            SharedPreferenceConstants.saveTimestamp(LoginActivity.this, Global.timestamp);
                            SharedPreferenceConstants.saveData(LoginActivity.this, s);
                            LoginMessage message = gson.fromJson(response.message, LoginMessage.class);
                            Global.currentGames = message.games;
                            Global.producerId = message.producerId;
                            Global.producerName = message.producerName;
                            Global.producerRating = message.producerRating;
                            Global.features = message.producerFeatures;
                            Global.userName = message.username;
                            Global.userLogin = message.userlogin;
                            Global.promotional = response.promotional;
                            Global.nick = message.nick;
                            setStatus("ok");
                        } else {
                            setStatus("fail");
                            Global.Toaster.get().showToast(LoginActivity.this, response.message, Toast.LENGTH_SHORT);
                        }
                    } catch (HttpRequest.HttpRequestException ex) {
                        setStatus("fail");
                        progressBar.setVisibility(View.GONE);
                        Global.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                    }
                    successLogin();
                } else {
                    showToast("Falla de red");
                }
                loginButton.setEnabled(true);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        loginButton.setEnabled(false);
        String user = userLogin.getText().toString().toUpperCase();
        String password = passwordText.getText().toString();
        new LoginAsyncTask().execute(user, password);
    }

    public boolean validate() {
        boolean valid = true;
        String user = userLogin.getText().toString();
        String password = passwordText.getText().toString();
        TextInputLayout textInputLayout = findViewById(R.id.textInputLayout9);
        if (user.isEmpty()) {
            userLogin.setError("Introduzca un usuario valido");
            valid = false;
        } else {
            userLogin.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            textInputLayout.setError("Introduzca una contrasena valida");
            valid = false;
        } else {
            textInputLayout.setError(null);
        }
        return valid;
    }

    private void successLogin() {
        new android.os.Handler().postDelayed(() -> {
            try {
                if (status.equals("fail")) {
                    onLoginFailed();
                } else {
                    onLoginSuccess();
                    if (isTokenExist) {
                        dbHelper.updateToken(1, Global.token, Global.userLogin, Global.agencyId, Global.userId);
                    } else {
                        dbHelper.insertToken(Global.token, Global.userLogin, Global.agencyId, Global.userId);
                    }
                    SharedPreferences preferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putString("user", Global.userLogin);
                    edit.apply();
                    finish();
                }
            } catch (Exception e) {
                onLoginFailed();
            }
        }, 100);
    }


    public void onLoginSuccess() {
        LoginState.setLoginState(this, true);
        Global.isLogin = true;
        progressBar.setVisibility(View.INVISIBLE);
        loginButton.setEnabled(true);
        Global.lastActivityTime = Time.getCurrentTimeInSeconds();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Fallo en Login", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
    }

}