package com.lotosuiteplay.azar.activities;

import com.lotosuiteplay.azar.R;

public class BlockedAppActivity extends BaseActivity {
    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_blocked_app;
    }
}
