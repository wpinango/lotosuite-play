package com.lotosuiteplay.azar.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.RechargeListAdapter;
import com.lotosuiteplay.azar.adapters.SpinnerRechargeAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.interfaces.TextListener;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.BooleanResponse;
import com.lotosuiteplay.azar.models.ItemPending;
import com.lotosuiteplay.azar.models.Recharge;
import com.lotosuiteplay.azar.models.RechargeItemPending;
import com.lotosuiteplay.azar.models.RechargePending;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.TransactionType;
import com.lotosuiteplay.azar.util.AmountWatcher;
import com.lotosuiteplay.azar.util.Util;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RechargeActivity extends BaseActivity implements AsyncktaskListener, TextListener {
    private EditText etCalendar, etReference, etAmount, etCash;
    private Spinner spBank;
    private ArrayList<String> bank = new ArrayList<>();
    private Recharge recharge;
    private String dateRecharge;
    private SweetAlertDialog progressDialog;
    private RadioButton rbDeposit, rbTransf;
    private RechargeListAdapter rechargeListAdapter;
    private ImageButton btnSend;
    private ImageButton btnCancel;
    private ImageButton btnInfo;
    private RechargeItemPending rechargeItemPending = new RechargeItemPending();
    private ProgressBar progressBar;
    private TextView tvDelete,tvInfo;
    private ArrayList<Bank> banks = new ArrayList<>();
    private SpinnerRechargeAdapter spinnerRechargeAdapter;
    public static String refresh = null;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_recharge;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Recargas");
        etCalendar = findViewById(R.id.et_date_recharge);
        etReference = findViewById(R.id.et_reference_recharge);
        ImageButton btnRefresh = findViewById(R.id.btn_refresh_recharge);
        btnCancel = findViewById(R.id.btn_cancel_recharge);
        etAmount = findViewById(R.id.et_amount_recharge);
        etAmount.requestFocus();
        Util.showKeyboard(this);
        spBank = findViewById(R.id.sp_bank);
        rbDeposit = findViewById(R.id.rb_deposit);
        rbTransf = findViewById(R.id.rb_transf);
        rbTransf.setChecked(true);
        etCash = findViewById(R.id.et_cash_recharge);
        try{
            etCash.setText("0");
            //etCash.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
        } catch (Exception e) {
            e.getMessage();
        }
        etAmount.addTextChangedListener(new AmountWatcher(this));
        tvInfo = findViewById(R.id.tv_info_label_recharge);
        btnInfo = findViewById(R.id.btn_info_recharge);
        tvDelete = findViewById(R.id.tv_delete_label_recharge);
        progressBar = findViewById(R.id.pb_refresh_recharge);
        progressBar.setVisibility(View.INVISIBLE);
        rechargeListAdapter = new RechargeListAdapter(this,rechargeItemPending);
        ListView lvRecharge = findViewById(R.id.lv_recharge);
        lvRecharge.setAdapter(rechargeListAdapter);
        etCalendar.setText(com.lotosuiteplay.azar.common.Time.getCurrentDate());
        btnSend = findViewById(R.id.btn_sent_recharge);
        spinnerRechargeAdapter = new SpinnerRechargeAdapter(this, R.layout.spinner_item2, banks);
        spBank.setAdapter(spinnerRechargeAdapter);
        btnCancel.setOnClickListener(v -> {
            btnCancel.setEnabled(false);
            elminateItem();
        });
        etCalendar.setOnClickListener(v -> alertDatePicker());
        spBank.setOnTouchListener((v, event) -> {
            InputMethodManager imm=(InputMethodManager)this.getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etReference.getWindowToken(), 0);
            return false;
        });
        btnSend.setOnClickListener(v -> {
            btnSend.setEnabled(false);
            sendRequest();
        });
        btnRefresh.setOnClickListener(view -> {
            requestPendingRecharges(0);
            disableInfoButton();
            disableDeleteButton();
        });
        if (rechargeListAdapter.isEmpty()){
            requestPendingRecharges(0);
        }
        lvRecharge.setOnItemClickListener((adapterView, view, i, l) -> {
            ItemPending itemPending = rechargeItemPending.getPending().get(i);
            for (int j = 0; j < rechargeItemPending.getPending().size(); j ++ ) {
                if (j == i) {
                    rechargeItemPending.getPending().get(i).setSelected(!rechargeItemPending.getPending().get(i).isSelected());
                }else {
                    rechargeItemPending.getPending().get(j).setSelected(false);
                }
            }
            rechargeListAdapter.notifyDataSetChanged();
            if (itemPending.isSelected() && itemPending.getStatus() == 0){
                enableDeleteButton();
            }else {
                disableDeleteButton();
            }
            if (itemPending.isSelected()) {
                enableInfoButton();
            } else {
                disableInfoButton();
            }
        });
        btnInfo.setOnClickListener(view -> showInformation());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    public void setNewCash(){
        try {
            etCash.setText("0");
            //etCash.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showInformation(){
        if (rechargeItemPending.isItemSelected(rechargeItemPending.getPending())) {
            ItemPending itemPending = rechargeItemPending.getSelectedItem(rechargeItemPending.getPending());
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View View = inflater.inflate(R.layout.dialog_pending_information, null);
            TextView tvAmoutn = View.findViewById(R.id.tv_amount_information);
            TextView tvDate = View.findViewById(R.id.tv_date_information);
            TextView tvType = View.findViewById(R.id.tv_type_information);
            TextView tvObervation = View.findViewById(R.id.tv_observation_information);
            TextView tvCode = View.findViewById(R.id.tv_ref_information);
            TextView tvStatus = View.findViewById(R.id.textView55);
            tvDate.setText((itemPending.getOperationDate()));
            tvObervation.setText(itemPending.getObservations());
            tvCode.setText((itemPending.getRefNumber()));
            tvAmoutn.setText(Format.getCashFormat(itemPending.getAmount()));
            tvType.setText("Recarga bancaria");
            if (itemPending.getStatus() == 0 ) {
                tvStatus.setText("En espera por aprobacion");
            }  else  if (itemPending.getStatus() == 2) {
                tvStatus.setText("Rechazado por el asesor");
            } else  if (itemPending.getStatus() == 3) {
                tvStatus.setText("Anulado por el jugador");
            }
            builder.setTitle("Informacion");
            builder.setCancelable(false);
            builder.setNegativeButton("Cerrar", (dialog, which) -> btnSend.setEnabled(true));
            builder.setView(View);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private void elminateItem() {
        if (rechargeItemPending.isItemSelected(rechargeItemPending.getPending())) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular esta(s) solicitud de recarga?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", (dialogo11, id) -> {
                btnCancel.setEnabled(true);
                disableDeleteButton();
                progressBar.setVisibility(View.VISIBLE);
                requestPendingRecharges(2);
            });
            dialogo1.setNegativeButton("Cancelar", (dialogo112, id) -> btnCancel.setEnabled(true));
            dialogo1.show();
        } else  {
            btnCancel.setEnabled(true);
        }
    }

    private void disableInfoButton() {
        btnInfo.setImageResource(R.drawable.info_dis);
        tvInfo.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableInfoButton() {
        btnInfo.setImageResource(R.drawable.info);
        tvInfo.setTextColor(Color.WHITE);
    }

    private void disableDeleteButton() {
        btnCancel.setImageResource(R.drawable.nul_dis);
        tvDelete.setTextColor(getResources().getColor(R.color.colorDisable));
        //btnDelete.setEnabled(true);
    }

    private void enableDeleteButton(){
        btnCancel.setImageResource(R.drawable.nul_white);
        tvDelete.setTextColor(Color.WHITE);
        //btnDelete.setEnabled(true);
    }

    /*public void getPendingRecharge(){
        progressBar.setVisibility(View.VISIBLE);
        //new RechargeFragment.GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
    }*/

    public void requestPendingTransaction(){
        //new RechargeFragment.GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
    }

    private void sendRequest() {
        if (etAmount.getText().length() != 0 && etReference.getText().length() != 0 && etCalendar.getText().length() != 0
                && (rbDeposit.isChecked() || rbTransf.isChecked()) && !banks.isEmpty()) {
            int type = 0;
            if(rbDeposit.isChecked()) {
                type = TransactionType.TRANSFER_RECHARGE;
            }else if (rbTransf.isChecked()) {
                type = TransactionType.TRANSFER_RECHARGE;
            }
            try {
                recharge = new Recharge();
                recharge.setUserId(Global.userId);
                recharge.setAmount(Integer.parseInt(etAmount.getText().toString()));
                recharge.setOperationDate(etCalendar.getText().toString());
                recharge.setRefNumber(etReference.getText().toString());
                recharge.setType(type);
                recharge.setAccountId(banks.get(spBank.getSelectedItemPosition()).getAccountId());
                confirmDialog();
            }catch (Exception e ) {
            }
        } else if (banks.isEmpty()) {
            btnSend.setEnabled(true);
            Toast.makeText(this,"Su productor no posee cuentas asociadas", Toast.LENGTH_SHORT).show();
        } else {
            btnSend.setEnabled(true);
            Toast.makeText(this,"Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = (DatePicker) view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -6);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(new Time(System.currentTimeMillis()).getTime());
        new AlertDialog.Builder(this).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Confirmar", (dialog, id) -> {
                    int month = myDatePicker.getMonth() + 1;
                    int day = myDatePicker.getDayOfMonth();
                    int year = myDatePicker.getYear();
                    String formattedMonth = "" + month;
                    String formattedDayOfMonth = "" + day;
                    if (month < 10) {
                        formattedMonth = "0" + month;
                    }
                    if (day < 10) {
                        formattedDayOfMonth = "0" + day;
                    }
                    dateRecharge = String.valueOf(formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                    etCalendar.setText(dateRecharge);
                    dialog.cancel();
                }).show();
    }

    private void confirmDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.dialog_confirm_recharge, null);
        TextView tvAmount = (TextView)ViewAlerta.findViewById(R.id.tv_amount_recharge);
        TextView tvReference = (TextView)ViewAlerta.findViewById(R.id.tv_reference_recharge);
        TextView tvDate = (TextView)ViewAlerta.findViewById(R.id.tv_date_recharge);
        TextView tvBank = (TextView)ViewAlerta.findViewById(R.id.tv_bank_recharge);
        tvAmount.setText(Format.getCashFormat(recharge.getAmount()));
        tvReference.setText(String.valueOf(recharge.getRefNumber()));
        tvDate.setText(recharge.getOperationDate());
        tvBank.setText(banks.get(spBank.getSelectedItemPosition()).getName());
        builder.setTitle("¿Desea realizar la recarga?");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", (dialog, which) -> {
            progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            progressDialog.setTitleText("Validando recarga");
            progressDialog.setCancelable(false);
            progressDialog.show();
            btnSend.setEnabled(true);
            requestPendingRecharges(1);
        });
        builder.setNegativeButton("Cerrar", (dialog, which) -> btnSend.setEnabled(true));
        builder.setView(ViewAlerta);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void onTaskExecuted(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.cash.action.UI_UPDATE");
        intent.putExtra("UI_KEY3", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void requestPendingRecharges(int requestStatus) {
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        if (requestStatus == 0) {
            progressBar.setVisibility(View.VISIBLE);
            String jsonData = new Gson().toJson(recharge);
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_GET_PENDING_RECHARGE, jsonData, headers, this).execute();
        } else if (requestStatus == 1) {
            String jsonData = new Gson().toJson(recharge);
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_REQUEST_CASH, jsonData, headers, this).execute();
        } else if (requestStatus == 2) {
            progressBar.setVisibility(View.VISIBLE);
            String jsonData = new Gson().toJson(rechargeItemPending.getSelectedItem(rechargeItemPending.getPending()));
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_DELETE_PENDING_TRANSACTION, jsonData, headers, this).execute();
        }
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        try {
            progressBar.setVisibility(View.INVISIBLE);
            if (progressDialog != null) {
                progressDialog.cancel();
            }
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("")) {
                if (endPoint.equals(Global.URL_GET_PENDING_RECHARGE)) {
                    banks.clear();
                    bank.clear();
                    RechargePending a = new Gson().fromJson(res.message,RechargePending.class);
                    rechargeItemPending.setPending(a.getPending());
                    rechargeItemPending.setStatus(a.isStatus());
                    rechargeListAdapter.notifyDataSetChanged();
                    rechargeItemPending.rate = a.rate;
                    rechargeItemPending.minPaiment = a.minPaiment;
                    TextInputLayout t = findViewById(R.id.textInputLayout5);
                    t.setHint("Credito 1/" + a.rate);
                    banks.addAll(a.getBanks());
                    for (Bank b : banks) {
                        bank.add(b.getName() + "..." + b.getAccount().substring(16,20));
                    }
                    spinnerRechargeAdapter.notifyDataSetChanged();
                    Global.setBalanceResponse(res.cash,res.deferred,res.locked, res.promotional);
                    setNewCash();
                    onTaskExecuted(String.valueOf(res.cash));
                } else if (endPoint.equals(Global.URL_REQUEST_CASH)) {
                    RechargeItemPending a = new Gson().fromJson(res.message, RechargeItemPending.class);
                    if (a.isStatus()){
                        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La peticion de recarga fue enviada, pendiente por aprobacion")
                                .show();
                        requestPendingRecharges(0);
                        Global.setBalanceResponse(res.cash,res.deferred,res.locked, res.promotional);
                    }
                    etAmount.setText("");
                    etCalendar.setText(com.lotosuiteplay.azar.common.Time.getCurrentDate());
                    etReference.setText("");
                } else if (endPoint.equals(Global.URL_DELETE_PENDING_TRANSACTION)) {
                    BooleanResponse booleanResponse = new Gson().fromJson(res.message, BooleanResponse.class);
                    if (booleanResponse.isStatus()) {
                        requestPendingRecharges(0);
                        Toast.makeText(this, "Se anulo transacion", Toast.LENGTH_SHORT).show();
                        Global.setBalanceResponse(res.cash,res.deferred,res.locked, res.promotional);
                    } else {
                        Toast.makeText(this, "No se pudo anular transaccion", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Global.Toaster.get().showToast(this, res.message, Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChangeText(String text) {
        System.out.println(text);
        if (!text.isEmpty()) {
            float a = Float.parseFloat(text);
            a = a / rechargeItemPending.rate;
            System.out.println(a);
            etCash.setText(String.valueOf(a));
        }
    }
}
