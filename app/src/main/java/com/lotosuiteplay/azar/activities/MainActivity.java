package com.lotosuiteplay.azar.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.activities.login.LoginActivity;
import com.lotosuiteplay.azar.adapters.SectionsPagerAdapter;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.dialog.Dialog;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.models.FragmentPosition;
import com.lotosuiteplay.azar.models.GameData;
import com.lotosuiteplay.azar.models.MessageType;
import com.lotosuiteplay.azar.models.RaffleData;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.ResultData;
import com.lotosuiteplay.azar.models.ResultNotification;
import com.lotosuiteplay.azar.models.Rule;
import com.lotosuiteplay.azar.models.SimpleDetail;
import com.lotosuiteplay.azar.singletons.RulesSingleton;
import com.lotosuiteplay.azar.sqlite.DBHelper;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;
import com.lotosuiteplay.azar.util.appRaterLib.AppRater;
import com.lotosuiteplay.azar.widgets.BadgeView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.lotosuiteplay.azar.services.MyFirebaseMessagingService.initUserIdVar;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, AsyncktaskListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private FragmentManager mFragmentManager;
    private final String ACTION_INTENT2 = "com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE";
    private final String ACTION_INTENT = "com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE";
    private final String ACTION_INTENT3 = "com.ruletadigital.ruletaplay.activity.MainActivity.newContract.UI_UPDATE";
    //private TextView tvNew;
    private Toolbar toolbar;
    private Menu menu;
    private RulesSingleton rulesSingleton;
    private TextView tvBadge;
    private int badgeCount = 0;
    private PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        toolbar.setPopupTheme(R.style.PopupTheme);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.viewPager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(myOnPageChangeListener);
        mViewPager.setOffscreenPageLimit(6);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mFragmentManager = getSupportFragmentManager();
        String version = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvVersion = headerView.findViewById(R.id.tv_version);
        tvVersion.setText("version " + version);
        if (showAnimationLabel()) {
            //tvNew = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_help_media));
            initializeCountDrawer();
            //setTexViewAnimation(tvNew);
        }
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        //showDialog();
        AppRater.setPackageName(this.getPackageName());
        AppRater.app_launched(this);
        Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        IntentFilter filter = new IntentFilter(ACTION_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        IntentFilter filterNotifications = new IntentFilter(ACTION_INTENT2);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotification, filterNotifications);
        IntentFilter filterNewContract = new IntentFilter(ACTION_INTENT3);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNewContrat, filterNewContract);
        try {
            int value = getIntent().getIntExtra(MessageType.KEY_MESSAGE, FragmentPosition.notificationFragment);
            String value2 = getIntent().getStringExtra("roulette");
            if (value == FragmentPosition.paymentFragment) {
                mViewPager.setCurrentItem(FragmentPosition.paymentFragment);
            } else if (value == FragmentPosition.rechargeFragment) {
                mViewPager.setCurrentItem(FragmentPosition.rechargeFragment);
            }
            if (value2 != null) {
                launchRouletteActivity(value2);
                mViewPager.setCurrentItem(FragmentPosition.resultFragment);
                mSectionsPagerAdapter.requestResultList();
            }
            getIntent().removeExtra("key");
        } catch (Exception e) {
            e.getMessage();
        }
        initUserIdVar(this);
        rulesSingleton = RulesSingleton.getInstance(this);
        drawer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                closePopupDialog();
                return false;
            }
        });

    }

    public void closePopupDialog() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    public void requestGameRules() {
        Rule.requestGameRules(this);
    }

    public void requestTermsConditions() {
        Rule.requestTermsConditions(this);
    }

    private void setTexViewAnimation(TextView textView) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(400); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textView.setAnimation(anim);
    }

    private void initializeCountDrawer() {
        /*tvNew.setGravity(Gravity.CENTER_VERTICAL);
        tvNew.setTypeface(null, Typeface.BOLD);
        tvNew.setTextColor(getResources().getColor(R.color.colorAccent));
        tvNew.setText("Nuevo");*/
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    private boolean showAnimationLabel() {
        SharedPreferences sharedPreferences = getSharedPreferences("Label", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isShow", true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Global.selectURL();
        try {
            if (Global.token.equals("")) {
                DBHelper dbHelper = new DBHelper(this);
                Cursor res = dbHelper.getToken();
                if (res.moveToFirst()) {
                    Global.token = res.getString(res.getColumnIndex("token"));
                    Global.userId = res.getString(res.getColumnIndex("userid"));
                    Global.agencyId = res.getString(res.getColumnIndex("agency"));
                    Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
                }
            }
            String value = SharedPreferenceConstants.getGeneralData(this, "termConditionsNotification");
            if (value.equals("active")) {
                //setTermsUpdateFlag(true);
                requestTermsConditions();
            }
            String v = SharedPreferenceConstants.getGeneralData(this, "gameRulesNotification");
            if (v.equals("active")){
                requestGameRules();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Global.lastActivityTime = Time.getCurrentTimeInSeconds();
        closePopupDialog();
        return super.dispatchTouchEvent(ev);
    }

    ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            hideKeyboard();
            closePopupDialog();
        }

        @Override
        public void onPageSelected(int position) {
            hideKeyboard();
        }
    };

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        View actionView = menuItem.getActionView();
        tvBadge = actionView.findViewById(R.id.cart_badge);
        setupBadge();
        actionView.setOnClickListener(v -> onOptionsItemSelected(menuItem));
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_cart) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View customPopUp = layoutInflater.inflate(R.layout.custom_menu, null);
            popupWindow = new PopupWindow(customPopUp, ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            if (Build.VERSION.SDK_INT >= 21) {
                popupWindow.setElevation(5.0f);
            }
            View actionBar = findViewById(android.R.id.content).getRootView();
            TextView tvTitle = customPopUp.findViewById(R.id.tv_title);
            TextView tv_badge = customPopUp.findViewById(R.id.badge);
            BadgeView badge = new BadgeView(this);
            badge.setTargetView(tv_badge);
            badge.setBadgeCount(badgeCount);
            if (badgeCount > 0) {
                badge.setVisibility(View.VISIBLE);
            } else {
                badge.setVisibility(View.INVISIBLE);
            }
            popupWindow.showAtLocation(actionBar, Gravity.TOP | Gravity.RIGHT, 0, -20);
            LinearLayout linearLayout = customPopUp.findViewById(R.id.ll_games_rules);
            linearLayout.setOnClickListener(v -> {
                popupWindow.dismiss();
                if (rulesSingleton.getGameRules().getTitle() == null) {
                    requestGameRules();
                } else {
                    if (rulesSingleton.isRulesUpdate() || !rulesSingleton.getGameRules().isAccepted()) {
                        requestGameRules();
                    } else {
                        Dialog.showRulesDialog(MainActivity.this, rulesSingleton.getGameRules());
                    }
                }
            });

            LinearLayout llConfig = customPopUp.findViewById(R.id.ll_config);
            llConfig.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    popupWindow.dismiss();
                }
            });
            LinearLayout llLogout = customPopUp.findViewById(R.id.ll_logout);
            llLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Importante")
                            .setMessage("Desea cerrar la sesion?")
                            .setPositiveButton("Ok", (dialog, which) -> {
                                dialog.cancel();
                                LoginState.setLoginState(MainActivity.this, false);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                new CloseToken().execute(Global.URL_CLOSE_TOKEN);
                                finish();
                            });
                    builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
                    builder.show();
                }
            });
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTermsUpdateFlag(boolean status) {
        if (status) {
            rulesSingleton.setTermsUpdate(true);
            badgeCount = 1;
            setupBadge();
        } else {
            badgeCount = 0;
            setupBadge();
            rulesSingleton.setTermsUpdate(false);
        }
    }

    private void setupBadge() {
        if (tvBadge != null) {
            if (badgeCount == 0) {
                if (tvBadge.getVisibility() != View.GONE) {
                    tvBadge.setVisibility(View.GONE);
                }
            } else {
                tvBadge.setText(String.valueOf(Math.min(badgeCount, 99)));
                if (tvBadge.getVisibility() != View.VISIBLE) {
                    tvBadge.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void showUpdateTermsDialog(Rule r ) {
        Dialog.showUpdateStatusRulesDialog(MainActivity.this, r,
                rule -> {
                    rulesSingleton.setTermsConditions(MainActivity.this, rule);
                    setTermsUpdateFlag(false);
                    SharedPreferenceConstants.saveGeneralData(this, "", "termConditionsNotification");
                });
    }

    private void showUpdateGameRulesDialog(Rule r) {
        Dialog.showUpdateStatusRulesDialog(MainActivity.this, r,
                rule -> rulesSingleton.setGameRules(MainActivity.this, rule));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_user) {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.sing_out) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Importante")
                    .setMessage("Desea cerrar la sesion?")
                    .setPositiveButton("Ok", (dialog, which) -> {
                        dialog.cancel();
                        LoginState.setLoginState(MainActivity.this, false);
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        TransitionAnimation.setOutActivityTransition(MainActivity.this);
                        new CloseToken().execute(Global.URL_CLOSE_TOKEN);
                        finish();
                    });
            builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
            builder.show();
        } else if (id == R.id.nav_share) {
            final String appPackageName = this.getPackageName();
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + "\n\n");
            String shareApp = "\nDescarga " + getResources().getString(R.string.app_name) + " y juega online\n\n";
            String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareApp + " " + link);
            startActivity(Intent.createChooser(sharingIntent, "Seleccione uno"));
        } else if (id == R.id.nav_terms_conditions) {
            if (rulesSingleton.isTermsUpdate()) {
                requestTermsConditions();
            } else {
                if (rulesSingleton.getTermsConditions().isAccepted()) {
                    Dialog.showRulesDialog(this, rulesSingleton.getTermsConditions());
                } else {
                    requestTermsConditions();
                }
            }
        } else if (id == R.id.nav_information_producer) {
            Intent intent = new Intent(this, ProducerInformationActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_rate_app) {
            AppRater.showRateDialog(this);
        } else if (id == R.id.nav_recharge) {
            Intent intent = new Intent(this, RechargeActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_payment) {
            Intent intent = new Intent(this, PaymentActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_block_app) {
            Intent intent = new Intent(this, CalendarActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        if (endPoint.contains(Global.URL_GET_TERM_CONDITIONS)) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("") && !res.message.equals("")) {
                Rule rule = new Gson().fromJson(res.message, Rule.class);
                rule.setTitle("Terminos y Condiciones");
                rule.setAccepted(false);
                if (rulesSingleton.getTermsConditions().getTitle() == null) {
                    showUpdateTermsDialog(rule);
                } else {
                    if (Integer.parseInt(rule.getVersion()) > Integer.parseInt(rulesSingleton.getTermsConditions().getVersion())) {
                        showUpdateTermsDialog(rule);
                    }
                }
            }
        } else if (endPoint.contains(Global.URL_GET_GAME_RULES)) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("") && !res.message.equals("")) {
                Rule rule = new Gson().fromJson(res.message, Rule.class);
                rule.setTitle("Reglas de Juego");
                rule.setAccepted(false);
                if (rulesSingleton.getGameRules().getTitle() == null) {
                    showUpdateGameRulesDialog(rule);
                } else {
                    if (Integer.parseInt(rule.getVersion()) > Integer.parseInt(rulesSingleton.getGameRules().getVersion())) {
                        showUpdateGameRulesDialog(rule);
                    }
                }
            }
        }
    }

    public class CloseToken extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(MainActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    public void setCash() {
        mSectionsPagerAdapter.setCash();
    }

    public void updateBalanceAmounts() {
        mSectionsPagerAdapter.updateBalanceAmount();
    }

    public void refreshTicket() {
        mSectionsPagerAdapter.refreshTickets();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction()) && Global.activityVisible) {
                String value = intent.getStringExtra("UI_KEY2");
                updateCurrentFragmentFromNotification(value);
            }
        }
    };

    protected BroadcastReceiver receiverNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT2.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                setNotificationReceive(value);
            }
        }
    };

    protected BroadcastReceiver receiverNewContrat = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT3.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                //setNotificationReceive(value);
                showNewContract(value);
            }
        }
    };

    private void setNotificationReceive(String value) {
        mSectionsPagerAdapter.updateNotificationReceive(value);
    }

    private void updateCurrentFragmentFromNotification(String value) {

        if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.rechargeFragment) {
            mSectionsPagerAdapter.refreshBalance();
        } else if (value.equals(MessageType.KEY_PAYMENT) && mViewPager.getCurrentItem() == FragmentPosition.paymentFragment) {
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.resultFragment) {
            mSectionsPagerAdapter.requestResultList();
        } else if (value.equals(MessageType.KEY_PAYMENT) && mViewPager.getCurrentItem() == FragmentPosition.balanceFragment) {
            mSectionsPagerAdapter.refreshBalance();
        } else if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.balanceFragment) {
            mSectionsPagerAdapter.refreshBalance();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.ticketFragment) {
            refreshTicket();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.primaryFragment) {
            mSectionsPagerAdapter.refreshRafflesAndChips();
        } else if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.primaryFragment) {
            mSectionsPagerAdapter.refreshRafflesAndChips();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.triplePlayFragment) {
            mSectionsPagerAdapter.updateTriplePlayData();
        }
    }

    private void showNewContract(String content) {
        //setTermsUpdateFlag(true);
    }

    private void launchRouletteActivity(String value) {
        ResultNotification resultNotifications = new Gson().fromJson(value, ResultNotification.class);
        ArrayList<SimpleDetail> simpleDetails = resultNotifications.getResults();
        GameData g = new GameData();
        GameData g1 = new GameData();
        GameData g2 = new GameData();
        LinkedHashMap<String, GameData> data = new LinkedHashMap<>();
        for (SimpleDetail s : simpleDetails) {
            switch (s.getCodeName()) {
                case Global.KEY_SAF:
                    g.chipWinner = s.getChip();
                    data.put(Global.KEY_SAF, g);
                    Global.resultImage[0] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
                case Global.KEY_TIZ:
                    g1.chipWinner = s.getChip();
                    data.put(Global.KEY_TIZ, g1);
                    Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    if (s.getChipDescription().equals(Global.KEY_PINEAPPLE)) {
                        Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), R.drawable.pina);
                    }
                    break;
                case Global.KEY_LOT:
                    g2.chipWinner = s.getChip();
                    data.put(Global.KEY_LOT, g2);
                    Global.resultImage[2] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
            }
        }
        String[] rotation = resultNotifications.getRotationTime().split(",");
        ResultData r = new ResultData();
        r.currentResult = new RaffleData();
        r.currentResult.results = data;
        r.timeRotation = new int[3];
        r.timeRotation[0] = Integer.valueOf(rotation[0]);      //Loto
        r.timeRotation[1] = Integer.valueOf(rotation[1]);      //Tizana
        r.timeRotation[2] = Integer.valueOf(rotation[2]);      //Safari
        r.currentResult.raffleWinSerial = resultNotifications.getSerial();
        r.currentResult.raffleName = resultNotifications.getRaffle();
        Intent intent = new Intent(this, RouletteActivity.class);
        intent.putExtra("roulette", new Gson().toJson(r));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void setNick(String nick) {
        toolbar.setTitle("Lotosuite Play - " + nick);
    }
}