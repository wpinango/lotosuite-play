package com.lotosuiteplay.azar.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.lotosuiteplay.azar.activities.login.LoginActivity;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.models.Setting;
import com.lotosuiteplay.azar.util.CalendarBlockedUtil;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;

/**
 * Created by wpinango on 6/18/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent;
        if (!CalendarBlockedUtil.isTodayBlocked(this)) {
            if (LoginState.isLogin(this)) {
                Setting setting = Setting.getSettingConfig(this, SharedPreferenceConstants.KEY_FINGER);
                if (setting.isStatus()) {
                    intent = new Intent(this, FingerPrintActivity.class);
                } else {
                    intent = new Intent(this, MainActivity.class);
                }

            } else {
                intent = new Intent(this, LoginActivity.class);
            }
        } else {
            intent = new Intent(this, BlockedAppActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
