package com.lotosuiteplay.azar.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

/*import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;*/
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.util.chat.AndroidUtilities;
import com.lotosuiteplay.azar.util.chat.ChatListAdapter;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.models.ChatMessage;
import com.lotosuiteplay.azar.models.Status;
import com.lotosuiteplay.azar.models.UserType;
import com.lotosuiteplay.azar.util.chat.NotificationCenter;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.util.chat.widgets.SizeNotifierRelativeLayout;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by wpinango on 7/30/17.
 */

public class ChatActivity extends AppCompatActivity implements SizeNotifierRelativeLayout.SizeNotifierRelativeLayoutDelegate, NotificationCenter.NotificationCenterDelegate {

    private ListView chatListView;
    private EditText chatEditText1;
    private ArrayList<ChatMessage> chatMessages = new ArrayList<>();
    private ImageView enterChatView1;
    private ChatListAdapter listAdapter;
    private SizeNotifierRelativeLayout sizeNotifierRelativeLayout;
    private int keyboardHeight;
    private boolean keyboardVisible;
    //private FirebaseDatabase database;// = FirebaseDatabase.getInstance();
    //private DatabaseReference myRef;

    private androidx.appcompat.app.ActionBar actionBar;

    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                EditText editText = (EditText) v;
                if (v == chatEditText1) {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                    chatListView.smoothScrollToPosition(chatListView.getAdapter().getCount()-1);
                }
                chatEditText1.setText("");
                return true;
            }
            return false;
        }
    };

    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == enterChatView1) {
                sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
                chatListView.smoothScrollToPosition(chatListView.getAdapter().getCount()-1);
            }
            chatEditText1.setText("");
        }
    };

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (chatEditText1.getText().toString().equals("")) {
            } else {
                enterChatView1.setImageResource(R.drawable.ic_chat_send);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                enterChatView1.setImageResource(R.drawable.ic_chat_send);
            } else {
                enterChatView1.setImageResource(R.drawable.ic_chat_send_active);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        AndroidUtilities.statusBarHeight = getStatusBarHeight();
        chatListView = findViewById(R.id.chat_list_view);
        chatEditText1 = findViewById(R.id.chat_edit_text1);
        enterChatView1 = findViewById(R.id.enter_chat1);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //myRef = FirebaseDatabase.getInstance().getReference();
        listAdapter = new ChatListAdapter(chatMessages, this);
        chatListView.setAdapter(listAdapter);
        chatEditText1.setOnKeyListener(keyListener);
        enterChatView1.setOnClickListener(clickListener);
        chatEditText1.addTextChangedListener(watcher1);
        sizeNotifierRelativeLayout = findViewById(R.id.chat_layout);
        sizeNotifierRelativeLayout.delegate = this;
        actionBar = getSupportActionBar();
        NotificationCenter.getInstance().addObserver(this, NotificationCenter.emojiDidLoaded);
        SharedPreferences sharedPreferences = getSharedPreferences("Chat", Context.MODE_PRIVATE);
        final String chat = sharedPreferences.getString("message", "");
        if (!chat.equals("")) {
            chatMessages.clear();
            Gson gson = new Gson();
            Type custom = new TypeToken<ArrayList<ChatMessage>>() {
            }.getType();
            ArrayList<ChatMessage> cm = gson.fromJson(chat, custom);
            chatMessages.addAll(cm);
            listAdapter.notifyDataSetChanged();
        }
        chatListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        chatListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                MenuInflater menuInflater = actionMode.getMenuInflater();
                menuInflater.inflate(R.menu.menu_item_delete,menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.menu_delete:
                        SparseBooleanArray checkedItemPositions = chatListView.getCheckedItemPositions();
                        final int checkedItemCount = checkedItemPositions.size();
                        for (int i = checkedItemCount - 1; i >= 0; i--) {
                            int key = checkedItemPositions.keyAt(i);
                            if (checkedItemPositions.get(key)) {
                                chatMessages.remove(key);
                            }
                        }
                        checkedItemPositions.clear();
                        listAdapter.notifyDataSetChanged();
                        actionMode.finish();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
                SparseBooleanArray checkedItemPositions = chatListView.getCheckedItemPositions();
                checkedItemPositions.clear();
                ChatMessage.clearArrayMessage(chatMessages);
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode actionMode, int i, long l, boolean b) {
                actionMode.setTitle(""+chatListView.getCheckedItemCount()+" items seleccionados");
                chatMessages.get(i).setSelected(!chatMessages.get(i).isSelected());
                listAdapter.notifyDataSetChanged();
            }
        });
        /*myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                //String message = dataSnapshot.getValue(String.class);

                //System.out.println("firebase values is : " + message);
                Messages m = dataSnapshot.getValue(Messages.class);
                System.out.println("mensajes : " + new Gson().toJson(m));
                //Log.d("mensajes", "mensajes : " + new Gson().toJson(m));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("firebase failed to read value :  " + error );
                //Log.w(TAG, "Failed to read value.", error.toException());
            }
        });*/
    }

    private void sendMessage(final String messageText, final UserType userType) {
        if (messageText.trim().length() == 0) {
            return;
        }
        final ChatMessage message = new ChatMessage();
        message.setMessageStatus(Status.SENT);
        message.setMessageText(messageText);
        message.setUserType(userType);
        message.setMessageTime(new Date().getTime());
        chatMessages.add(message);
        if (listAdapter != null) {
            listAdapter.notifyDataSetChanged();
        }
        // Mark message as delivered after one second
        final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.schedule(new Runnable() {
            @Override
            public void run() {
                message.setMessageStatus(Status.DELIVERED);
                final ChatMessage message = new ChatMessage();
                message.setMessageStatus(Status.SENT);
                message.setMessageText(messageText);
                message.setUserType(UserType.SELF);
                message.setMessageTime(new Date().getTime());
                chatMessages.add(message);
                ChatActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        chatListView.smoothScrollToPosition(chatListView.getAdapter().getCount()-1);
                    }
                });
            }
        }, 1, TimeUnit.SECONDS);
        //myRef.child("chatMessage").setValue(chatMessages);
    }

    private Activity getActivity() {
        return this;
    }

    @Override
    public void onSizeChanged(int height) {
        Rect localRect = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);
        WindowManager wm = (WindowManager) ChatActivity.this.getSystemService(Activity.WINDOW_SERVICE);
        if (wm == null || wm.getDefaultDisplay() == null) {
            return;
        }
        if (height > AndroidUtilities.dp(50) && keyboardVisible) {
            keyboardHeight = height;
            getSharedPreferences("emoji", 0).edit().putInt("kbd_height", keyboardHeight).apply();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationCenter.getInstance().removeObserver(this, NotificationCenter.emojiDidLoaded);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Gson gson = new Gson();
        String data = gson.toJson(chatMessages);
        SharedPreferences preferences = getSharedPreferences("Chat", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("message", data);
        edit.apply();
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }
}
