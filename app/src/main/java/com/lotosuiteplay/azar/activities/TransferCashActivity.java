package com.lotosuiteplay.azar.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.FindUserResponse;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.util.Util;

import java.io.UnsupportedEncodingException;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 7/25/17.
 */

public class TransferCashActivity extends BaseActivity {
    private EditText etCash, etUser, etPassword, etUserName, etAmount;
    private String userName, pass;
    private ProgressBar progressBar;
    private int targetId, amount;
    private ImageView imageIsFinded;
    private ImageButton btnSend, btnBack;
    private final String ACTION_INTENT = "com.ruletadigital.ruletaplay.fragments.cash.action.UI_UPDATE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etUser = findViewById(R.id.et_user_transfer);
        etUser.setSelection(etUser.getText().length());
        progressBar = findViewById(R.id.progressBar6);
        progressBar.setVisibility(View.INVISIBLE);
        etUserName = findViewById(R.id.et_user_name);
        etAmount = findViewById(R.id.et_amount_transfer);
        etPassword = findViewById(R.id.et_password_transfer);
        imageIsFinded = findViewById(R.id.iv_user_is_find);
        btnSend = findViewById(R.id.btn_send_transfer);
        btnBack = findViewById(R.id.imageButton3);
        etCash = findViewById(R.id.et_cash_transfer);
        IntentFilter filter = new IntentFilter(ACTION_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        ImageButton btnFindUser = findViewById(R.id.btn_find_user);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Transferir saldo");
        if (Global.cash != null) {
            etCash.setText(getAmountAllowed());
        }
        etUser.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (userValidation()) {
                    userName = etUser.getText().toString();
                    new FindUser().execute(Global.URL_FIND_USER);
                }
                return true;
            }
            return false;
        });
        btnFindUser.setOnClickListener(view -> {
            if (userValidation()) {
                userName = etUser.getText().toString();
                new FindUser().execute(Global.URL_FIND_USER);
            }
        });
        btnSend.setOnClickListener(view -> {
            if (validation()) {
                btnSend.setEnabled(false);
                showConfirmDialog();
            }
        });
        etUser.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_DEL) {
                imageIsFinded.setImageResource(R.color.transparent);
                etUserName.setText("");
            }
            return false;
        });
        btnBack.setOnClickListener(view -> {
            finish();
            TransitionAnimation.setOutActivityTransition(TransferCashActivity.this);
        });
    }

    private String getAmountAllowed() {
        if (Integer.valueOf(Global.cash) <= 0) {
            return "0";
        }
        return Format.getCashFormat(Integer.valueOf(Global.cash) - Global.promotional);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_transfer_cash;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY3");
                updateCurrentFragmentFromNotification(value);
            }
        }
    };

    private void updateCurrentFragmentFromNotification(String value) {
        etCash.setText(getAmountAllowed());
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(TransferCashActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea realizar esta transferencia?. \n\nNota: El saldo se convertira en saldo especial en el destinatario");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", (dialogInterface, i) -> {
            if (amountValidation()) {
                amount = Integer.valueOf(etAmount.getText().toString());
                if (amount <= (Integer.valueOf(Global.cash) - 1000)) {
                    try {
                        pass = Format.getPEncrypted(etPassword.getText().toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Util.hideKeyboard(this);
                    new SendTransf().execute(Global.URL_SEND_TRANSFER);

                } else {
                    Toast.makeText(TransferCashActivity.this, "Debe ingresar un monto permitido", Toast.LENGTH_SHORT).show();
                }
            }
            btnSend.setEnabled(true);
        });
        dialog.setNegativeButton("Cancelar", (dialogInterface, i) -> btnSend.setEnabled(true));
        dialog.show();
    }

    private boolean validation() {
        boolean valid = true;
        String userName = etUser.getText().toString();
        String cash = etCash.getText().toString();
        String amount = etAmount.getText().toString();
        String pass = etPassword.getText().toString();
        String user = etUser.getText().toString();
        if (userName.isEmpty() || cash.isEmpty() || pass.isEmpty() || user.isEmpty()) {
            Toast.makeText(TransferCashActivity.this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (amount.isEmpty()) {
            Toast.makeText(TransferCashActivity.this, "Debe actualizar su saldo disponible", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (!etAmount.getText().toString().equals("")) {
            if (Integer.valueOf(Global.cash) < Integer.valueOf(amount)) {
                Toast.makeText(TransferCashActivity.this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
                valid = false;
            }
        } else {
            valid = false;
        }
        return valid;
    }

    private boolean userValidation() {
        boolean valid = true;
        String userName = etUser.getText().toString();
        if (userName.isEmpty() || userName.length() < 7 || userName.length() > 8) {
            etUser.setError("Introduzca un usuario valido");
            valid = false;
        } else {
            etUser.setError(null);
        }
        return valid;
    }

    private boolean amountValidation() {
        boolean valid = true;
        String cash = etCash.getText().toString();
        if (cash.isEmpty()) {
            etCash.setError("Introduzca un monto valido");
            valid = false;
        } else {
            etCash.setError(null);
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class FindUser extends AsyncTask<String, Void, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userName", userName);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(TransferCashActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                etUserName.setText("");
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    FindUserResponse f = gson.fromJson(response.message, FindUserResponse.class);
                    if (f.status) {
                        targetId = f.getTargetId();
                        etUserName.setText(f.getName());
                        imageIsFinded.setImageResource(R.drawable.ok);
                    }
                } else if (response.error.equals("not-found")) {
                    Global.Toaster.get().showToast(TransferCashActivity.this, "Usuario no encontrado", Toast.LENGTH_SHORT);
                    imageIsFinded.setImageResource(R.drawable.no);
                } else {
                    imageIsFinded.setImageResource(R.color.transparent);
                    Global.Toaster.get().showToast(TransferCashActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SendTransf extends AsyncTask<String, Void, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("targetId", targetId);
            jsonObject.addProperty("amount", amount);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SECURITY, pass.trim())
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(TransferCashActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    FindUserResponse f = gson.fromJson(response.message, FindUserResponse.class);
                    if (f.status) {
                        new SweetAlertDialog(TransferCashActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La transferencia exitosa")
                                .show();
                        Global.cash = String.valueOf(response.cash);
                        Global.promotional = response.promotional;
                        etCash.setText(getAmountAllowed());
                        etAmount.setText("");
                        etUser.setText("");
                        etUserName.setText("");
                        etPassword.setText("");
                        imageIsFinded.setImageResource(R.color.transparent);
                    } else {
                        new SweetAlertDialog(TransferCashActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Datos incorrectos")
                                .setContentText("no se pudo procesar la transferencia")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(TransferCashActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
