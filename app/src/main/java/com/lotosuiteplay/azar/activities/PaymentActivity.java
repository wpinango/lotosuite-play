package com.lotosuiteplay.azar.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.PaymentListAdapter;
import com.lotosuiteplay.azar.adapters.SpinnerPaymentAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.FourDigitBankAccountFormatWatcher;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.common.TransitionAnimation;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.models.AccountNumber;
import com.lotosuiteplay.azar.models.BooleanResponse;
import com.lotosuiteplay.azar.models.ItemPending;
import com.lotosuiteplay.azar.models.Payment;
import com.lotosuiteplay.azar.models.PaymentPending;
import com.lotosuiteplay.azar.models.RechargeItemPending;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.TransactionType;
import com.lotosuiteplay.azar.util.Util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PaymentActivity extends BaseActivity implements AsyncktaskListener {
    private EditText etAmount, etCode, etAmountPermitted;
    private RadioButton rbEfective, rbTrans;
    private TextView tvDelete, tvInfo;
    private Payment payment;
    private ImageButton btnSend, btnDelete, btnInfo;
    private SweetAlertDialog progressDialog;
    private PaymentListAdapter paymentListAdapter;
    private RechargeItemPending paymentItemPending = new RechargeItemPending();
    private ProgressBar progressBar;
    private Long ts;
    private Spinner spinner;
    private ArrayList<String> accounts = new ArrayList<>();
    private ArrayList<AccountNumber> accountNumbers = new ArrayList<>();
    private SpinnerPaymentAdapter spinnerPaymentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Solicitar Pagos");
        etAmount = findViewById(R.id.et_amount_payment);
        etAmount.requestFocus();
        Util.showKeyboard(this);
        spinner = findViewById(R.id.sp_account);
        spinnerPaymentAdapter = new SpinnerPaymentAdapter(this, R.layout.spinner_item2, accountNumbers);
        spinner.setAdapter(spinnerPaymentAdapter);
        etCode = findViewById(R.id.et_code_payment);
        btnSend = findViewById(R.id.btn_sent_payment);
        etAmountPermitted = findViewById(R.id.et_amount_payment_request);
        ts = System.currentTimeMillis() / 1000;
        etCode.setText(Format.getFormatValidationCode(String.valueOf(ts)));
        final InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        tvDelete = findViewById(R.id.tv_delete_label_payment);
        tvInfo = findViewById(R.id.tv_info_label);
        btnInfo = findViewById(R.id.btn_info_payment);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        btnDelete = findViewById(R.id.btn_delete_payment);
        ImageButton btnRefresh = findViewById(R.id.btn_refresh_payment);
        paymentListAdapter = new PaymentListAdapter(this, paymentItemPending);
        ListView listView = findViewById(R.id.lv_payment);
        listView.setAdapter(paymentListAdapter);
        rbEfective = findViewById(R.id.rb_savings);
        rbTrans = findViewById(R.id.rb_current);
        rbTrans.setChecked(true);
        try {
            etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
        } catch (Exception e) {
            e.getMessage();
        }
        etAmount.addTextChangedListener(new TextWatcher() {
            String afterTextChanged = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                afterTextChanged = etAmount.getText().toString();
                try {
                    if (Integer.parseInt(afterTextChanged) > 10000) {
                        rbTrans.setChecked(true);
                        rbEfective.setEnabled(false);
                        rbTrans.toggle();
                    } else if (Integer.parseInt(afterTextChanged) < 10000) {
                        rbEfective.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        rbEfective.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (rbEfective.isChecked()) {
                imm.hideSoftInputFromWindow(rbEfective.getWindowToken(), 0);
            }
        });
        btnSend.setOnClickListener(view -> {
            btnSend.setEnabled(false);
            sendRequest();
        });
        btnRefresh.setOnClickListener(view -> {
            requestPendingPayments(0);
            disableInfoButton();
            disableDeleteButton();
        });
        if (paymentListAdapter.isEmpty()) {
            requestPendingPayments(0);
        }
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            ItemPending itemPending = paymentItemPending.getPending().get(i);
            for (int j = 0; j < paymentItemPending.getPending().size(); j++) {
                if (j == i) {
                    paymentItemPending.getPending().get(i).setSelected(!paymentItemPending.getPending().get(i).isSelected());
                } else {
                    paymentItemPending.getPending().get(j).setSelected(false);
                }
            }
            paymentListAdapter.notifyDataSetChanged();
            if (itemPending.isSelected() && itemPending.getStatus() == 0) {
                enableDeleteButton();
            } else {
                disableDeleteButton();
            }
            if (itemPending.isSelected()) {
                enableInfoButton();
            } else {
                disableInfoButton();
            }
        });
        btnDelete.setOnClickListener(view -> {
            btnDelete.setEnabled(false);
            deleteItem();
        });
        btnInfo.setOnClickListener(view -> showInformation());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_payment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void showInformation() {
        if (paymentItemPending.isItemSelected(paymentItemPending.getPending())) {
            ItemPending itemPending = paymentItemPending.getSelectedItem(paymentItemPending.getPending());
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View View = inflater.inflate(R.layout.dialog_pending_information, null);
            TextView tvAmoutn = View.findViewById(R.id.tv_amount_information);
            TextView tvDate = View.findViewById(R.id.tv_date_information);
            TextView tvType = View.findViewById(R.id.tv_type_information);
            TextView tvObervation = View.findViewById(R.id.tv_observation_information);
            TextView tvCode = View.findViewById(R.id.tv_ref_information);
            TextView tvStatus = View.findViewById(R.id.textView55);
            tvDate.setText((itemPending.getOperationDate()));
            tvObervation.setText(itemPending.getObservations());
            tvCode.setText(Format.getFormatValidationCode(itemPending.getRefNumber()));
            tvAmoutn.setText(Format.getCashFormat(itemPending.getAmount()));
            if (itemPending.getStatus() == 0) {
                tvStatus.setText("En espera por aprobacion");
            } else if (itemPending.getStatus() == 2) {
                tvStatus.setText("Rechazado por el asesor");
            } else if (itemPending.getStatus() == 3) {
                tvStatus.setText("Anulado por el jugador");
            }
            if (itemPending.getType() == TransactionType.CASH_WITHDRAWAL) {
                tvType.setText("Efectivo");
            } else if (itemPending.getType() == TransactionType.TRANSFER_WITHDRAWAL) {
                tvType.setText("Transferencia");
            }
            builder.setTitle("Informacion");
            builder.setCancelable(false);
            builder.setNegativeButton("Cerrar", (dialog, which) -> btnSend.setEnabled(true));
            builder.setView(View);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private void deleteItem() {
        if (paymentItemPending.isItemSelected(paymentItemPending.getPending())) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular esta(s) solicitud de pago?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", (dialogo11, id) -> {
                btnDelete.setEnabled(true);
                disableDeleteButton();
                progressBar.setVisibility(View.VISIBLE);
                requestPendingPayments(2);
            });
            dialogo1.setNegativeButton("Cancelar", (dialogo112, id) -> btnDelete.setEnabled(true));
            dialogo1.show();
        } else {
            btnDelete.setEnabled(true);
        }
    }

    private void disableInfoButton() {
        btnInfo.setImageResource(R.drawable.info_dis);
        tvInfo.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableInfoButton() {
        btnInfo.setImageResource(R.drawable.info);
        tvInfo.setTextColor(Color.WHITE);
    }

    private void disableDeleteButton() {
        btnDelete.setImageResource(R.drawable.nul_dis);
        tvDelete.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableDeleteButton() {
        btnDelete.setImageResource(R.drawable.nul_white);
        tvDelete.setTextColor(Color.WHITE);
    }

    private void requestPayment() {
        try {
            progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            progressDialog.setTitleText("Validando pago");
            progressDialog.setCancelable(false);
            progressDialog.show();
            requestPendingPayments(1);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private int getAmountAllowed() {
        if (Integer.valueOf(Global.cash) <= 0) {
            return 0;
        }
        return Integer.valueOf(Global.cash) - Global.promotional;
    }

    private void sendRequest() {
        if (etAmount.getText().length() != 0 && etCode.getText().length() != 0
                && (rbEfective.isChecked() || rbTrans.isChecked())) {
            payment = new Payment();
            if ((getAmountAllowed()) >= Integer.valueOf(etAmount.getText().toString()) && Integer.valueOf(etAmount.getText().toString()) > 0) {
                if (rbTrans.isChecked()) {
                    if (!accountNumbers.isEmpty()) {
                        AccountNumber accountNumber = accountNumbers.get(spinner.getSelectedItemPosition());
                        payment.setType(TransactionType.TRANSFER_WITHDRAWAL);
                        payment.setTypeName("Transferencia");
                        payment.setAccountNumber(accountNumber.getAccountNumber());
                        payment.setAmount(Integer.parseInt(etAmount.getText().toString()));
                        payment.setOperationDate(Time.getCurrentDate());
                        payment.setRefNumber(String.valueOf(ts));
                        payment.setUserId(Global.userId);
                        payment.setAccountId(accountNumber.getAccountId());
                        confirmDialog();
                    } else {
                        btnSend.setEnabled(true);
                        Toast.makeText(this, "Debe tener al menos una cuenta registrada en Mis Cuentas Bancarias", Toast.LENGTH_SHORT).show();
                    }
                } else if (rbEfective.isChecked()) {
                    payment.setType(TransactionType.CASH_WITHDRAWAL);
                    payment.setTypeName("Efectivo");
                    payment.setAccountNumber("No Aplica");
                    payment.setAmount(Integer.parseInt(etAmount.getText().toString()));
                    payment.setOperationDate(Time.getCurrentDate());
                    payment.setRefNumber(String.valueOf(ts));
                    payment.setUserId(Global.userId);
                    confirmDialog();
                } else {
                    Toast.makeText(this, "Debe introducir un numero de cuenta valido", Toast.LENGTH_SHORT).show();
                    btnSend.setEnabled(true);
                }
            } else {
                btnSend.setEnabled(true);
                Toast.makeText(this, "Monto excede de su saldo disponible", Toast.LENGTH_SHORT).show();
            }
        } else {
            btnSend.setEnabled(true);
            Toast.makeText(this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void confirmDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View View = inflater.inflate(R.layout.dialog_confirm_payment, null);
        TextView tvAmount = View.findViewById(R.id.tv_amount_payment);
        TextView tvType = View.findViewById(R.id.tv_type_payment);
        TextView tvAcoount = View.findViewById(R.id.tv_account_payment);
        TextView tvCode = View.findViewById(R.id.tv_code_payment);
        tvAcoount.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        tvAmount.setText(Format.getCashFormat(payment.getAmount()));
        tvType.setText(String.valueOf(payment.getTypeName()));
        tvAcoount.setText(payment.getAccountNumber());
        tvCode.setText(Format.getFormatValidationCode(payment.getRefNumber()));
        builder.setTitle("¿Desea realizar este pago?");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", (dialog, which) -> {
            requestPayment();
            btnSend.setEnabled(true);
        });
        builder.setNegativeButton("Cerrar", (dialog, which) -> btnSend.setEnabled(true));
        builder.setView(View);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void requestPendingPayments(int requestStatus) {
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        if (requestStatus == 0) {
            progressBar.setVisibility(View.VISIBLE);
            String jsonData = new Gson().toJson(payment);
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_GET_PENDING_PAYMENT, jsonData, headers, this).execute();
        } else if (requestStatus == 1) {
            String jsonData = new Gson().toJson(payment);
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_GET_PAYMENT, jsonData, headers, this).execute();
        } else if (requestStatus == 2) {
            progressBar.setVisibility(View.VISIBLE);
            String jsonData = new Gson().toJson(paymentItemPending.getSelectedItem(paymentItemPending.getPending()));
            new GlobalAsyntask.PostMethodAsynctask(Global.URL_DELETE_PENDING_TRANSACTION, jsonData, headers, this).execute();
        }
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        try {
            progressBar.setVisibility(View.INVISIBLE);
            if (progressDialog != null) {
                progressDialog.cancel();
            }
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.equals("")) {
                if (endPoint.equals(Global.URL_GET_PENDING_PAYMENT)) {
                    accounts.clear();
                    accountNumbers.clear();
                    PaymentPending a = new Gson().fromJson(res.message, PaymentPending.class);
                    paymentItemPending.setPending(a.getPending());
                    paymentItemPending.setStatus(a.isStatus());
                    accountNumbers.addAll(a.getAccountNumbers());
                    for (int i = 0; i < accountNumbers.size(); i++) {
                        accounts.add(accountNumbers.get(i).getBank() + " ..." +
                                accountNumbers.get(i).getAccountNumber().substring(16, 20));
                    }
                    spinnerPaymentAdapter.notifyDataSetChanged();
                    paymentListAdapter.notifyDataSetChanged();
                    Global.setBalanceResponse(res.cash, res.deferred, res.locked, res.promotional);
                    etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                } else if (endPoint.equals(Global.URL_GET_PAYMENT)) {
                    RechargeItemPending a = new Gson().fromJson(res.message, RechargeItemPending.class);
                    if (a.isStatus()) {
                        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La solicitud de pago fue enviada correctamente, sera procesada proximamente.")
                                .show();
                        requestPendingPayments(0);
                        etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                        Global.setBalanceResponse(res.cash, res.deferred, res.locked, res.promotional);
                    }
                    ts = System.currentTimeMillis() / 1000;
                    etCode.setText(Format.getFormatValidationCode(String.valueOf(ts)));
                    etAmount.setText("");
                } else if (endPoint.equals(Global.URL_DELETE_PENDING_TRANSACTION)) {
                    BooleanResponse booleanResponse = new Gson().fromJson(res.message, BooleanResponse.class);
                    if (booleanResponse.isStatus()) {
                        paymentItemPending.getPending().get(paymentItemPending.getItemIdSelected(paymentItemPending.getPending())).setStatus(3);
                        paymentListAdapter.notifyDataSetChanged();
                        Toast.makeText(this, "Se anulo transacion", Toast.LENGTH_SHORT).show();
                        Global.setBalanceResponse(res.cash, res.deferred, res.locked, res.promotional);
                        etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                    } else {
                        Toast.makeText(this, "No se pudo anular transaccion", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Global.Toaster.get().showToast(this, res.message, Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
