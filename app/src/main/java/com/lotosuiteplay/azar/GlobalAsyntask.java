package com.lotosuiteplay.azar;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;

import java.util.Map;

/**
 * Created by wpinango on 7/10/17.
 */

public class GlobalAsyntask {

    private static int timeout = 30000;


    public static class GetMethodAsynctask extends AsyncTask<String, String, String> {
        private String requestHeaders, response, url;
        private HttpRequest request;
        private Map<String,String> headers;
        private AsyncktaskListener listener;

        public GetMethodAsynctask(String url, Map<String, String> headers,
                                  AsyncktaskListener listener) {
            this.headers = headers;
            this.listener = listener;
            this.url = url;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.get(url)
                        .accept("application/json")
                        .headers(headers)
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            listener.onAsynctaskFinished(url, s,requestHeaders);
        }
    }

    public static class PostMethodAsynctask extends AsyncTask<String, String, String> {
        private String response, requestHeaders, url;
        private HttpRequest request;
        private Map<String, String> headers;
        private String body;
        private AsyncktaskListener listener;

        public PostMethodAsynctask(String url, String body, Map<String,String> headers,
                                   AsyncktaskListener listener) {
            this.headers = headers;
            this.listener = listener;
            this.body = body;
            this.url = url;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(url)
                        .accept("application/json")
                        .headers(headers)
                        .contentType("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout)
                        .send(body);
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            }  catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            listener.onAsynctaskFinished(url, s, requestHeaders);
        }
    }
}
