package com.lotosuiteplay.azar.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.activities.MainActivity;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.fragments.NotificationsFragment;
import com.lotosuiteplay.azar.fragments.ResultFragment;
import com.lotosuiteplay.azar.models.FragmentPosition;
import com.lotosuiteplay.azar.models.MessageType;
import com.lotosuiteplay.azar.models.Notification;
import com.lotosuiteplay.azar.models.ResultNotification;
import com.lotosuiteplay.azar.models.Setting;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.roulette.Roulette;
import com.lotosuiteplay.azar.sqlite.DBHelper;
import com.lotosuiteplay.azar.util.notification.NotificationHelper;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.lotosuiteplay.azar.models.Setting.getSettingsConfig;

/**
 * Created by wpinango on 8/31/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static String TAG = "MyFirebaseMsgService";
    private NotificationID notificationID = new NotificationID();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            initUserIdVar(this);
            if (remoteMessage.getData().size() > 0) {
                ArrayList<Setting> settings = getSettingsConfig(this);
                for (Setting setting : settings) {
                    switch (setting.getTAG()) {
                        case SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION:
                            if (setting.isStatus() && !Global.isNotificationShow && LoginState.isLogin(this)) {
                                sendNotification(remoteMessage);
                            }
                            break;
                        case SharedPreferenceConstants.KEY_NOTIFICATION_SOUND:
                            if (setting.isStatus() && LoginState.isLogin(this)) {
                                makeNotificationSound();
                            }
                            break;
                        case SharedPreferenceConstants.KEY_SHOW_RAFFE:
                            if (setting.isStatus() && Global.activityVisible && remoteMessage.getData().containsKey("result") && LoginState.isLogin(this)) {
                                Roulette.launchRouletteActivity(this,
                                        remoteMessage.getData().get("result"));
                            }
                            break;
                    }
                }
                saveIncomingNotification(remoteMessage.getData());
                if (Global.isNotificationShow && LoginState.isLogin(this)) {
                    sendNotificationToFragmentsView(new Gson().toJson(remoteMessage.getData()));
                } else if (!Global.isNotificationShow && LoginState.isLogin(this)) {
                    checkNotificationMessageType(remoteMessage.getData());
                }
            }

            if (remoteMessage.getNotification() != null) {
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void initUserIdVar(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        Cursor res = dbHelper.getToken();
        if (res.moveToFirst()) {
            Global.userId = res.getString(res.getColumnIndex("userid"));
        }
    }

    private void saveIncomingNotification(Map<String, String> data) {
        try {
            ArrayList<Notification> a = new ArrayList<>();
            if (!NotificationsFragment.getSavedNotifications(this).equals("")) {
                a = new ArrayList<>(Arrays.asList(new Gson().fromJson(NotificationsFragment.getSavedNotifications(this), Notification[].class)));
            }
            if (data.containsKey("title") && !data.get("title").equals("")) {
                Notification notification = new Notification();
                notification.setTitle(data.get("title"));
                notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                a.add(0, notification);
            }
            if (data.containsKey("result")) {
                Notification n = new Notification();
                ResultNotification resultNotification = new Gson().fromJson(data.get("result"), ResultNotification.class);
                n.setResult(resultNotification);
                n.setTitle("Ver repeticion de resultado de " + resultNotification.getRaffle() + " en la ruleta");
                n.setHour(Time.getNotificationDate(data.get("notificationDate")));
                a.add(0, n);
            }
            NotificationsFragment.saveNotifications(this, new Gson().toJson(a));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void checkNotificationMessageType(Map<String, String> data) {
        try {
            if (data.containsKey(MessageType.KEY_MESSAGE)) {
                String dataContains = data.get(MessageType.KEY_MESSAGE);
                if (dataContains.equals(MessageType.KEY_PAYMENT)) {
                    //PaymentFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RECHARGE)) {
                    //RechargeFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RESULT)) {
                    ResultFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_GAME_RULES)) {
                    SharedPreferenceConstants.saveGeneralData(this, "active", "gameRulesNotification");
                    //sendNewContract(new Gson().toJson(data));
                }
                if (dataContains.equals(MessageType.KEY_TERMS_CONDITIONS)) {
                    SharedPreferenceConstants.saveGeneralData(this, "active", "termConditionsNotification");
                    //sendNewContract(new Gson().toJson(data));
                }
                updateFragmentViewFromNotification(data.get(MessageType.KEY_MESSAGE));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void sendNewContract(String body){
        Intent intent = new Intent("com.ruletadigital.ruletaplay.activity.MainActivity.newContract.UI_UPDATE");
        intent.putExtra("UI_KEY", body);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    protected void sendNotificationToFragmentsView(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        //{"title":"Actualización de las Reglas del Juego","notificationDate":"1587603785000"}
    }

    private void updateFragmentViewFromNotification(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY2", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    /*private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.lotoplay);
        if (remoteMessage.getData().containsKey("result") && !Global.activityVisible) {
            Intent intent = new Intent(this, MainActivity.class);
            ResultNotification resultNotification = new Gson().fromJson(remoteMessage.getData()
                    .get("result"), ResultNotification.class);
            intent.putExtra("roulette", new Gson().toJson(resultNotification));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.lotoplay)
                        .setContentTitle("LotoSuite Play")
                        .setContentText("Ver repeticion de resultado de " + resultNotification.getRaffle()
                                + " en la ruleta")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, notificationBuilder.build());
            } else {
                NotificationHelper noti = new NotificationHelper(this);
                android.app.Notification.Builder nb = null;
                nb = noti.getNotification2("LotoSuite Play", "Ver repeticion de resultado de "
                        + resultNotification.getRaffle() + " en la ruleta", R.mipmap.lotoplay, bm, pendingIntent);
                noti.notify(1, nb);
            }
        }
        if (remoteMessage.getData().containsKey("title") && !remoteMessage.getData().get("title").equals("")) {
            Intent intent = new Intent(this, MainActivity.class);
            if (remoteMessage.getData().containsKey(MessageType.KEY_MESSAGE)) {
                String value = remoteMessage.getData().get(MessageType.KEY_MESSAGE);
                if (value.equals(MessageType.KEY_PAYMENT)) {
                    intent.putExtra(MessageType.KEY_MESSAGE, FragmentPosition.paymentFragment);
                } else if (value.equals(MessageType.KEY_RECHARGE)) {
                    intent.putExtra(MessageType.KEY_MESSAGE, FragmentPosition.rechargeFragment);
                } else if (value.equals(MessageType.KEY_SYSTEM_MESSAGE)) {
                    intent.putExtra(MessageType.KEY_SYSTEM_MESSAGE, FragmentPosition.notificationFragment);
                } else if (value.equals(MessageType.KEY_TERMS_CONDITIONS)) {
                    intent.putExtra(MessageType.KEY_TERMS_CONDITIONS, FragmentPosition.primaryFragment);
                } else if (value.equals(MessageType.KEY_GAME_RULES)) {
                    intent.putExtra(MessageType.KEY_GAME_RULES, FragmentPosition.primaryFragment);
                }
            }
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.lotoplay)
                        .setContentText(remoteMessage.getData().get("title"))
                        .setContentTitle("LotoSuite Play")
                        .setAutoCancel(true)
                        .setNumber(1)
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(notificationID.getID(), notificationBuilder.build());
            } else {
                NotificationHelper noti = new NotificationHelper(this);
                android.app.Notification.Builder nb = null;
                nb = noti.getNotification2("LotoSuite Play", remoteMessage.getData().get("title"), R.mipmap.lotoplay, bm, pendingIntent);
                noti.notify(notificationID.getID(), nb);
            }

        }
    }

    private void makeNotificationSound() {
        try {
            Vibrator vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            vb.vibrate(600);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class NotificationID {
        private final AtomicInteger c = new AtomicInteger(0);

        private int getID() {
            return c.incrementAndGet();
        }
    }
}