package com.lotosuiteplay.azar.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.BankListAdapter;
import com.lotosuiteplay.azar.interfaces.ButtonInterface;
import com.lotosuiteplay.azar.interfaces.FingerAuth;
import com.lotosuiteplay.azar.interfaces.RulesUpdateInterface;
import com.lotosuiteplay.azar.models.Bank;
import com.lotosuiteplay.azar.models.Rule;
import com.lotosuiteplay.azar.util.FingerprintHandler;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;


public class Dialog {
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private static final String KEY_NAME = "yourKey";
    private AlertDialog alertDialog;


    public void dismissDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public static void showDialogQuestionDialog(Context context, String title, String body, com.lotosuiteplay.azar.interfaces.DialogInterface dialogInterface, String positiveTextButton, String negativeTextButton) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setPositiveButton(positiveTextButton, (dialog, which) -> dialogInterface.positiveClick(""))
                .setNegativeButton(negativeTextButton, (dialog, which) -> dialogInterface.negativeClick())
                .show();
    }

    public static void showRulesDialog(Context context, Rule rule) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_terms_and_conditions , null);
        TextView tvContent = view.findViewById(R.id.tv_content);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.setText(Html.fromHtml(rule.getContent(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvContent.setText(Html.fromHtml(rule.getContent()));
        }
        tvContent.setMovementMethod(new ScrollingMovementMethod());
        builder.setPositiveButton("Cerrar", (dialogInterface, i) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        builder.setTitle(rule.getTitle());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    public static void showUpdateStatusRulesDialog(Context context, Rule rule, RulesUpdateInterface rulesUpdateInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_update_rules , null);
        TextView tvContent = view.findViewById(R.id.tv_content);
        //tvContent.setText(rule.getContent());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.setText(Html.fromHtml(rule.getContent(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvContent.setText(Html.fromHtml(rule.getContent()));
        }
        tvContent.setMovementMethod(new ScrollingMovementMethod());
        CheckBox cbAccept = view.findViewById(R.id.cb_accept);
        cbAccept.setText("Aceptar " + rule.getTitle());
        cbAccept.setOnCheckedChangeListener((buttonView, isChecked) -> rule.setAccepted(isChecked));
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        builder.setTitle(rule.getTitle());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (rule.isAccepted()) {
                rulesUpdateInterface.onRuleAccepted(rule);
                alertDialog.dismiss();
            } else {
                Global.showToast(context, "Debe marcar primero en aceptar para poder continuar", 2000);
            }
        });
    }

    public static void showInputPosPassDialog(Context context, ButtonInterface listener, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_input_pass , null);
        EditText etPass = view.findViewById(R.id.et_owner_pass);
        TextView tvTitle = view.findViewById(R.id.textView10);
        tvTitle.setText(title);
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            if (etPass.getText().toString().equals("")){
                Global.showToast(context, "Introduzca clave de usuario", 2000);
            }  else {
                listener.onPositiveButtonClick(etPass.getText().toString());
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.onNegativeButtonClick("");
            alertDialog.dismiss();
            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        });
    }

    public static void showBankAccountDialog(Context context, ArrayList<Bank> banks) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        builder.setTitle("Cuentas");
        builder.setMessage("Listado de cuentas para realizar las recargas");
        View view = inflater.inflate(R.layout.dialog_bank_accounts, null);
        ListView lvBanks = view.findViewById(R.id.lv_bank_accounts);
        BankListAdapter bankListAdapter = new BankListAdapter(context, banks);
        lvBanks.setAdapter(bankListAdapter);
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    public void showFingerAuthDialog(Context context, FingerAuth fingerAuth) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_finger , null);
        TextView tvMessage = view.findViewById(R.id.tv_finger_message);
        ImageView imgFingerDetection = view.findViewById(R.id.img_finger_detection);
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            keyguardManager = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) context.getSystemService(FINGERPRINT_SERVICE);
            try {
                generateKey();
            } catch (Exception e) {

            }

            if (initCipher()) {
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
                FingerprintHandler helper = new FingerprintHandler(context, new FingerAuth() {
                    @Override
                    public void onSuccess() {
                        fingerAuth.onSuccess();
                        tvMessage.setText("Verificacion correcta");
                        imgFingerDetection.setImageResource(R.drawable.ic_check_green_36dp);
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onFailed() {
                        tvMessage.setText("Verificacion fallida");
                    }
                });
                helper.startAuth(fingerprintManager, cryptoObject);
            }

        }
    }


    private void generateKey() throws Exception {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");


            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new Exception(exc);
        }


    }


    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }
}
