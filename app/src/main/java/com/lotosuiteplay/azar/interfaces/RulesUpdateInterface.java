package com.lotosuiteplay.azar.interfaces;

import com.lotosuiteplay.azar.models.Rule;

public interface RulesUpdateInterface {
    void onRuleAccepted(Rule rule);
}
