package com.lotosuiteplay.azar.interfaces;

public interface ButtonInterface {
    void onPositiveButtonClick(String buttonAction);
    void onNegativeButtonClick(String buttonAction);

}
