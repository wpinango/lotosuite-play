package com.lotosuiteplay.azar.interfaces;

public interface FingerAuth {

    void onSuccess();
    void onFailed();
}
