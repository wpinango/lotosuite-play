package com.lotosuiteplay.azar.interfaces;

public interface FingerprintStatus {

    void onStatusChange(boolean status);
}
