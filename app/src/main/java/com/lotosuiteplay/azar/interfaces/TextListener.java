package com.lotosuiteplay.azar.interfaces;

public interface TextListener {

    void onChangeText(String text);
}
