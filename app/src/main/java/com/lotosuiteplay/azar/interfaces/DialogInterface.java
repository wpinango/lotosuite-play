package com.lotosuiteplay.azar.interfaces;

public interface DialogInterface {
    void positiveClick(String value);
    void negativeClick();
}
