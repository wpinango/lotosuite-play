package com.lotosuiteplay.azar.common;


import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Chip;
import com.lotosuiteplay.azar.models.Statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Created by wpinango on 9/7/17.
 */

public class OrderArray implements Comparator<Chip>{

    public static void orderArrayListZerosChips(ArrayList<Chip> chips){
        ArrayList<Chip> arrayList = new ArrayList<>();
        Collections.sort(chips);
        for (Chip c: chips) {
            if (c.chipLabel.equals("0")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (c.chipLabel.equals("00")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (c.chipLabel.equals("000")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (!c.chipLabel.equals("0") && !c.chipLabel.equals("00") && !c.chipLabel.equals("000")){
                arrayList.add(c);
            }
        }
        chips.clear();
        chips.addAll(arrayList);
    }

    public static void orderArrayListZeros(ArrayList<Statistics> chips){
        ArrayList<Statistics> lotList = new ArrayList<>();
        ArrayList<Statistics> tizList = new ArrayList<>();
        ArrayList<Statistics> safList = new ArrayList<>();
        ArrayList<Statistics> arrayList = new ArrayList<>();

        for (Statistics s: chips) {
            if (s.getCodeName().equals(Global.KEY_SAF) ) {
                safList.add(s);
            } else if (s.getCodeName().equals(Global.KEY_TIZ)) {
                tizList.add(s);
            } else if(s.getCodeName().equals(Global.KEY_LOT)) {
              lotList.add(s);
            }
        }
        for (int i = 0; i < chips.size() / 3; i++) {
            arrayList.add(safList.get(i));
            arrayList.add(tizList.get(i));
            arrayList.add(lotList.get(i));
        }
        chips.clear();
        chips.addAll(arrayList);
    }

    public static ArrayList<Statistics> orderByGame(ArrayList<Statistics> chips, String codeName){
        ArrayList<Statistics> arrayList = new ArrayList<>();

        for (Statistics s: chips) {
            if (s.getCodeName().equals(codeName) ) {
                arrayList.add(s);
            }
        }
        //chips.clear();
        //chips.addAll(arrayList);
        return arrayList;
    }

    public static String setLotteryImage(String codeName){
        switch (codeName){
            case Global.KEY_TIZ:
                return "tizana2";
            case Global.KEY_SAF:
                return "safari2";
            case Global.KEY_LOT:
                return "loto2";
        }
        return "";
    }

    @Override
    public int compare(Chip chip, Chip t1) {
        return 0;
    }

    @Override
    public Comparator<Chip> reversed() {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparing(Comparator<? super Chip> other) {
        return null;
    }

    @Override
    public <U> Comparator<Chip> thenComparing(Function<? super Chip, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<Chip> thenComparing(Function<? super Chip, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingInt(ToIntFunction<? super Chip> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingLong(ToLongFunction<? super Chip> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingDouble(ToDoubleFunction<? super Chip> keyExtractor) {
        return null;
    }
}
