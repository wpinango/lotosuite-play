package com.lotosuiteplay.azar.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lotosuiteplay.azar.activities.MainActivity;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.BooleanResponse;
import com.lotosuiteplay.azar.models.Details;
import com.lotosuiteplay.azar.models.MyTickets;
import com.lotosuiteplay.azar.models.SimpleDetail;
import com.lotosuiteplay.azar.models.Ticket;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.adapters.TicketListAdapter;
import com.lotosuiteplay.azar.adapters.TicketListDetailAdapter;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lotosuiteplay.azar.widgets.OwnTextView;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TicketFragment extends Fragment {
    private HashMap<String, Details> allTickets2 = new HashMap<>();
    private OwnTextView etTotalAmount, etAward;
    private ProgressBar progressBar;
    private String dateTicket;
    private ArrayList<Ticket> tickets = new ArrayList<>();
    private TicketListAdapter ticketListAdapter;
    private ArrayList<SimpleDetail> simpleDetails = new ArrayList<>();
    private TicketListDetailAdapter ticketListDetailAdapter;
    private ListView lvTicket;
    private ImageButton btnCancel, btnCopy;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageButton btnSearchDate;
    private int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (inflater.inflate(R.layout.fragment_ticket, container, false));
        lvTicket = rootView.findViewById(R.id.lv_repeat_raffles);
        final ListView lvDetails = rootView.findViewById(R.id.lv_detail);
        etTotalAmount = rootView.findViewById(R.id.ownTextView4);
        etAward = rootView.findViewById(R.id.ownTextView3);
        mSwipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh);
        progressBar = rootView.findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.INVISIBLE);
        ticketListAdapter = new TicketListAdapter(getActivity(), tickets);
        lvTicket.setAdapter(ticketListAdapter);
        ticketListDetailAdapter = new TicketListDetailAdapter(getActivity(), simpleDetails);
        lvDetails.setAdapter(ticketListDetailAdapter);
        btnSearchDate = rootView.findViewById(R.id.btn_search_ticket);
        ImageButton btnRefresh = rootView.findViewById(R.id.btn_refresh_ticket);
        btnCancel = rootView.findViewById(R.id.btn_dicard_ticket);
        btnRefresh.setOnClickListener(v -> refreshContent());
        btnSearchDate.setOnClickListener(v -> {
            btnSearchDate.setEnabled(false);
            alertDatePicker();
        });
        lvTicket.setOnItemClickListener((parent, view, position, id) -> {
            populateDetailList(position);
            this.position = position;
        });
        btnCancel.setOnClickListener(v -> {
            if (!ticketListAdapter.isEmpty()) {
                if (tickets.get(Global.selectedPosition).details.getStatus() == 2) {
                    Toast.makeText(getContext(), "Ticket ya anulado, seleccione otro", Toast.LENGTH_SHORT).show();
                } else {
                    btnCancel.setEnabled(false);
                    cancelTicket();
                }
            } else {
                Toast.makeText(getActivity(), "Debe Seleccionar un ticket", Toast.LENGTH_SHORT).show();
            }

        });
        mSwipeRefreshLayout.setOnRefreshListener(() -> refreshContent1());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!tickets.isEmpty()) {
            populateDetailList(0);
        }
    }

    private void refreshContent1() {
        dateTicket = com.lotosuiteplay.azar.common.Time.getCurrentDate();
        new GetTickets().execute(Global.URL_ALL_TICKET);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void refreshContent() {
        dateTicket = com.lotosuiteplay.azar.common.Time.getCurrentDate();
        new GetTickets().execute(Global.URL_ALL_TICKET);
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void cancelTicket() {
        if (!ticketListAdapter.isEmpty()) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular este ticket?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", (dialogo11, id) -> {
                new DiscardTicket().execute(Global.URL_DISCARD_TICKET);
                btnCancel.setEnabled(true);
            });
            dialogo1.setNegativeButton("Cancelar", (dialogo112, id) -> btnCancel.setEnabled(true));
            dialogo1.show();
        } else {
            btnCancel.setEnabled(true);
            Toast.makeText(getActivity(), "Debe seleccionar un ticket", Toast.LENGTH_SHORT).show();
        }
    }

    private void populateDetailList(int position) {
        Global.selectedPosition = position;
        ticketListAdapter.notifyDataSetChanged();
        lvTicket.smoothScrollToPosition(position);
        Details detail = allTickets2.get(ticketListAdapter.getSerial(position));
        etTotalAmount.setBodyText(Format.getCashFormat(detail.getTotalAmount()));
        etAward.setBodyText(Format.getCashFormat(detail.getAward()));
        simpleDetails.clear();
        for (SimpleDetail sd : detail.getItems()) {
            if (sd.getEffectiveSell() != 0) {
                simpleDetails.add(sd);
            }
        }
        ticketListDetailAdapter.notifyDataSetChanged();
    }

    private void checkTicketStatus() {
        int count = 0;
        for (Ticket ticket : tickets) {
            Details details = ticket.getDetails();
            for (SimpleDetail simpleDetail : details.getItems()) {
                if (simpleDetail.getProcessed() == 1 && simpleDetail.getStatus() == 0) {
                    count++;
                }
                if (details.getStatus() == 1) {
                    if (simpleDetail.getProcessed() == 0) {
                        details.setStatus(0);
                    }
                }
            }
            if (count == details.getItems().size()) {
                details.setStatus(4);
                ticketListAdapter.notifyDataSetChanged();
                ticketListDetailAdapter.notifyDataSetChanged();
            }
            count = 0;
        }
    }

    private class GetTickets extends AsyncTask<String, String, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject ticketRequestJson = new JsonObject();
            ticketRequestJson.addProperty("userId", Global.userId);
            ticketRequestJson.addProperty("startDate", dateTicket);
            ticketRequestJson.addProperty("endDate", dateTicket);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(ticketRequestJson.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
                tickets.clear();
                simpleDetails.clear();
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (progressBar != null) {
                progressBar.setVisibility(View.INVISIBLE);
            }
            try {
                tickets.clear();
                simpleDetails.clear();
                ticketListDetailAdapter.notifyDataSetChanged();
                ticketListAdapter.notifyDataSetChanged();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    MyTickets myTickets = gson.fromJson(response.message, MyTickets.class);
                    for (Map.Entry<String, Details> t : myTickets.tickets.entrySet()) {
                        tickets.add(new Ticket(t.getKey(), t.getValue()));
                        allTickets2.put(t.getKey(), t.getValue());
                    }
                    ticketListAdapter.notifyDataSetChanged();
                    if (ticketListAdapter.isEmpty()) {
                        Global.Toaster.get().showToast(getActivity(), "No se encontro tickets para esta fecha", Toast.LENGTH_SHORT);
                        etTotalAmount.setBodyText("0");
                        etAward.setBodyText("0");
                    }
                    populateDetailList(0);
                    if (tickets.size() > 0) {
                        populateDetailList(position);
                    }
                    checkTicketStatus();
                } else {
                    Global.Toaster.get().showToast(getActivity(), "No se encontro tickets para esta fecha", Toast.LENGTH_SHORT);
                    etTotalAmount.setBodyText("0");
                    etAward.setBodyText("0");
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (HttpRequest.HttpRequestException e) {
                progressBar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DiscardTicket extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject serialTicket = new JsonObject();
            serialTicket.addProperty("xSerial", ticketListAdapter.getSerial(Global.selectedPosition));
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(serialTicket.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        Toast.makeText(getActivity(), "Se elimino ticket", Toast.LENGTH_SHORT).show();
                        tickets.get(Global.selectedPosition).details.setStatus(2);
                        for (SimpleDetail t : tickets.get(Global.selectedPosition).getDetails().getItems()) {
                            t.setStatus(2);
                        }
                        ticketListDetailAdapter.notifyDataSetChanged();
                        ticketListAdapter.notifyDataSetChanged();
                        Global.cash = String.valueOf(response.cash);
                        Global.promotional = response.promotional;
                        ((MainActivity) getActivity()).setCash();
                    } else {
                        Toast.makeText(getActivity(), "No se pudo eliminar ticket", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
            }
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -1);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(new Time(System.currentTimeMillis()).getTime());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Confirmar", (dialog, id) -> {
                    int month = myDatePicker.getMonth() + 1;
                    int day = myDatePicker.getDayOfMonth();
                    int year = myDatePicker.getYear();
                    String formattedMonth = "" + month;
                    String formattedDayOfMonth = "" + day;
                    if (month < 10) {
                        formattedMonth = "0" + month;
                    }
                    if (day < 10) {
                        formattedDayOfMonth = "0" + day;
                    }
                    dateTicket = formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2);
                    new GetTickets().execute(Global.URL_ALL_TICKET);
                    progressBar.setVisibility(View.VISIBLE);
                    btnSearchDate.setEnabled(true);
                    dialog.cancel();
                }).show().setCancelable(false);
    }
}
