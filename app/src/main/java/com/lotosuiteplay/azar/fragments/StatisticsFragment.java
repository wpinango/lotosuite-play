package com.lotosuiteplay.azar.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapButtonGroup;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.StatisticsGridAdapter;
import com.lotosuiteplay.azar.common.OrderArray;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.Statistics;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;

public class StatisticsFragment extends Fragment implements AsyncktaskListener {

    private ProgressBar progressBar;
    private ArrayList<Statistics> statistics = new ArrayList<>();
    private ArrayList<Statistics> statistics1 = new ArrayList<>();
    private StatisticsGridAdapter statisticsGridAdapter;
    private ImageButton btnRefresh, btnHistory;
    private BootstrapButton btn15, btn7, btn30, btn1;
    private String starDate, endDate;
    private int status = -1;
    private SegmentedButtonGroup group;
    private BootstrapButtonGroup btnGroup;
    private TextView tvDate;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_statistics, container, false);
        btnRefresh = rootView.findViewById(R.id.btn_refresh_statistics);
        ListView gvStatistics = rootView.findViewById(R.id.gv_statistics);
        statisticsGridAdapter = new StatisticsGridAdapter(getActivity(), statistics1);
        gvStatistics.setAdapter(statisticsGridAdapter);
        mSwipeRefreshLayout = rootView.findViewById(R.id.sw_statistics);
        btnGroup = rootView.findViewById(R.id.button_group_outline_change);
        tvDate = rootView.findViewById(R.id.tv_date);
        group = rootView.findViewById(R.id.segmentedButtonGroup);
        btn7 = rootView.findViewById(R.id.btn_7);
        btn15 = rootView.findViewById(R.id.btn_15);
        btn30 = rootView.findViewById(R.id.btn_30);
        btn1 = rootView.findViewById(R.id.btn_1);
        btnHistory = rootView.findViewById(R.id.btn_search_statistics_history);
        progressBar = rootView.findViewById(R.id.progressBar13);
        if (statistics.isEmpty()) {
            refresh();
        }
        btnRefresh.setOnClickListener(view -> refresh());
        group.setOnClickedButtonListener(position -> {
            status = position;
            updateAdapter(position);
        });
        btn7.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn1.setSelected(false);
                btn30.setSelected(false);
                btn15.setSelected(false);
                prepareCalendar(7);
            }
        });
        btn15.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn1.setSelected(false);
                btn30.setSelected(false);
                btn7.setSelected(false);
                prepareCalendar(15);
            }
        });
        btn30.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn1.setSelected(false);
                btn7.setSelected(false);
                btn15.setSelected(false);
                prepareCalendar(30);
            }

        });
        btn1.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn7.setSelected(false);
                btn15.setSelected(false);
                btn30.setSelected(false);
                prepareCalendar(0);
            }

        });
        btnHistory.setOnClickListener(view -> alertDatePicker());
        mSwipeRefreshLayout.setOnRefreshListener(this::refreshContent);
        return rootView;
    }

    public void refreshContent() {
        mSwipeRefreshLayout.setRefreshing(false);
        refresh();
    }

    private void refresh() {
        btnGroup.setVisibility(View.VISIBLE);
        tvDate.setVisibility(View.INVISIBLE);
        prepareCalendar(0);
        btn1.setSelected(true);
        status = 0;
        updateAdapter(0);
        group.setPosition(0);
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -6);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(System.currentTimeMillis());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Ir", (dialog, id) -> {
                    int month = myDatePicker.getMonth() + 1;
                    int day = myDatePicker.getDayOfMonth();
                    int year = myDatePicker.getYear();
                    String formattedMonth = "" + month;
                    String formattedDayOfMonth = "" + day;
                    if (month < 10) {
                        formattedMonth = "0" + month;
                    }
                    if (day < 10) {
                        formattedDayOfMonth = "0" + day;
                    }
                    starDate = (formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                    endDate = (formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                    tvDate.setVisibility(View.VISIBLE);
                    tvDate.setText(starDate.replace("-","/"));
                    btnGroup.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                    requestChips();
                    btn1.setSelected(false);
                    btn7.setSelected(false);
                    btn15.setSelected(false);
                    btn30.setSelected(false);
                }).show().setCancelable(false);
    }

    private void prepareCalendar(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -days);
        Date date = calendar.getTime();
        starDate = new SimpleDateFormat("dd-MM-yy").format(date);
        endDate = new SimpleDateFormat("dd-MM-yy").format(new Date());
        requestChips();
    }

    private void requestChips() {
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        progressBar.setVisibility(View.VISIBLE);
        JsonObject dateToConsult = new JsonObject();
        dateToConsult.addProperty("startDate", starDate);
        dateToConsult.addProperty("endDate", endDate);
        dateToConsult.addProperty("userId", Global.userId);
        new GlobalAsyntask.PostMethodAsynctask(Global.URL_GET_STATISTICS, dateToConsult.toString(), headers, this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        progressBar.setVisibility(View.INVISIBLE);
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.error.isEmpty()){
                Type custom = new TypeToken<ArrayList<Statistics>>() {
                }.getType();
                ArrayList<Statistics> s = new Gson().fromJson(res.message, custom);
                statistics.clear();
                statistics.addAll(s);
                Collections.sort(statistics);
                updateAdapter(status);
                statisticsGridAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateAdapter(int status) {
        statistics1.clear();
        switch (status) {
            case 0:
                statistics1.addAll(OrderArray.orderByGame(statistics, Global.KEY_SAF));
                break;
            case 1:
                statistics1.addAll(OrderArray.orderByGame(statistics, Global.KEY_TIZ));
                break;
            case 2:
                statistics1.addAll(OrderArray.orderByGame(statistics, Global.KEY_LOT));
                break;
        }
        statisticsGridAdapter.notifyDataSetChanged();
    }
}
