package com.lotosuiteplay.azar.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.GlobalAsyntask;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.adapters.TriplePlayWinnersListAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.interfaces.AsyncktaskListener;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.TriplePlayData;
import com.lotosuiteplay.azar.models.TriplePlayWinner;
import com.lotosuiteplay.azar.models.TriplePlayWinnerHistory;
import com.lotosuiteplay.azar.util.rackMonthPicker.RackMonthPicker;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;

public class TriplePlayFragment extends Fragment implements AsyncktaskListener {
    private TextView tvAccumulated, tvWinnerAmount, tvAccumulatedHour, tvSerial, tvWinnerCount;
    private TriplePlayWinnersListAdapter triplePlayWinnersListAdapter;
    private ArrayList<TriplePlayWinner> triplePlayWinners = new ArrayList<>();
    private ProgressBar progressBar;
    private ImageButton btnRefresh, btnHistoric;
    private String startDate, endDate, month;
    private boolean isHistory = false;
    //private OwnTextView otvHour, otvRaffle;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = (inflater.inflate(R.layout.activity_triple_play, container, false));
        progressBar = rootView.findViewById(R.id.progressBar14);
        triplePlayWinnersListAdapter = new TriplePlayWinnersListAdapter(getActivity(), triplePlayWinners);
        ListView lvWinners = rootView.findViewById(R.id.lv_winners);
        btnHistoric = rootView.findViewById(R.id.btn_search_triple_play);
        btnRefresh = rootView.findViewById(R.id.btn_refresh_triple_play);
        lvWinners.setAdapter(triplePlayWinnersListAdapter);
        tvAccumulated = rootView.findViewById(R.id.tv_accumulated_amount);
        tvWinnerAmount = rootView.findViewById(R.id.tv_winner_amount);
        tvAccumulatedHour = rootView.findViewById(R.id.tv_accumulated_time);
        tvSerial = rootView.findViewById(R.id.tv_accumulated_serial);
        tvWinnerCount = rootView.findViewById(R.id.tv_winner_count);
        requestAccumulated();
        btnRefresh.setOnClickListener(view -> requestAccumulated());
        btnHistoric.setOnClickListener(view -> {
            showMonthPicker();
        });
        /*otvHour = rootView.findViewById(R.id.otv_hour);
        otvRaffle = rootView.findViewById(R.id.otv_raffles);
        otvHour.setTitleText("Horas Acumuladas");
        otvRaffle.setTitleText("Sorteos Acumulados");*/
        return rootView;
    }

    private void showMonthPicker() {
        new RackMonthPicker(getActivity())
                .setPositiveButton((month, startDate, endDate, year, monthLabel) -> {
                    setFirstAndLastDayOfMonth((month - 1), year);
                    RackMonthPicker.currentMonth = month - 1;
                    isHistory = true;
                    requestHistoryData();
                    btnHistoric.setEnabled(true);
                    this.month = monthLabel;
                })
                .setNegativeButton(alertDialog -> {
                    alertDialog.cancel();
                    btnHistoric.setEnabled(true);
                })
                .show();
    }

    private void setFirstAndLastDayOfMonth(int month, int year) {
        Calendar gc = new GregorianCalendar();
        gc.set(Calendar.YEAR, year);
        gc.set(Calendar.MONTH, month);
        gc.set(Calendar.DAY_OF_MONTH, 1);
        Date monthStart = gc.getTime();
        gc.add(Calendar.MONTH, 1);
        gc.add(Calendar.DAY_OF_MONTH, -1);
        Date monthEnd = gc.getTime();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy 00:00:00");
        startDate = format.format(monthStart);
        endDate = format.format(monthEnd);
    }

    private void requestHistoryData() {
        JsonObject dateToConsult = new JsonObject();
        dateToConsult.addProperty("startDate", startDate);
        dateToConsult.addProperty("endDate", endDate);
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        progressBar.setVisibility(View.VISIBLE);
        new GlobalAsyntask.PostMethodAsynctask(Global.URL_GET_TRIPLE_PLAY_HISTORY, dateToConsult.toString(), headers, this).execute();
    }

    public void requestAccumulated() {
        Map<String, String> headers = new LinkedHashMap<>();
        headers.put(Global.KEY_TOKEN, Global.token);
        headers.put(Global.KEY_AGENCY_ID, Global.agencyId);
        headers.put(Global.KEY_ID_USER, Global.userId);
        headers.put(Global.KEY_SCHEMA, Global.KEY_MOVIL);
        progressBar.setVisibility(View.VISIBLE);
        new GlobalAsyntask.GetMethodAsynctask(Global.URL_GET_TRIPLE_PLAY_WINNERS, headers, this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        progressBar.setVisibility(View.INVISIBLE);
        if (!response.isEmpty() || !response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.status == 200 && res.error.equals("")) {
                try {
                    if (endPoint.equals(Global.URL_GET_TRIPLE_PLAY_WINNERS)) {
                        TriplePlayData triplePlayData = new Gson().fromJson(res.message, TriplePlayData.class);
                        int value = triplePlayData.getRaffleSerial() == null ? 0 : triplePlayData.getRaffleSerial();
                        tvSerial.setText(String.valueOf(value));
                        tvAccumulated.setText(Format.getCashFormat(triplePlayData.getAcumulated()));
                        triplePlayWinners.clear();
                        triplePlayWinners.addAll(triplePlayData.getLastTriplePlayWinners());
                        triplePlayWinnersListAdapter.notifyDataSetChanged();
                        float amount = 0.0f;
                        for (TriplePlayWinner t : triplePlayData.getLastTriplePlayWinners()) {
                            amount += Double.parseDouble(t.getAmount());
                        }
                        tvWinnerAmount.setText(Format.getCashFormat(amount));
                        tvAccumulatedHour.setText(Time.convertToHours(triplePlayData.getTimeStampLastWin()));
                        int count = triplePlayData.getLastTriplePlayWinners() == null ? 0 : triplePlayData.getLastTriplePlayWinners().size();
                        tvWinnerCount.setText("Ultimos Ganadores(" + count + ")");
                    } else if (endPoint.equals(Global.URL_GET_TRIPLE_PLAY_HISTORY)) {
                        Type custom = new TypeToken<ArrayList<TriplePlayWinnerHistory>>() {
                        }.getType();
                        triplePlayWinners.clear();
                        ArrayList<TriplePlayWinnerHistory> triplePlayWinnerHistories = new Gson().fromJson(res.message, custom);
                        for (TriplePlayWinnerHistory t : triplePlayWinnerHistories) {
                            TriplePlayWinner triplePlayWinner = new TriplePlayWinner();
                            triplePlayWinner.setAmount(String.valueOf(t.getAward()));
                            triplePlayWinner.setPercent(String.valueOf(t.getPercent()));
                            triplePlayWinner.setName(t.getNick());
                            triplePlayWinners.add(triplePlayWinner);
                        }
                        float amount = 0.0f;
                        for (TriplePlayWinnerHistory t : triplePlayWinnerHistories) {
                            amount += t.getAward();
                        }
                        tvWinnerAmount.setText(Format.getCashFormat(amount));
                        int count = triplePlayWinners == null ? 0 : triplePlayWinners.size();
                        tvWinnerCount.setText("Ganadores de " + month + " (" + count + ")");
                        triplePlayWinnersListAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Global.Toaster.get().showToast(getActivity(), "No hay datos", Toast.LENGTH_SHORT);
                }
            } else {
                Global.Toaster.get().showToast(getActivity(), res.message, Toast.LENGTH_SHORT);
            }
        } else {
            Global.Toaster.get().showToast(getActivity(), "Algo salio mal", Toast.LENGTH_SHORT);
        }
    }
}
