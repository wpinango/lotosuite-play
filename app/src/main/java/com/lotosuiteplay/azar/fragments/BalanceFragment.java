package com.lotosuiteplay.azar.fragments;

import androidx.appcompat.app.AlertDialog;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.lotosuiteplay.azar.adapters.BalanceListAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Balance;
import com.lotosuiteplay.azar.models.MyBalance;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.R;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;
import com.lotosuiteplay.azar.util.rackMonthPicker.RackMonthPicker;
import com.lotosuiteplay.azar.widgets.OwnTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by wpinango on 6/27/17.
 */

public class BalanceFragment extends Fragment {
    private ProgressBar progressBar;
    private ListView lvBalance;
    private ImageButton btnRefresh, btnHelp, btnMonPicker;
    private ArrayList<Balance> balances = new ArrayList<>();
    private String startDateBalance;
    private String endDateBalance;
    private BalanceListAdapter balanceListAdapter;
    private OwnTextView etCashCollect, etCashBloqued, etPromotional, etCashDefered;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String balanceIdTag = "balanceId1";
    private String balanceTag = "balance1";
    private boolean isHistory = false;
    private boolean isRefresh = false;
    private BootstrapButton btn15, btn7, btnToday, btnYesterday;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_balance, null);
        lvBalance = rootView.findViewById(R.id.lv_balance);
        btnRefresh = rootView.findViewById(R.id.btn_refresh_balance);
        btnHelp = rootView.findViewById(R.id.btn_help_balance);
        btnMonPicker = rootView.findViewById(R.id.btn_mon_picker);
        mSwipeRefreshLayout = rootView.findViewById(R.id.sw_balance);
        progressBar = rootView.findViewById(R.id.pb_balance);
        progressBar.setVisibility(View.INVISIBLE);
        etCashCollect = rootView.findViewById(R.id.et_cash);
        etCashBloqued = rootView.findViewById(R.id.et_cash_bloqued);
        etCashDefered = rootView.findViewById(R.id.et_cash_dif);
        etPromotional = rootView.findViewById(R.id.et_promotional);
        btn7 = rootView.findViewById(R.id.btn_7);
        btn15 = rootView.findViewById(R.id.btn_15);
        btnToday = rootView.findViewById(R.id.btn_today);
        btnYesterday = rootView.findViewById(R.id.btn_yesterday);
        /*if (checkSavedDataMonth() && balances.isEmpty()) {
            balances.addAll(getSavedBalances());
        }*/

        balanceListAdapter = new BalanceListAdapter(getActivity(), balances);
        lvBalance.setAdapter(balanceListAdapter);
        RackMonthPicker.currentMonth = Time.getCurrentMonth();
        btnMonPicker.setOnClickListener(v -> {
            btnMonPicker.setEnabled(false);
            showMonthPicker();
        });
        if (balanceListAdapter.isEmpty()) {
            refreshContent();
        }
        btnRefresh.setOnClickListener(v -> {
            isRefresh = true;
            requestDayOperation(0);
            //RackMonthPicker.currentMonth = Time.getCurrentMonth();
            //setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
            //new GetBalance().execute(Global.URL_REQUEST_BALANCE);
        });
        btnHelp.setOnClickListener(v -> showHelp());
        /*if (balanceListAdapter.isEmpty()) {
            isRefresh = true;
            setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
            new GetBalance().execute(Global.URL_REQUEST_BALANCE);
        }*/
        mSwipeRefreshLayout.setOnRefreshListener(() -> refreshContent());

        btnToday.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn15.setSelected(false);
                btn7.setSelected(false);
                btnYesterday.setSelected(false);
                requestDayOperation(0);
            }
        });
        btnYesterday.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn15.setSelected(false);
                btnToday.setSelected(false);
                btn7.setSelected(false);
                requestDayOperation(1);
            }
        });
        btn7.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn15.setSelected(false);
                btnToday.setSelected(false);
                btnYesterday.setSelected(false);
                requestDayOperation(7);
            }
        });
        btn15.setOnCheckedChangedListener((bootstrapButton, isChecked) -> {
            if (isChecked) {
                btn7.setSelected(false);
                btnToday.setSelected(false);
                btnYesterday.setSelected(false);
                requestDayOperation(30);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            etCashCollect.setBodyText(Format.getCashFormat(Integer.parseInt(Global.cash)));
            etCashBloqued.setBodyText(Format.getCashFormat(Global.locked));
            etCashDefered.setBodyText(Format.getCashFormat(Global.deferred));
            etPromotional.setBodyText(Format.getCashFormat(Global.promotional));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void refreshContent() {
        //isRefresh = true;
        //setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
        //new GetBalance().execute(Global.URL_REQUEST_BALANCE);
        mSwipeRefreshLayout.setRefreshing(false);
        requestDayOperation(0);
        btnToday.setSelected(true);
    }

    public void updateBalanceAmounts() {
        try {
            etCashCollect.setBodyText(Format.getCashFormat(Integer.parseInt(Global.cash)));
            etCashBloqued.setBodyText(Format.getCashFormat(Global.locked));
            etCashDefered.setBodyText(Format.getCashFormat(Global.deferred));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showMonthPicker() {
        new RackMonthPicker(getActivity())
                .setPositiveButton((month, startDate, endDate, year, monthLabel) -> {
                    setFirstAndLastDayOfMonth((month - 1), year);
                    RackMonthPicker.currentMonth = month - 1;
                    isHistory = true;
                    new GetBalance().execute(Global.URL_REQUEST_BALANCE);
                    btnMonPicker.setEnabled(true);
                    btnToday.setSelected(false);
                    btnYesterday.setSelected(false);
                    btn7.setSelected(false);
                    btn15.setSelected(false);
                })
                .setNegativeButton(alertDialog -> {
                    alertDialog.cancel();
                    btnMonPicker.setEnabled(true);
                })
                .show();
    }

    private void setFirstAndLastDayOfMonth(int month, int year) {
        Calendar gc = new GregorianCalendar();
        gc.set(Calendar.YEAR, year);
        gc.set(Calendar.MONTH, month);
        gc.set(Calendar.DAY_OF_MONTH, 1);
        Date monthStart = gc.getTime();
        gc.add(Calendar.MONTH, 1);
        gc.add(Calendar.DAY_OF_MONTH, -1);
        Date monthEnd = gc.getTime();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy 00:00:00");
        startDateBalance = format.format(monthStart);
        endDateBalance = format.format(monthEnd);
    }

    private void showHelp() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("RB: Recarga Bancaria");
        arrayList.add("RE: Recarga Efectivo");
        arrayList.add("SR: Saldo   Recibido");
        arrayList.add("ST: Saldo   Transferido");
        arrayList.add("CT: Cobro   Transferencia");
        arrayList.add("CE: Cobro   Efectivo");
        arrayList.add("TJ: Ticket  Jugado");
        arrayList.add("TP: Ticket  Premiado");
        arrayList.add("TA: Ticket  Anulado");
        arrayList.add("3P: Triple  Play");
        arrayList.add("SE: Saldo   Especial");
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, arrayList);
        builder.setAdapter(arrayAdapter, (dialog, which) -> {

        });
        builder.setTitle("Leyenda");
        builder.setNegativeButton("Cerrar", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean checkSavedDataMonth() {
        try {
            ArrayList<Balance> b = new ArrayList<>();
            b.addAll(getSavedBalances());
            int month = Integer.valueOf(b.get(0).getOperationDate().split("-")[1]);
            if (month == Time.getCurrentMonth() + 1) {
                return true;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    private void requestDayOperation(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -days);
        Date date = calendar.getTime();
        startDateBalance = new SimpleDateFormat("dd-MM-yy 00:00:00").format(date);
        endDateBalance = new SimpleDateFormat("dd-MM-yy 00:00:00").format(new Date());
        new GetBalance().execute(Global.URL_REQUEST_BALANCE);
    }

    private class GetBalance extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();
        private String hashBalance;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject dateToConsult = new JsonObject();
            dateToConsult.addProperty("startDate", startDateBalance);
            dateToConsult.addProperty("endDate", endDateBalance);
            try {
                //hashBalance = SharedPreferenceConstants.getGeneralData(getActivity(), balanceIdTag);
                hashBalance = "";
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .header(Global.KEY_HASH_BALANCE, hashBalance)
                        .contentType("application/json")
                        .send(dateToConsult.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;

        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                balances.clear();
                Response response = gson.fromJson(s, Response.class);
                //hashBalance = SharedPreferenceConstants.getGeneralData(getActivity(), balanceIdTag);
                hashBalance = "";
                if (response.error.equals("")) {
                    if (!hashBalance.equals(response.getHashBalance()) || isHistory || isRefresh) {
                        SharedPreferenceConstants.saveGeneralData(getActivity(), response.getHashBalance(),
                                balanceIdTag);
                        MyBalance mb = gson.fromJson(response.message, MyBalance.class);
                        Global.locked = mb.getLocked();
                        Global.deferred = mb.getDeferred();
                        Global.promotional = response.promotional;
                        etCashDefered.setBodyText(Format.getCashFormat(Global.deferred));
                        etCashBloqued.setBodyText(Format.getCashFormat(Global.locked));
                        etPromotional.setBodyText(Format.getCashFormat(Global.promotional));
                        Global.cash = String.valueOf(mb.getAmount());
                        etCashCollect.setBodyText(Format.getCashFormat(Integer.valueOf(Global.cash)));
                        balances.clear();
                        balances.addAll(mb.getBalances());
                        if (!isHistory || isRefresh) {
                            //saveBalance(balances);
                            //((MainActivity) getActivity()).setNewRechargeCash();
                        }
                        balanceListAdapter.notifyDataSetChanged();
                        isHistory = false;
                        isRefresh = false;
                    }
                } else if (response.error.equals("no-content")) {
                    balances.clear();
                    balanceListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void saveBalance(ArrayList<Balance> balances) {
        ArrayList<Balance> balances1 = new ArrayList<>();
        balances1.addAll(filterStorageBalanceByMonth());
        balances1.addAll(balances);
        SharedPreferenceConstants.saveSharedPreferencesData(
                getActivity(), Global.userId, balanceTag, new Gson().toJson(filterDuplicate(balances1)));
    }

    public ArrayList<Balance> getSavedBalances() {
        return filterDuplicate(filterStorageBalanceByMonth());
    }

    private ArrayList<Balance> filterStorageBalanceByMonth() {
        ArrayList<Balance> balances = new ArrayList<>();
        ArrayList<Balance> balances1 = new ArrayList<>();
        if ((!SharedPreferenceConstants.getSharedPreferencesData(getActivity(), Global.userId,
                balanceTag).equals(""))) {
            balances.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.
                    getSharedPreferencesData(getActivity(), Global.userId, balanceTag), Balance[].class)));
            for (Balance b : balances) {
                int month = Integer.valueOf(b.getOperationDate().split("-")[1]);
                if (month == Time.getCurrentMonth() + 1) {
                    balances1.add(b);
                }
            }
        }
        return balances1;
    }

    private ArrayList<Balance> filterDuplicate(ArrayList<Balance> balances) {
        Map<String, Balance> map = new HashMap<>();
        for (Balance b : balances) {
            if (!map.containsKey(b.reference + b.amount)) {
                map.put(b.reference + b.amount , b);
            }
        }

        return filterByDate(new ArrayList<>(map.values()));
    }

    private ArrayList<Balance> filterByDate(ArrayList<Balance> balances) {
        DateFormat f = new SimpleDateFormat("dd-MM-yy hh:mma", new Locale("en", "US"));
        for (Balance b : balances) {
            b.setOperationDate(b.getOperationDate().replace("\n", ""));
        }
        Collections.sort(balances, (o1, o2) -> {
            try {
                return f.parse(o2.operationDate).compareTo(f.parse(o1.operationDate));
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        });
        return balances;
    }
}
