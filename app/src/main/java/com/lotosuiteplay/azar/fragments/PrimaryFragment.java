package com.lotosuiteplay.azar.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.activities.MainActivity;
import com.lotosuiteplay.azar.activities.login.LoginActivity;
import com.lotosuiteplay.azar.adapters.GameListAdapters.ChipsPlayGridAdapter;
import com.lotosuiteplay.azar.adapters.ListConfirmAdapter;
import com.lotosuiteplay.azar.adapters.PlaysListAdapter;
import com.lotosuiteplay.azar.adapters.RaffleListAdapter;
import com.lotosuiteplay.azar.common.Format;
import com.lotosuiteplay.azar.common.LoginState;
import com.lotosuiteplay.azar.common.OrderArray;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.models.Chip;
import com.lotosuiteplay.azar.models.Game;
import com.lotosuiteplay.azar.models.LoginMessage;
import com.lotosuiteplay.azar.models.Raffle;
import com.lotosuiteplay.azar.models.RaffleLabel;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.models.Sell;
import com.lotosuiteplay.azar.models.SellItem;
import com.lotosuiteplay.azar.singletons.RandomModeSingleton;
import com.lotosuiteplay.azar.singletons.RulesSingleton;
import com.lotosuiteplay.azar.sqlite.DBHelper;
import com.lotosuiteplay.azar.util.CompareDate;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;
import com.lotosuiteplay.azar.util.TriplePlayAvailability;
import com.lotosuiteplay.azar.util.random.RandomUtil;
import com.lotosuiteplay.azar.widgets.OwnTextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 10/9/17.
 */

public class PrimaryFragment extends Fragment implements PlaysListAdapter.AdapterCallback{
    private TextView tvRandom;
    private ListView playsListView;
    private ListView raffleGameListView;
    private ImageButton btnPlay, btnCancel, btnRamdon;
    private int total = 0;
    static int cash;
    private DecimalFormat formatter;
    private LinkedHashMap<String, SellItem> myGames = new LinkedHashMap<>();
    private SweetAlertDialog progressDialog;
    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<SellItem> sellItems = new ArrayList<>();
    private PlaysListAdapter playsListAdapter;
    private boolean isPlayAdded = false;
    private RaffleListAdapter raffleListAdapter;
    private ArrayList<RaffleLabel> rafflesList = new ArrayList<>();
    private ArrayList<Chip> chipArrayList = new ArrayList<>();
    private ArrayList<String> gamesNameLogo = new ArrayList<>();
    private int amount;
    private RandomModeSingleton randomModeSingleton;
    private Map<String, Integer> randomMap = new LinkedHashMap<>();
    private OwnTextView etTotal, etBalance;
    private RulesSingleton rulesSingleton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        randomModeSingleton = RandomModeSingleton.getInstance(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_primary, container, false);
        init(rootView);
        try {
            cash = Integer.valueOf(Global.cash);
        } catch (Exception e) {
            e.getMessage();
        }
        etBalance.setBodyText(Format.getCashFormat(cash));
        if (savedInstanceState != null) {
            cash = savedInstanceState.getInt("cash");
        }
        raffleGameListView.setOnItemClickListener((adapterView, view, i, l) -> {
            RaffleLabel.clearRaffleSelection(rafflesList); //TODO pasar raffles
            RaffleLabel.checkRaffleTime(rafflesList);
            rafflesList.get(i).setSelected(true);
            raffleListAdapter.notifyDataSetChanged();
            if (RaffleLabel.isRaffleSelected(rafflesList)) {
                RaffleLabel.setSelectedRaffle(rafflesList);
                isPlayAdded = true;
                raffleGameListView.setEnabled(false);
                if (randomModeSingleton.isRandom()) {
                    showChipsRandomDialog();
                } else {
                    showChipsAvailable();
                }
            }
        });
        playsListView.setOnItemClickListener((parent, view, position, id) -> {
            playsListAdapter.toggleChecked(position);
            if (playsListAdapter.isSelected()) {
                playsListView.setEnabled(false);
                editAmount(position);
            }
        });
        btnPlay.setOnClickListener(v -> {
            btnPlay.setEnabled(false);
            makePlay();
        });
        btnCancel.setOnClickListener(v -> cancelPlay());
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                refreshContent();
            } catch (Exception e) {
                e.getMessage();
            }
        });
        return rootView;
    }

    private void editAmount(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setTitle("Modifique jugada");
        View ViewAlerta = inflater.inflate(R.layout.dialog_plays_edit, null);
        ImageButton btnConfirm = ViewAlerta.findViewById(R.id.imageButton4);
        ImageButton btnClose = ViewAlerta.findViewById(R.id.imageButton7);
        ImageButton btnDelete = ViewAlerta.findViewById(R.id.imageButton5);
        ImageButton btnPlus = ViewAlerta.findViewById(R.id.imageButton2);
        ImageButton btnMinus = ViewAlerta.findViewById(R.id.imageButton);
        EditText tvAmount = ViewAlerta.findViewById(R.id.et_amount_edit);
        btnPlus.setOnClickListener(v -> tvAmount.setText(String.valueOf(Integer.parseInt(tvAmount.getText().toString()) + 100)));
        btnMinus.setOnClickListener(v -> {
            if (!tvAmount.getText().toString().equals("0") && !tvAmount.getText().toString().equals("")) {
                tvAmount.setText(String.valueOf(Integer.parseInt(tvAmount.getText().toString()) - 100));
                if (tvAmount.getText().toString().equals("")) {
                    tvAmount.setText("0");
                }
            }
        });
        SellItem sellItem = myGames.get(playsListAdapter.getString(position));
        tvAmount.setText(String.valueOf(sellItem.amount));
        builder.setCancelable(false);
        builder.setView(ViewAlerta);
        AlertDialog alertDialog = builder.create();
        btnClose.setOnClickListener(view -> {
            alertDialog.dismiss();
            playsListView.setEnabled(true);
            playsListAdapter.updateDelete();
        });
        btnDelete.setOnClickListener(view -> {
            deletePlay();
            alertDialog.dismiss();
            playsListView.setEnabled(true);
        });
        btnConfirm.setOnClickListener(view -> {
            if (Integer.valueOf(tvAmount.getText().toString()) >= 50) {
                sellItem.amount = Integer.valueOf(tvAmount.getText().toString());
                myGames.put(playsListAdapter.getString(position), sellItem);
                addHashMapToAdapter(true);
                setEditAmount();
                alertDialog.dismiss();
                playsListAdapter.updateDelete();
                playsListView.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), R.string.primary_empty_amount_toast, Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.show();
    }

    private void makePlay() {
        if (cash >= getTotalAmount() && !playsListAdapter.isEmpty()) {
            showPlaysConfirmed();
        } else if (cash < getTotalAmount()) {
            Toast.makeText(getActivity(), R.string.primary_insufficient_balance_toast, Toast.LENGTH_SHORT).show();
            btnPlay.setEnabled(true);
        } else if (playsListAdapter.isEmpty()) {
            Toast.makeText(getActivity(), R.string.primary_finish_play_toast, Toast.LENGTH_SHORT).show();
            btnPlay.setEnabled(true);
        }
    }

    public void cancelPlay() {
        if (Global.currentGames != null) {
            randomModeSingleton.setRandom(false);
            setRandomModeView(randomModeSingleton.isRandom());
            resetRandomVars();
            setNewAmount("0");
            for (int i = 0; i < Global.rafflesByGames.size(); i++) {
                Global.rafflesByGames.get(i).setSelected(false);
            }
            RaffleLabel.clearRaffleSelection(rafflesList);
            raffleListAdapter.notifyDataSetChanged();
            myGames.clear();
            sellItems.clear();
            playsListAdapter.notifyDataSetChanged();
            Chip.clearChipSelection();
        }
    }

    private void deletePlay() {
        if (Global.mycheked.containsValue(true)) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea eliminar esta(s) jugada?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", (dialogo11, id) -> {
                Toast.makeText(getActivity(), R.string.primary_play_delete_toast, Toast.LENGTH_SHORT).show();
                for (int i = 0; i < playsListAdapter.getCount(); i++) {
                    if (Global.mycheked.get(i).equals(true)) {
                        if (myGames.containsValue(playsListAdapter.getItem(i))) {
                            myGames.remove(playsListAdapter.getString(i));
                            eliminateAmount(playsListAdapter.getAmount(i));
                        }
                    }
                }
                addHashMapToAdapter(false);
            });
            dialogo1.setNegativeButton("Cancelar", (dialogInterface, i) -> playsListAdapter.updateDelete());
            dialogo1.show();
        }
    }

    public void setCash() {
        cash = Integer.parseInt(Global.cash);
        etBalance.setBodyText(Format.getCashFormat(cash));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.putInt("cash", cash);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Global.currentGames == null) {
                DBHelper dbHelper = new DBHelper(getActivity());
                Cursor res = dbHelper.getToken();
                if (res.moveToFirst()) {
                    Global.token = res.getString(res.getColumnIndex("token"));
                    Global.userId = res.getString(res.getColumnIndex("userid"));
                    Global.agencyId = res.getString(res.getColumnIndex("agency"));
                    Global.timestamp = SharedPreferenceConstants.getSavedTimestamp(getActivity());
                }
                populateSavedJsonData(SharedPreferenceConstants.getSavedData(getActivity()));
                new ApplySynchronization().execute(Global.URL_SYNCRO_MOBILE);
            } else if (Global.currentGames != null && raffleListAdapter.isEmpty()) {
                getData();
            } else {
                if (Time.consultSyncronizationTime(getActivity(), Time.getCurrentTimeInSeconds())) {
                    getData();
                }
            }
            ((MainActivity)getActivity()).setNick(Global.nick);
            cash = Integer.valueOf(Global.cash);
            etBalance.setBodyText(Format.getCashFormat(cash));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void populateSavedJsonData(String jsonData) {
        Response response = new Gson().fromJson(jsonData, Response.class);
        LoginMessage message = new Gson().fromJson(response.message, LoginMessage.class);
        Global.currentGames = message.games;
        Global.cash = String.valueOf(response.cash);
        Global.producerName = message.producerName;
        Global.producerRating = message.producerRating;
        Global.nick = message.nick;
        cash = response.cash;
        Global.promotional = response.promotional;
        getData();
    }

    private void saveIncomingCashPlayer(int amount){
        String s = SharedPreferenceConstants.getSavedData(getActivity());
        Response res = new Gson().fromJson(s, Response.class);
        res.cash = amount;
        SharedPreferenceConstants.saveData(getActivity(),new Gson().toJson(res));
    }

    public void refreshContent() {
        try {
            mSwipeRefreshLayout.setRefreshing(false);
            new ApplySynchronization().execute(Global.URL_SYNCRO_MOBILE);
        } catch (Exception e){
            e.getMessage();
        }
    }

    private void addPlay() {
        if (RaffleLabel.isRaffleSelected(rafflesList) &&
                Chip.isChipSelected()) {
            validateRafflesAndChips();
            isPlayAdded = true;
        } else if (amount < 50) {
            Toast.makeText(getActivity(), R.string.primary_empty_amount_toast, Toast.LENGTH_LONG).show();
        }  else if (!RaffleLabel.isRaffleSelected(rafflesList) || rafflesList.isEmpty()) {
            Toast.makeText(getActivity(), R.string.primary_empty_raffle, Toast.LENGTH_LONG).show();
        } else if (!Chip.isChipSelected()) {
            Toast.makeText(getActivity(), R.string.primary_empty_chip_toast, Toast.LENGTH_SHORT).show();
        }
    }

    private void validateRafflesAndChips() {
        filterRaffle();
        for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
            Game game = games.getValue();
            String gameName = games.getKey();
            LinkedHashMap<Integer, Raffle> raffles = game.getRaffles();
            for (Map.Entry<Integer, Raffle> ra : raffles.entrySet()) {
                int raffleName = ra.getKey();
                Raffle raffle = ra.getValue();
                if (raffle.isSelected()) {
                    for (Chip chip : game.getSelectedChip()) {
                        SellItem sellItem = new SellItem();
                        sellItem.gameId = game.getId();
                        sellItem.raffleId = raffle.getId();
                        sellItem.chipId = chip.id;
                        sellItem.amount = amount;
                        sellItem.game = gameName;
                        sellItem.raffle = raffleName;
                        sellItem.chip = chip.chipLabel;
                        sellItem.codeName = game.getCodeName();
                        sellItem.description = chip.description;
                        String gameRaffleChip = sellItem.game + " " + Format.getRaffleHour(sellItem.raffle) +
                                " \n" + sellItem.chip + " " + chip.description;
                        if (myGames.containsKey(gameRaffleChip)) {
                            SellItem se = new SellItem();
                            se.amount = myGames.get(gameRaffleChip).amount;
                            se.amount += amount;
                            sellItem.amount = se.amount;
                            myGames.put(gameRaffleChip, sellItem);
                        } else {
                            myGames.put(gameRaffleChip, sellItem);
                        }
                        setTotalAmount();
                    }
                }
            }
        }
        addHashMapToAdapter(true);
    }

    private void addHashMapToAdapter(boolean flag) {
        sellItems.clear();
        Global.mycheked.clear();
        int j = 0;
        if (flag) {
            if ((myGames.size() - playsListView.getCount() == 0)) {
                Toast.makeText(getActivity(), "Se actualizo monto", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Se añadieron " + (myGames.size() - playsListView.getCount()) + " jugada(s)", Toast.LENGTH_SHORT).show();
            }
        }
        for (Map.Entry<String, SellItem> entry : myGames.entrySet()) {
            sellItems.add(0, entry.getValue());
            Global.mycheked.put(j, false);
            j++;
        }
        playsListAdapter.notifyDataSetChanged();
    }

    private void eliminateAmount(int amount) {
        setNewAmount(String.valueOf(getTotalAmount() - amount));
    }

    private int getTotalAmount() {
        return total;
    }

    private void setEditAmount() {
        int amount = 0;
        for (SellItem se : sellItems) {
            amount += se.amount;
        }
        setNewAmount(String.valueOf(amount));
    }

    private void setNewAmount(String amount) {
        etTotal.setBodyText(Format.getCashFormat(Integer.valueOf(amount)));
        total = Integer.valueOf(amount);
    }

    private void setTotalAmount() {
        int totalTemp = amount;
        total += totalTemp;
        etTotal.setBodyText(formatter.format(total));
    }

    private String getTotalPlays() {
        return String.valueOf(sellItems.size());
    }

    private void showPlaysConfirmed() {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            Collections.sort(sellItems, (o1, o2) -> Integer.valueOf(o1.raffle).compareTo(o2.raffle));
            ListConfirmAdapter listConfirmAdapter = new ListConfirmAdapter(getActivity(), sellItems, true);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View ViewAlerta = inflater.inflate(R.layout.list_confirm_plays, null);
            ListView ListView = ViewAlerta.findViewById(R.id.lv_confirm_pays);
            TextView lvTitulo = ViewAlerta.findViewById(R.id.tv_confirm_title);
            TextView tvPlayInformation = ViewAlerta.findViewById(R.id.tv_information_confirm);
            String plaInformation = "";
            int tpStatus = Global.isTicketAvailableTPWinner(sellItems);
            if (tpStatus == 0){
                tpStatus = TriplePlayAvailability.checkTpAvailability(getActivity(), sellItems) ? 0 : 1;
            }
            if (tpStatus == 0) {
                plaInformation = "<font color=\"#008000\">" + "*** Ticket valido para Triple Play ***</font>";
            } else if (tpStatus == 1 ||tpStatus == 2 ||tpStatus == 4) {
                plaInformation = "<font color=\"red\">" + "*** Ticket NO valido para Triple Play ***</font>";
            } else if (tpStatus == 3) {
                tvPlayInformation.setVisibility(View.INVISIBLE);
            }
            tvPlayInformation.setText(Html.fromHtml(plaInformation));
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(400);
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            tvPlayInformation.startAnimation(anim);
            String value = "Jugadas: <b>" + getTotalPlays() + "</b> | Total: <b>" + Format.getCashFormat(getTotalAmount()) + "</b>";
            lvTitulo.setText(Html.fromHtml(value));
            ListView.setAdapter(listConfirmAdapter);
            builder.setCancelable(false);
            builder.setPositiveButton("Confirmar", (dialog, which) -> {

                if (rulesSingleton.getGameRules().isAccepted() && rulesSingleton.getTermsConditions().isAccepted()) {
                    progressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    progressDialog.setTitleText("Validando jugada");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    btnPlay.setEnabled(true);
                    new ApplySell().execute(Global.ENDPOINT_SELL);
                } else if (!rulesSingleton.getGameRules().isAccepted()) {
                    Toast.makeText(getActivity(),"Debe aceptar las Reglas de Juego para continuar",Toast.LENGTH_SHORT).show();
                    btnPlay.setEnabled(true);
                } else if (!rulesSingleton.getTermsConditions().isAccepted()) {
                    Toast.makeText(getActivity(),"Debe aceptar los Terminos y Condiciones para continuar",Toast.LENGTH_SHORT).show();
                    btnPlay.setEnabled(true);
                }
            });
            builder.setNegativeButton("Cerrar", (dialog, which) -> btnPlay.setEnabled(true));
            builder.setView(ViewAlerta);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.getMessage();
            btnPlay.setEnabled(true);
            Toast.makeText(getActivity(),"Algo salio mal, actualice los sorteos",Toast.LENGTH_SHORT).show();
        }
    }

    private void filterRaffle() {
        int tmpRaffle = 0;
        rafflesList.clear();
        for (int i = Global.rafflesByGames.size() - 1; i >= 0; i--) {
            if (Global.rafflesByGames.get(i).getHour() <= Time.getCurrentTime()) {
                Global.rafflesByGames.remove(i);
            }
        }
        for (int i = 0; i < Global.rafflesByGames.size(); i++) {
            if (tmpRaffle != Global.rafflesByGames.get(i).getHour()) {
                rafflesList.add(Global.rafflesByGames.get(i));
            }
            tmpRaffle = Global.rafflesByGames.get(i).getHour();
        }
        raffleListAdapter.notifyDataSetChanged();
    }

    private void showChipsRandomDialog() {
        AlertDialog iconDialog;
        if (isPlayAdded) {
            Chip.clearChipSelection();
            isPlayAdded = false;
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_random_chips_selection, null);
        ImageButton btnPlusSafari = view.findViewById(R.id.btn_plus_safari);
        ImageButton btnPlusTizana = view.findViewById(R.id.btn_plus_tizana);
        ImageButton btnPlusLoto = view.findViewById(R.id.btn_plus_loto);
        ImageButton btnMinusSafari = view.findViewById(R.id.btn_minus_safari);
        ImageButton btnMinusTizana = view.findViewById(R.id.btn_minus_tizana);
        ImageButton btnMinusLoto = view.findViewById(R.id.btn_minus_loto);
        TextView tvSafari = view.findViewById(R.id.tv_random_safari);
        TextView tvTizana = view.findViewById(R.id.tv_random_tizana);
        TextView tvLoto = view.findViewById(R.id.tv_random_loto);
        ImageButton plus = view.findViewById(R.id.btn_amount_plus_random);
        ImageButton sus = view.findViewById(R.id.btn_amount_minus_random);
        EditText etAmount = view.findViewById(R.id.etAmount);
        TextView tvNumberOfPlays = view.findViewById(R.id.tv_play_number2);
        tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
        TextView tvRaffle = view.findViewById(R.id.tv_raffle_chips2);
        tvRaffle.setText("Sorteo: " + Format.getRaffleHour(RaffleLabel.getSelectedRaffle(rafflesList).getHour()));
        etAmount.setGravity(Gravity.CENTER);
        etAmount.setHorizontallyScrolling(false);
        etAmount.setText("100");
        etAmount.setGravity(Gravity.CENTER);
        ImageButton btnDone = view.findViewById(R.id.btn_done_random);
        ImageButton btnCancel = view.findViewById(R.id.btn_cancel_random);
        builder.setCancelable(false);
        builder.setView(view);
        iconDialog = builder.create();
        btnPlusSafari.setOnClickListener(view17 -> {
            int size;
            if (Chip.getChipsSizePerGame(Global.currentGames, Global.KEY_SAF) > randomMap.get(Global.KEY_SAF)) {
                size = randomMap.get(Global.KEY_SAF) + 1;
                randomMap.put(Global.KEY_SAF, size);
                tvSafari.setText(randomMap.get(Global.KEY_SAF).toString());
                tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
            }
        });
        btnPlusTizana.setOnClickListener(view16 -> {
            int size;
            if (Chip.getChipsSizePerGame(Global.currentGames, Global.KEY_TIZ) > randomMap.get(Global.KEY_TIZ)) {
                size = randomMap.get(Global.KEY_TIZ) + 1;
                randomMap.put(Global.KEY_TIZ, size);
                tvTizana.setText(randomMap.get(Global.KEY_TIZ).toString());
                tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
            }
        });
        btnPlusLoto.setOnClickListener(view15 -> {
            int size;
            if (Chip.getChipsSizePerGame(Global.currentGames, Global.KEY_LOT) > randomMap.get(Global.KEY_LOT)) {
                size = randomMap.get(Global.KEY_LOT) + 1;
                randomMap.put(Global.KEY_LOT, size);
                tvLoto.setText(randomMap.get(Global.KEY_LOT).toString());
                tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
            }
        });
        btnMinusSafari.setOnClickListener(view14 -> {
            int size = 0;
            if (randomMap.get(Global.KEY_SAF) != 0) {
                size = randomMap.get(Global.KEY_SAF) - 1;
            }
            randomMap.put(Global.KEY_SAF, size);
            tvSafari.setText(randomMap.get(Global.KEY_SAF).toString());
            tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
        });
        btnMinusTizana.setOnClickListener(view13 -> {
            int size = 0;
            if (randomMap.get(Global.KEY_TIZ) != 0) {
                size = randomMap.get(Global.KEY_TIZ) - 1;
            }
            randomMap.put(Global.KEY_TIZ, size);
            tvTizana.setText(randomMap.get(Global.KEY_TIZ).toString());
            tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
        });
        btnMinusLoto.setOnClickListener(view12 -> {
            int size = 0;
            if (randomMap.get(Global.KEY_LOT) != 0) {
                size = randomMap.get(Global.KEY_LOT) - 1;
            }
            randomMap.put(Global.KEY_LOT, size);
            tvLoto.setText(randomMap.get(Global.KEY_LOT).toString());
            tvNumberOfPlays.setText("Fichas: " + (randomMap.get(Global.KEY_SAF) + randomMap.get(Global.KEY_LOT) + randomMap.get(Global.KEY_TIZ)));
        });
        plus.setOnClickListener(v -> etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) + 100)));
        sus.setOnClickListener(v -> {
            if (!etAmount.getText().toString().equals("0") && !etAmount.getText().toString().equals("")) {
                etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) - 100));
                if (Integer.parseInt(etAmount.getText().toString()) <= 0 && !etAmount.getText().toString().equals("")) {
                    etAmount.setText("0");
                }
            }
        });
        btnDone.setOnClickListener(view1 -> {
            for (Map.Entry<String, Integer> map : randomMap.entrySet()) {
                for (int i = 0; i < map.getValue(); i++) {
                    Chip chip = RandomUtil.getRandomChip(map.getKey());
                    int index = chipArrayList.indexOf(chip);
                    chipArrayList.get(index).setSelected(true);
                }
            }
            amount = Integer.valueOf(etAmount.getText().toString());
            addPlay();
            iconDialog.dismiss();
            randomModeSingleton.setRandom(false);
            setRandomModeView(randomModeSingleton.isRandom());
            raffleGameListView.setEnabled(true);
            resetRandomVars();
            RaffleLabel.clearRaffleSelection(rafflesList);
            raffleListAdapter.notifyDataSetChanged();
        });
        btnCancel.setOnClickListener(view13 -> {
            iconDialog.dismiss();
            Chip.clearChipSelection();
            RaffleLabel.clearRaffleSelection(rafflesList);
            raffleListAdapter.notifyDataSetChanged();
            raffleGameListView.setEnabled(true);
            randomModeSingleton.setRandom(false);
            setRandomModeView(randomModeSingleton.isRandom());
            resetRandomVars();
        });
        iconDialog.show();
    }

    private void showChipsAvailable() {
        try {
            AlertDialog iconDialog;
            if (isPlayAdded) {
                Chip.clearChipSelection();
                isPlayAdded = false;
            }
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.grid_play2, null);
            final TextView tvNumberOfPlays = view.findViewById(R.id.tv_play_number);
            ImageView img1 = view.findViewById(R.id.imageView90);
            ImageView img2 = view.findViewById(R.id.imageView10);
            ImageView img3 = view.findViewById(R.id.imageView110);
            TextView tvSafariPlays = view.findViewById(R.id.tv_safari_plays);
            TextView tvTizanaPlay = view.findViewById(R.id.tv_tizana_plays);
            TextView tvLotoPlays = view.findViewById(R.id.tv_loto_plays);
            ImageButton btnCancel = view.findViewById(R.id.imageButton10);
            ImageButton btnAdd = view.findViewById(R.id.imageButton9);
            ImageButton plus = view.findViewById(R.id.imageButton8);
            ImageButton sus = view.findViewById(R.id.imageButton6);
            EditText etAmount = view.findViewById(R.id.etAmount);
            etAmount.setGravity(Gravity.CENTER);
            etAmount.setHorizontallyScrolling(false);
            etAmount.setText(getLastAmount());
            etAmount.setGravity(Gravity.CENTER);
            img1.setImageResource(getResources().getIdentifier(gamesNameLogo.get(0), "drawable", getActivity().getPackageName()));
            img2.setImageResource(getResources().getIdentifier(gamesNameLogo.get(1), "drawable", getActivity().getPackageName()));
            img3.setImageResource(getResources().getIdentifier(gamesNameLogo.get(2), "drawable", getActivity().getPackageName()));
            TextView tvRaffle = view.findViewById(R.id.tv_raffle_chips);
            tvRaffle.setText("Sorteo: " + Format.getRaffleHour(RaffleLabel.getSelectedRaffle(rafflesList).getHour()));
            ChipsPlayGridAdapter chipsGameGridAdapter = new ChipsPlayGridAdapter(getActivity(), chipArrayList);
            GridView gridView = view.findViewById(R.id.grid_play);
            gridView.setAdapter(chipsGameGridAdapter);
            gridView.setOnItemClickListener((adapterView, view1, i, l) -> {
                chipArrayList.get(i).setSelected(!chipArrayList.get(i).isSelected());
                chipsGameGridAdapter.notifyDataSetChanged();
                tvNumberOfPlays.setText("Fichas: " + String.valueOf(Chip.getNumberChipsSelected(Global.currentGames)));
                tvLotoPlays.setText("(" + String.valueOf(Chip.getNumberChipsSelectedPerGames(Global.currentGames, Global.KEY_LOT)) + ")");
                tvSafariPlays.setText("(" + String.valueOf(Chip.getNumberChipsSelectedPerGames(Global.currentGames, Global.KEY_SAF)) + ")");
                tvTizanaPlay.setText("(" + String.valueOf(Chip.getNumberChipsSelectedPerGames(Global.currentGames, Global.KEY_TIZ)) + ")");
            });
            builder.setCancelable(false);
            builder.setView(view);
            iconDialog = builder.create();
            etAmount.setOnKeyListener((v, keyCode, event) -> {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    playsListView.setFocusable(false);
                    return true;
                }
                return false;
            });
            plus.setOnClickListener(v -> etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) + 100)));
            sus.setOnClickListener(v -> {
                if (!etAmount.getText().toString().equals("0") && !etAmount.getText().toString().equals("")) {
                    etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) - 100));
                    if (Integer.parseInt(etAmount.getText().toString()) <= 0 && !etAmount.getText().toString().equals("")) {
                        etAmount.setText("0");
                    }
                }
            });
            btnAdd.setOnClickListener(view12 -> {
                try {
                    if (Global.currentGames != null && Chip.isChipSelected() && Integer.valueOf(etAmount.getText().toString()) >= 50) {
                        amount = Integer.valueOf(etAmount.getText().toString());
                        SharedPreferenceConstants.saveSharedPreferencesData(getActivity(), SharedPreferenceConstants.BET_AMOUNT_PATH, SharedPreferenceConstants.BET_AMOUNT_FILE, etAmount.getText().toString());
                        addPlay();
                        Chip.clearChipSelection();
                        RaffleLabel.clearRaffleSelection(rafflesList);
                        raffleGameListView.setEnabled(true);
                        raffleListAdapter.notifyDataSetChanged();
                        iconDialog.dismiss();
                    } else if (!Chip.isChipSelected()) {
                        Toast.makeText(getActivity(), "Debe seleccionar una ficha", Toast.LENGTH_SHORT).show();
                    } else if (Integer.valueOf(etAmount.getText().toString()) < 50) {
                        Toast.makeText(getActivity(), "Monto minimo 50", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
            });
            btnCancel.setOnClickListener(view13 -> {
                iconDialog.dismiss();
                Chip.clearChipSelection();
                RaffleLabel.clearRaffleSelection(rafflesList);
                raffleGameListView.setEnabled(true);
                raffleListAdapter.notifyDataSetChanged();
            });
            iconDialog.show();
        } catch (Exception e) {
            Global.Toaster.get().showToast(getActivity(), "No se puede seleccionar sorteo", Toast.LENGTH_SHORT);
        }
    }

    private String getLastAmount() {
        String amount = SharedPreferenceConstants.getSharedPreferencesData(getActivity(), SharedPreferenceConstants.BET_AMOUNT_PATH, SharedPreferenceConstants.BET_AMOUNT_FILE);
        if (amount == null || amount.isEmpty()) {
            amount = "100";
        }
        return amount;
    }

    private void resetRandomVars() {
        randomMap.put(Global.KEY_SAF, 3);
        randomMap.put(Global.KEY_LOT, 3);
        randomMap.put(Global.KEY_TIZ, 3);
    }

    void init(View rootView) {
        rulesSingleton = RulesSingleton.getInstance(getActivity());
        btnRamdon = rootView.findViewById(R.id.btn_ramdon);
        tvRandom = rootView.findViewById(R.id.tv_ramdon);
        setRandomModeView(randomModeSingleton.isRandom());
        formatter = new DecimalFormat("#,###");
        etBalance = rootView.findViewById(R.id.ownTextView2);
        etTotal = rootView.findViewById(R.id.ownTextView);
        btnCancel = rootView.findViewById(R.id.btn_cancel);
        btnPlay = rootView.findViewById(R.id.btn_play);
        raffleGameListView = rootView.findViewById(R.id.lv_repeat_raffles);
        playsListView = rootView.findViewById(R.id.PlaysListView);
        raffleListAdapter = new RaffleListAdapter(getActivity(), rafflesList);
        raffleGameListView.setAdapter(raffleListAdapter);
        mSwipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh);
        playsListAdapter = new PlaysListAdapter(getActivity(), sellItems, PrimaryFragment.this);
        playsListView.setAdapter(playsListAdapter);
        playsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (playsListView == null || playsListView.getChildCount() == 0) ? 0 : playsListView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        raffleGameListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (raffleGameListView == null || raffleGameListView.getChildCount() == 0) ? 0 : raffleGameListView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        btnRamdon.setOnClickListener(view -> {
            randomModeSingleton.setRandom(!randomModeSingleton.isRandom());
            setRandomModeView(randomModeSingleton.isRandom());
        });
        progressBar = rootView.findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void getData() {
        Global.rafflesByGames.clear();
        chipArrayList.clear();
        gamesNameLogo.clear();
        if (CompareDate.isAppDateChanged()) {
            if (Global.currentGames != null) {
                for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
                    Game sg = games.getValue();
                    String gameName = games.getKey();
                    randomMap.put(games.getValue().getCodeName(), 3);
                    gamesNameLogo.add(OrderArray.setLotteryImage(sg.getCodeName()));
                    LinkedHashMap<Integer, Raffle> raffle = sg.getRaffles();
                    for (Map.Entry<Integer, Raffle> ra : raffle.entrySet()) {
                        if (ra.getKey() > Time.getCurrentTime()) {
                            RaffleLabel raffleLabel = new RaffleLabel(gameName, ra.getValue(), ra.getKey());
                            Global.rafflesByGames.add(raffleLabel);
                        }
                    }
                    LinkedHashMap<String, Chip> chips = sg.getChips();
                    for (Map.Entry<String, Chip> chip : chips.entrySet()) {
                        Chip c = chip.getValue();
                        c.chipLabel = chip.getKey();
                        c.gameId = sg.getId();
                        chipArrayList.add(chip.getValue());
                    }
                }
                Collections.sort(Global.rafflesByGames);
                OrderArray.orderArrayListZerosChips(chipArrayList);
                filterRaffle();
            }
        }
    }

    @Override
    public void onItemDelete(int amount) {
        deletePlay();
    }

    private void setRandomModeView(boolean isRandom) {
        if (isRandom) {
            btnRamdon.setImageResource(R.drawable.ic_games_white_32dp);
            tvRandom.setTextColor(Color.WHITE);
        } else {
            btnRamdon.setImageResource(R.drawable.ic_games_gray_32dp);
            tvRandom.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    private class ApplySynchronization extends AsyncTask<String, Integer, String> {
        String response;
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpRequest request = HttpRequest.post(Global.URL_SYNCRO_MOBILE).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .header(Global.KEY_TOKEN_FB, Global.refreshedToken)
                        .header(Global.KEY_TIMESTAMP,Global.timestamp)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = request.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "No se pueden actualizar los sorteos", Toast.LENGTH_SHORT);
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            Response response;
            progressBar.setVisibility(View.GONE);
            if (!s.isEmpty()) {
                try {
                    response = new Gson().fromJson(s, Response.class);
                    if (response.error.isEmpty()) {
                        LoginMessage message = gson.fromJson(response.message, LoginMessage.class);
                        Global.cash = String.valueOf(response.cash);
                        cash = response.cash;
                        Global.promotional = response.promotional;
                        etBalance.setBodyText(Format.getCashFormat(cash));
                        saveIncomingCashPlayer(cash);
                        if (message.synchronization) {
                            SharedPreferenceConstants.saveTimestamp(getActivity(),Global.timestamp);
                            SharedPreferenceConstants.saveData(getActivity(),s);
                            Global.timestamp = response.timestamp;
                            Global.producerRating = message.producerRating;
                            Global.producerName = message.producerName;
                            Global.currentGames = message.games;
                            Global.producerId = message.producerId;
                            Global.userLogin = message.userlogin;
                            Global.features = message.producerFeatures;
                            Global.userName = message.username;
                            getData();
                        }
                    } else {
                        Global.Toaster.get().showToast(getContext(), response.message, Toast.LENGTH_SHORT);
                        if (response.message.equals("Problema de autorizacion") && Global.activityVisible) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Error al actualizar")
                                    .setContentText("Su sesion se encuentra activa en otro dispositivo")
                                    .setConfirmText("Cerrar Aplicacion")
                                    .showCancelButton(true)
                                    .setConfirmClickListener(sDialog -> {
                                        sDialog.cancel();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        LoginState.setLoginState(getActivity(), false);
                                        getActivity().finish();
                                    })
                                    .show();
                        }
                    }
                } catch (HttpRequest.HttpRequestException ex) {
                    progressBar.setVisibility(View.GONE);
                    Global.Toaster.get().showToast(getActivity(), "No se pueden actualizar los sorteos", Toast.LENGTH_SHORT);
                }
            } else {
                progressBar.setVisibility(View.GONE);
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            }
        }
    }

    private class ApplySell extends AsyncTask<String, Integer, String> {
        AlertDialog.Builder builder = new AlertDialog.Builder(PrimaryFragment.this.getActivity());
        boolean flagResponse = false;
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            Sell sell = new Sell();
            sell.userId = Global.userId;
            sell.xSerial = new Date().getTime() + "";
            sell.sellItems = sellItems;
            jsonData = gson.toJson(sell);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getContext(), "Falla de red", Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (!s.isEmpty()) {
                Response response = gson.fromJson(s, Response.class);
                try {
                    if (response.error.equals("")) {
                        Sell sellResponse = gson.fromJson(response.message, Sell.class);
                        if (sellResponse.getTriplePlayValid() != null && sellResponse.getTriplePlayValid().size() > 0) {
                            TriplePlayAvailability.saveTicketTpStatus(getActivity(), sellResponse.getTriplePlayValid(), new Date());
                        }
                        ArrayList<SellItem> se = new ArrayList<>();
                        cash = response.cash;
                        Global.promotional = response.promotional;
                        etBalance.setBodyText(Format.getCashFormat(cash));
                        Global.cash = String.valueOf(cash);
                        saveIncomingCashPlayer(cash);
                        se.addAll(sellResponse.sellItems);
                        ListConfirmAdapter listConfirmAdapter = new ListConfirmAdapter(getActivity(), se, false);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View ViewAlerta = inflater.inflate(R.layout.list_view_general, null);
                        ListView xGrilla = ViewAlerta.findViewById(R.id.listView1);
                        TextView xTitulo = ViewAlerta.findViewById(R.id.lblTit);
                        String value = "Ticket: <b>" + sellResponse.xSerial + "&#160; &#160; &#160; &#160;" +
                                "<font color=\"#008000\">" + SellItem.getCountSellItem(se, true) + "</font>/" +
                                "<font color=\"red\">" + SellItem.getCountSellItem(se, false) + "</font></b>";
                        xTitulo.setText(Html.fromHtml(value));
                        xGrilla.setAdapter(listConfirmAdapter);
                        builder.setNegativeButton("Cerrar", null);
                        builder.setView(ViewAlerta);
                        builder.setCancelable(false);
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        flagResponse = true;
                        Global.Toaster.get().showToast(getActivity(), "Ticket Procesado", Toast.LENGTH_SHORT);
                        ((MainActivity)getActivity()).refreshTicket();
                        cancelPlay();
                        if (Integer.parseInt(sellResponse.getLicenseTermVersion()) > Integer.parseInt(rulesSingleton.getTermsConditions().getVersion())) {
                            rulesSingleton.setTermsUpdate(true);
                            //((MainActivity)getActivity()).setTermsUpdateFlag(true);
                            ((MainActivity)getActivity()).requestTermsConditions(); 
                        }
                        if (Integer.parseInt(sellResponse.getGameTermVersion()) > Integer.parseInt(rulesSingleton.getGameRules().getVersion())) {
                            rulesSingleton.setRulesUpdate(true);
                            ((MainActivity)getActivity()).requestGameRules();
                        }
                    } else {
                        Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);//showToast("No se pudo completar la venta");
                    }
                } catch (HttpRequest.HttpRequestException e) {
                    Global.Toaster.get().showToast(getActivity(), "falla de red", Toast.LENGTH_SHORT);//showToast("Falla1 de red");
                }
            } else {
                Global.Toaster.get().showToast(getActivity(), "No se pudo completar la compra", Toast.LENGTH_SHORT);//showToast("No se pudo completar la venta");
            }
            if (flagResponse) {
                PrimaryFragment.this.cancelPlay();
            }
        }
    }
}
