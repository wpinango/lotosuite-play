package com.lotosuiteplay.azar.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.common.Time;
import com.lotosuiteplay.azar.models.ResultList;
import com.lotosuiteplay.azar.R;
import com.lotosuiteplay.azar.models.Response;
import com.lotosuiteplay.azar.adapters.ResultListAdapter;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wpinango on 7/29/17.
 */
public class ResultFragment extends Fragment {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ResultListAdapter resultListAdapter;
    private ProgressBar progressBar;
    private ArrayList<ResultList> resultLists = new ArrayList<>();
    private String dateResult;
    private ImageButton btnHist;
    public static String refresh = null;
    private String resultTag = "result1" + Global.userId;
    private String resultIdTag = "resultId1" + Global.userId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_result, null);
        ImageButton refreshButton = root.findViewById(R.id.btn_refresh_result);
        mSwipeRefreshLayout = root.findViewById(R.id.activity_main_swipe_refresh_layout);
        btnHist = root.findViewById(R.id.btn_search_result);
        ListView lvResult = root.findViewById(R.id.lv_result);
        progressBar = root.findViewById(R.id.progress_bar_result);
        progressBar.setVisibility(View.INVISIBLE);
        resultListAdapter = new ResultListAdapter(getActivity(),resultLists);
        lvResult.setAdapter(resultListAdapter);

        refreshButton.setOnClickListener(v -> refreshContent());
        mSwipeRefreshLayout.setOnRefreshListener(() -> refreshContent());
        btnHist.setOnClickListener(view -> {
            btnHist.setEnabled(false);
            alertDatePicker();
        });
        checkDataSaved();
        return root;
    }

    private void checkDataSaved() {
        if (resultLists.isEmpty()) {
            if (SharedPreferenceConstants.getGeneralData(getContext(),resultTag) != "") {
                if (checkSavedDataDay()) {
                    resultLists.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants
                            .getGeneralData(getActivity(), resultTag), ResultList[].class)));
                } else {
                    SharedPreferenceConstants.saveGeneralData(getActivity(), "", resultTag);
                    refreshAllContent();
                }
            } else {
                refreshAllContent();
            }
        }
    }

    private void refreshAllContent(){
        SharedPreferenceConstants.saveGeneralData(getActivity(), "", resultIdTag);
        refreshContent();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (resultLists.isEmpty() || refresh != null) {
                refreshContent();
                refresh = null;
            }
        } catch (Exception e) {

        }
    }

    public void refreshContent() {
        dateResult = com.lotosuiteplay.azar.common.Time.getCurrentDate();
        new GetResults().execute(Global.URL_GET_RESULT);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -6);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(System.currentTimeMillis());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Ir", (dialog, id) -> {
                    int month = myDatePicker.getMonth() + 1;
                    int day = myDatePicker.getDayOfMonth();
                    int year = myDatePicker.getYear();
                    String formattedMonth = "" + month;
                    String formattedDayOfMonth = "" + day;
                    if (month < 10) {
                        formattedMonth = "0" + month;
                    }
                    if (day < 10) {
                        formattedDayOfMonth = "0" + day;
                    }
                    dateResult = String.valueOf(formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                    new GetResults().execute(Global.URL_GET_RESULT);
                    dialog.cancel();
                    btnHist.setEnabled(true);
                }).show().setCancelable(false);
    }

    private boolean checkSavedDataDay() {
        try {
            ArrayList<ResultList> r = new ArrayList<>(Arrays.asList(new Gson()
                    .fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), resultTag),
                            ResultList[].class)));
            if (r.isEmpty()) {
                return false;
            }
            for (int i = 0; i < r.size(); i ++) {
                int day = Integer.valueOf(r.get(i).getDate().split("-")[0]);
                if (day != Time.getCurrentDay()) {
                    return false;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return true;
    }

    public class GetResults extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();
        String response;
        private String hashResult;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            JsonObject dateToConsult = new JsonObject();
            dateToConsult.addProperty("startDate", dateResult);
            dateToConsult.addProperty("endDate", dateResult);
            try {
                hashResult = SharedPreferenceConstants.getGeneralData(getActivity(),resultIdTag);
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .header(Global.KEY_HASH_RESULT, hashResult)
                        .contentType("application/json")
                        .send(dateToConsult.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                HashMap<String,  ResultList> myResults; //myResults.clear();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {

                    Type custom = new TypeToken<HashMap<String, ResultList>>() {
                    }.getType();
                    myResults = gson.fromJson(response.message, custom);
                    hashResult = SharedPreferenceConstants.getGeneralData(getActivity(),resultIdTag);
                    if (!hashResult.equals(response.getHashResult()) && !myResults.isEmpty()) {     // Hash son diferentes se debe modificar todos los resultados
                        SharedPreferenceConstants.saveGeneralData(getActivity(),
                                response.getHashResult(), resultIdTag);
                        resultLists.clear();
                        resultListAdapter.notifyDataSetChanged();
                        for (Map.Entry<String, ResultList> si : myResults.entrySet()) {
                            resultLists.add(new ResultList(si.getKey(), si.getValue().getResultMobiles(), dateResult, si.getValue().getTp()));
                        }
                        Collections.sort(resultLists, (o1, o2) -> Integer.valueOf(o2.hour)
                                .compareTo(Integer.valueOf(o1.hour)));
                        if (SharedPreferenceConstants.getGeneralData(getActivity(), resultTag) != "") {
                            resultLists.addAll(Arrays.asList(new Gson().fromJson(
                                    SharedPreferenceConstants.getGeneralData(getActivity(), resultTag),
                                    ResultList[].class)));
                        }
                        resultListAdapter.notifyDataSetChanged();
                        SharedPreferenceConstants.saveGeneralData(getActivity(), new Gson()
                                .toJson(resultLists), resultTag);
                    } else if (hashResult.equals(response.getHashResult()) && !myResults.isEmpty()) {// Hash son iguales pero existe un cambio en los resultados
                        resultLists.clear();
                        for (Map.Entry<String, ResultList> si : myResults.entrySet()) {
                            resultLists.add(new ResultList(si.getKey(), si.getValue().getResultMobiles(), dateResult,si.getValue().getTp()));
                        }
                        Collections.sort(resultLists, (o1, o2) -> Integer.valueOf(o2.hour)
                                .compareTo(Integer.valueOf(o1.hour)));
                        resultListAdapter.notifyDataSetChanged();
                    } else if (myResults.isEmpty()) {                                               //
                        resultLists.clear();
                        if (SharedPreferenceConstants.getGeneralData(getActivity(), resultTag) != "") {
                            resultLists.addAll(Arrays.asList(new Gson().fromJson(
                                    SharedPreferenceConstants.getGeneralData(getActivity(), resultTag),
                                    ResultList[].class)));
                        }
                        resultListAdapter.notifyDataSetChanged();
                    }
                } else if (response.error.equals("no-content")) {
                    Global.Toaster.get().showToast(getActivity(), "No se encontraron resultados", Toast.LENGTH_SHORT);
                    resultLists.clear();
                    resultListAdapter.notifyDataSetChanged();
                }
                else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
