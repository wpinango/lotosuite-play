package com.lotosuiteplay.azar;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.widget.Toast;

import com.lotosuiteplay.azar.models.Feature;
import com.lotosuiteplay.azar.models.Game;
import com.lotosuiteplay.azar.models.RaffleLabel;
import com.lotosuiteplay.azar.models.SellItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class Global {
    public static final String KEY_TOKEN = "x-session-token";
    public static final String KEY_ID_USER = "x-identity";
    public static final String KEY_AGENCY_ID = "x-agency-id";
    public static final String KEY_SECURITY = "x-security";
    public static final String KEY_PRODUCER_ID = "x-pid";
    public static final String KEY_AUTHORIZATION = "authorization";
    public static final String KEY_SCHEMA = "x-schema";
    public static final String NEW_KEY = "x-new-key";
    public static final String KEY_MOVIL = "movilx";
    public static final String KEY_TOKEN_FB = "x-msg-token";
    public static final String KEY_TIMESTAMP = "x-ts-sync";
    public static final String KEY_HASH_BALANCE = "x-hash-balance";
    public static final String KEY_HASH_RESULT = "x-hash-result";
    public static final String TAG = "chatbubbles";
    public static final String KEY_DENIED_RESPONSE = "Problema de autorizacion";
    public static final String NAME_NOTIFICATION_SOUND = "Sonido de notificacion";
    public static final String NAME_NOTIFICATION = "Notificaciones";
    public static final String NAME_RAFFLE = "Mostrar sorteo en vivo";
    public static final String NAME_FINGER_OPTION = "Desbloquear con huella";
    public static ArrayList<String> name = new ArrayList<>();
    public static ArrayList<String> phone = new ArrayList<>();
    public static ArrayList<String> id = new ArrayList<>();
    public static ArrayList<String> email = new ArrayList<>();
    public static ArrayList<String> alias = new ArrayList<>();
    public static ArrayList<RaffleLabel> rafflesByGames = new ArrayList<>();
    public static String token = "";
    public static String userId;
    public static String agencyId;
    public static String userName;
    public static String cash;
    public static String userLogin;
    public static String footerTicket;
    public static String password;
    public static String nick;
    public static long timestamp = -1 ;
    public static int amount;
    public static int locked;
    public static int deferred;
    public static int promotional;
    public static String HOST;
    public static String URL_ONE_TICKET;
    public static String URL_ALL_TICKET;
    public static String ENDPOINT_SELL;
    public static String URL_LOGIN_MOBILE;
    public static String URL_SYNCRO_MOBILE;
    public static String URL_GET_RESULT;
    public static String URL_DISCARD_TICKET;
    public static String URL_REQUEST_CASH;
    public static String URL_REQUEST_BALANCE;
    public static String URL_GET_PRODUCER;
    public static String URL_GET_REGISTER_NEW_USERS;
    public static String URL_CLOSE_TOKEN;
    public static String URL_GET_PENDING_RECHARGE;
    public static String URL_GET_PAYMENT;
    public static String URL_GET_PENDING_PAYMENT;
    public static String URL_DELETE_PENDING_TRANSACTION;
    public static String URL_GET_PRODUCER_RATING;
    public static String URL_FIND_USER;
    public static String URL_SEND_TRANSFER;
    public static String URL_RATE_PRODUCER;
    public static String URL_GET_PRODUCER_FEATURES;
    public static String URL_CHANGE_PRODUCER;
    public static String URL_GET_BANKS;
    public static String URL_REGISTER_ACCOUNT;
    public static String URL_GET_PLAYER_ACCOUNTS;
    public static String URL_UPDATE_PLAYER_ACCOUNTS;
    public static String URL_DELETE_PLAYER_ACCOUNTS;
    public static String URL_GET_USER_PROFILE;
    public static String URL_UPDATE_USER_PROFILE;
    public static String URL_UPDATE_PASSWORD;
    public static String URL_REQUEST_PASSWORD;
    public static String URL_UPDATE_FB_TOKEN;
    public static String URL_GET_TUTORIAL;
    public static String URL_GET_TRIPLE_PLAY_WINNERS;
    public static String URL_GET_STATISTICS;
    public static String URL_GET_TRIPLE_PLAY_HISTORY;
    public static String URL_GET_TERM_CONDITIONS;
    public static String URL_GET_GAME_RULES;
    public static String refreshedToken;
    public static boolean isLogin = false;
    public static int selectedPosition;
    public static HashMap<Integer, Boolean> mycheked = new HashMap<>();
    public static LinkedHashMap<String, Game> currentGames;
    public static final String KEY_SAF = "sgt-saf";
    public static final String KEY_LOT = "sgt-lot";
    public static final String KEY_TIZ = "sgt-tiz";
    public static String producerName;
    public static int producerId;
    public static ArrayList<Feature> features = new ArrayList<>();
    public static float producerRating;
    public static long lastActivityTime = 0;
    public static boolean isNotificationShow = false;
    public static final String KEY_PINEAPPLE = "Piña";
    public static boolean activityVisible;
    public static Bitmap[] resultImage = new Bitmap[3];
    public static final String KEY_VALUE_ACCOUNT = "-";
    public static final String KEY_VALUE_ROULETTE = "roulette";
    public static final int httpRequestTimeout = 40000;

    public static void selectURL() {
        //HOST = "http://172.16.29.134:8090";
        //HOST = "http://172.16.10.175:8090";
        //HOST = "http://movil.lotosuite.com:8090";
        //HOST = "http://186.24.254.139:8083";
        //HOST = "http://172.16.31.22:8090";
        //HOST = "http://172.16.31.101:8090";
        //HOST = "http://200.125.187.60:8090";
        //HOST = "http://172.16.31.104:8090";
        //HOST = "http://46.101.158.43:8090";
        //HOST = "http://64.227.1.190:8090";
        HOST = "http://api.lotosuite.com:80";
        URL_ONE_TICKET = HOST + "/mobile/getOneTicket";
        URL_ALL_TICKET = HOST + "/mobile/getAllTickets";
        ENDPOINT_SELL = HOST + "/mobile/sell";
        URL_LOGIN_MOBILE = HOST + "/mobile/doAnonymousLogin";
        URL_SYNCRO_MOBILE = HOST + "/mobile/synchronization";
        URL_GET_RESULT = HOST + "/mobile/resultsMobile";
        URL_DISCARD_TICKET = HOST + "/mobile/discardItemsGroup";
        URL_REQUEST_CASH = HOST + "/mobile/requestCash";
        URL_REQUEST_BALANCE = HOST + "/mobile/getBalanceHistory";
        URL_GET_PRODUCER = HOST + "/mobile/getProducerList";
        URL_GET_REGISTER_NEW_USERS = HOST + "/mobile/registerNewUser";
        URL_CLOSE_TOKEN = HOST + "/mobile/destroySession";
        URL_GET_PENDING_RECHARGE = HOST + "/mobile/getPendingRecharges";
        URL_GET_PAYMENT = HOST + "/mobile/requestPayment";
        URL_GET_PENDING_PAYMENT = HOST + "/mobile/getPaymentPending";
        URL_DELETE_PENDING_TRANSACTION = HOST + "/mobile/deletePendingTransaction";
        URL_GET_PRODUCER_RATING = HOST + "/mobile/getProducerRating";
        URL_FIND_USER = HOST + "/mobile/findUser";
        URL_SEND_TRANSFER = HOST + "/mobile/transfer";
        URL_RATE_PRODUCER = HOST + "/mobile/rateProducer";
        URL_GET_PRODUCER_FEATURES = HOST + "/mobile/getProducerFeatures";
        URL_CHANGE_PRODUCER = HOST + "/mobile/changeProducer";
        URL_GET_BANKS = HOST + "/mobile/getBanks";
        URL_REGISTER_ACCOUNT = HOST + "/mobile/registerAccountNumber";
        URL_GET_PLAYER_ACCOUNTS = HOST + "/mobile/getPlayerAccounts";
        URL_UPDATE_PLAYER_ACCOUNTS = HOST + "/mobile/updatePlayerAccounts";
        URL_DELETE_PLAYER_ACCOUNTS = HOST + "/mobile/deletePlayerAccounts";
        URL_GET_USER_PROFILE = HOST + "/mobile/getUserInformation";
        URL_UPDATE_USER_PROFILE = HOST + "/mobile/updateUserProfile";
        URL_UPDATE_PASSWORD = HOST + "/mobile/updatePassword";
        URL_REQUEST_PASSWORD = HOST + "/mobile/requestAccountUnlock";
        URL_UPDATE_FB_TOKEN = HOST + "/mobile/updateFBaseToken";
        URL_GET_TUTORIAL = HOST + "/mobile/getVideos";
        URL_GET_TRIPLE_PLAY_WINNERS = HOST + "/mobile/getTriplePlayData";
        URL_GET_STATISTICS = HOST + "/mobile/resultsMobileStatistics";
        URL_GET_TRIPLE_PLAY_HISTORY = HOST + "/mobile/getTriplePlayHistory";
        URL_GET_GAME_RULES = HOST + "/mobile/getGameTerm";
        URL_GET_TERM_CONDITIONS = HOST + "/mobile/getLicenseTerm";
    }

    public static int[] lotoLogo = {R.drawable.safari2, R.drawable.tizana2, R.drawable.loto2};

    public static int[] newSafariImages = {
            R.drawable.delfin, R.drawable.ballena, R.drawable.culebra,
            R.drawable.carnero, R.drawable.toro, R.drawable.ciempies,
            R.drawable.alacran, R.drawable.leon, R.drawable.rana,
            R.drawable.perico, R.drawable.raton, R.drawable.aguila,
            R.drawable.tigre, R.drawable.gato, R.drawable.caballo,
            R.drawable.mono, R.drawable.paloma, R.drawable.zorro,
            R.drawable.oso, R.drawable.pavo, R.drawable.burro,
            R.drawable.chivo, R.drawable.cochino, R.drawable.gallo,
            R.drawable.camello, R.drawable.cebra, R.drawable.iguana,
            R.drawable.gallina, R.drawable.vaca, R.drawable.perro,
            R.drawable.zamuro, R.drawable.elefante, R.drawable.caiman,
            R.drawable.lapa, R.drawable.ardilla, R.drawable.pescado
    };

    public static int[] newTizanaImages = {R.drawable.patilla, R.drawable.pina, R.drawable.melon, R.drawable.nispero,
            R.drawable.guanabana, R.drawable.aguacate, R.drawable.limon, R.drawable.naranja,
            R.drawable.toronja, R.drawable.mandarina, R.drawable.tamarindo, R.drawable.parchita,
            R.drawable.coco, R.drawable.guayaba, R.drawable.mango, R.drawable.cambur,
            R.drawable.lechoza, R.drawable.durazno, R.drawable.pera, R.drawable.manzana,
            R.drawable.mora, R.drawable.fresa, R.drawable.melocoton, R.drawable.kiwi,
            R.drawable.mamon, R.drawable.cereza, R.drawable.ciruela, R.drawable.icaco,
            R.drawable.cotoperiz, R.drawable.uva, R.drawable.cacao, R.drawable.granada,
            R.drawable.merey, R.drawable.almendra, R.drawable.avellana, R.drawable.mani
    };

    public static int[] newLotoImages = {R.drawable.dinero, R.drawable.sexo, R.drawable.amor, R.drawable.comida,
            R.drawable.helado, R.drawable.botella, R.drawable.carro, R.drawable.bicicleta, R.drawable.moto,
            R.drawable.barco, R.drawable.avion, R.drawable.tren, R.drawable.celular,
            R.drawable.laptop, R.drawable.taza, R.drawable.bombillo, R.drawable.radio,
            R.drawable.televisor, R.drawable.fuego, R.drawable.agua, R.drawable.sol,
            R.drawable.luna, R.drawable.estrella, R.drawable.nube, R.drawable.diente,
            R.drawable.sal, R.drawable.escalera, R.drawable.espejo, R.drawable.escoba,
            R.drawable.bruja, R.drawable.arma, R.drawable.cachos, R.drawable.termometro,
            R.drawable.fantasma, R.drawable.ladron, R.drawable.despecho
    };

    public static void showToast(Context context, String msg, int timeMillis){
        final Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        if (context != null) {
            toast.show();
        }

        Handler handler = new Handler();
        handler.postDelayed(() -> toast.cancel(), timeMillis);
    }

    public enum Toaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> Toast.makeText(context, message, length).show()
                );
            }
        }

        public static Toaster get() {
            return INSTANCE;
        }
    }

    public enum CenterToaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> {
                            Toast toast = Toast.makeText(context, message, length);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                );
            }
        }

        public static CenterToaster get() {
            return INSTANCE;
        }
    }

    public static boolean playsIsSelected() {
        for (Map.Entry<Integer, Boolean> a : mycheked.entrySet()) {
            if (a.getValue().equals(true)) {
                return true;
            }
        }
        return false;
    }

    public static void setBalanceResponse(int cash, int deferred, int locked, int promotional) {
        Global.deferred = deferred;
        Global.locked = locked;
        Global.cash = String.valueOf(cash);
        Global.promotional = promotional;
    }

    public static int isTicketAvailableTPWinner(ArrayList<SellItem> sellItems) {
        // 0-tp, 1-no Tp, 2-no Tp
        int valueReturn = 4;
        LinkedHashMap<Integer, LinkedHashMap<String, Integer>> test = new LinkedHashMap<>();
        for (int i = 0; i < sellItems.size(); i++) {
            LinkedHashMap<String, Integer> t = test.get(sellItems.get(i).getRaffle());
            if (t == null) {
                t = new LinkedHashMap<>();
                t.put(sellItems.get(i).getCodeName(), 1);
            } else {
                if (t.get(sellItems.get(i).getCodeName()) == null) {
                    t.put(sellItems.get(i).getCodeName(), 1);
                } else {
                    t.put(sellItems.get(i).getCodeName(), t.get(sellItems.get(i).getCodeName()) + 1);
                }
            }
            test.put(sellItems.get(i).getRaffle(), t);
        }
        for (Map.Entry<Integer, LinkedHashMap<String, Integer>> t : test.entrySet()) {
            LinkedHashMap<String, Integer> a = t.getValue();
            /*if (a.size() != 3){
                return 2;
            }*/
            for (Map.Entry<String,Integer> b: a.entrySet() ){
                if (b.getValue() > 9){
                    return 1;
                }
                else if (b.getValue() <= 9 && a.size() == 3){
                    valueReturn = 0;
                }
            }
            //return valueReturn;
        }
        return valueReturn;
    }
}