package com.lotosuiteplay.azar.singletons;

import android.content.Context;

public class BlockAppSingleton {

    private static BlockAppSingleton instance;
    private boolean isBlock;

    private BlockAppSingleton(Context context) {
        isBlock = false;
    }

    public static BlockAppSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new BlockAppSingleton(context);
        }
        return instance;
    }

    public void setRandom(boolean isRandom) {
        this.isBlock = isRandom;
    }

    public boolean isRandom() {
        return this.isBlock;
    }

    public boolean checkAppIsBlocked() {
        
        return true;
    }
}
