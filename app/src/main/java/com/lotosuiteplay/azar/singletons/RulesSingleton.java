package com.lotosuiteplay.azar.singletons;

import android.content.Context;

import com.google.gson.Gson;
import com.lotosuiteplay.azar.Global;
import com.lotosuiteplay.azar.models.Rule;
import com.lotosuiteplay.azar.util.SharedPreferenceConstants;


import static com.lotosuiteplay.azar.util.SharedPreferenceConstants.getGeneralData;
import static com.lotosuiteplay.azar.util.SharedPreferenceConstants.saveGeneralData;

public class RulesSingleton {
    private static RulesSingleton instance;
    private Rule gameRules;
    private Rule termsConditions;
    private boolean isTermsUpdate = false;
    private boolean isRulesUpdate = false;

    public boolean isTermsUpdate() {
        return isTermsUpdate;
    }

    public void setTermsUpdate(boolean termsUpdate) {
        isTermsUpdate = termsUpdate;
    }

    public boolean isRulesUpdate() {
        return isRulesUpdate;
    }

    public void setRulesUpdate(boolean rulesUpdate) {
        isRulesUpdate = rulesUpdate;
    }

    private RulesSingleton(Context context) {
        //String gR = getGeneralData(context, "gameRules");
        //String tC = getGeneralData(context, "termsConditions");
        String gR =  SharedPreferenceConstants.getSharedPreferencesData(context, Global.userId,
                "gameRules");
        String tC = SharedPreferenceConstants.getSharedPreferencesData(context, Global.userId,
                "termsConditions");
        if (!gR.equals("")) {
            gameRules = new Gson().fromJson(gR, Rule.class);
        } else {
            gameRules = new Rule();
        }
        if (!tC.equals("")) {
            termsConditions = new Gson().fromJson(tC, Rule.class);
        } else {
            termsConditions = new Rule();
        }
    }

    public static RulesSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new RulesSingleton(context);
        }
        return instance;
    }

    public Rule getGameRules() {
        return gameRules;
    }

    public Rule getTermsConditions() {
        return termsConditions;
    }

    public void setGameRules(Context context, Rule gameRules) {
        this.gameRules = gameRules;
        //saveGeneralData(context, new Gson().toJson(gameRules), "gameRules");
        SharedPreferenceConstants.saveSharedPreferencesData(
                context, Global.userId, "gameRules", new Gson().toJson(gameRules));
    }

    public void setTermsConditions(Context context, Rule termsConditions) {
        this.termsConditions = termsConditions;
        //saveGeneralData(context, new Gson().toJson(termsConditions), "termsConditions");
        SharedPreferenceConstants.saveSharedPreferencesData(
                context, Global.userId, "termsConditions", new Gson().toJson(termsConditions));
    }
}
