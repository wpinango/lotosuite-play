package com.lotosuiteplay.azar.singletons;

import android.content.Context;

public class RandomModeSingleton {

    private static RandomModeSingleton instance;
    private boolean isRandom;

    private RandomModeSingleton(Context context) {
        isRandom = false;
    }

    public static RandomModeSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new RandomModeSingleton(context);
        }
        return instance;
    }

    public void setRandom(boolean isRandom) {
        this.isRandom = isRandom;
    }

    public boolean isRandom() {
        return this.isRandom;
    }
}
